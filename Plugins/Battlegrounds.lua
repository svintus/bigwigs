--[[ToDo
	Outside - timer bar when BG pops
			Figure out when to check for time left
			Figure out Which index to verify
			Figure out how to stop the bar on cancel and on bg enter
			expiration = GetBattlefieldPortExpiration(1);  -- gives time in milliseconds, is 1 always WSG?
			clickTime = expiration / 1000
			self:Bar(L["bar_clickTime"], clickTime, icon.clickTime)

	WSG - 	Add powerUps timers

	AB - Create bar for victory/defeat and verify accuracy
	
	AV - module not done
]]--
--[[
/script landmarksQty = GetNumMapLandmarks();
/script DEFAULT_CHAT_FRAME:AddMessage("landmarksQty: "..landmarksQty );

/script a=1;
/script name, desc, typ, x, y = GetMapLandmarkInfo(a);
/script DEFAULT_CHAT_FRAME:AddMessage(a.." name: "..name);
/script DEFAULT_CHAT_FRAME:AddMessage(a.." desc: "..desc);
/script DEFAULT_CHAT_FRAME:AddMessage(a.." typ: "..typ);
/script DEFAULT_CHAT_FRAME:AddMessage(a.." x: "..x);
/script DEFAULT_CHAT_FRAME:AddMessage(a.." y: "..y);

typ:
Alterac Valley
	0 = Mines, no icon
	1 = Horde controlled mine
	2 = Alliance controlled mine
	3 = Horde graveyard attacked by Alliance
	4 = Towns (Booty Bay, Stonard, etc)
	5 = Destroyed tower
	6 = 
	7 = Uncontrolled Graveyard (Snowfall at start)
	8 = Horde tower attacked by Alliance
	9 = Horde controlled tower
	10 = Alliance controlled tower
	11 = Alliance tower attacked by Horde
	12 = Horde controlled graveyard
	13 = Alliance graveyard attacked by Horde
	14 = Alliance controlled graveyard
	15 = Garrisons/Caverns, no icon
Arathi Basin
	16 - Gold Mine - Uncontrolled
	17 - Gold Mine .. in conflict (Alliance capturing)
	18 - Gold Mine - Alliance controlled
	19 = Gold Mine .. in conflict (Horde capturing)
	20 = Gold Mine - Horde Controlled
	21 = Lumber Mill - Uncontrolled
	22 = Lumber Mill .. in conflict (Alliance capturing)
	23 = Lumber Mill - Alliance controlled
	24 = Lumber Mill .. in conflict (Horde capturing)
	25 = Lumber Mill - Horde Controlled
	26 = Blacksmith - Uncontrolled
	27 = Blacksmith .. in conflict (Alliance capturing)
	28 = Blacksmith - Alliance controlled
	29 = Blacksmith .. in conflict (Horde capturing)
	30 = Blacksmith - Horde controlled
	31 = Farm - Uncontrolled
	32 = Farm .. in conflict (Alliance capturing)
	33 = Farm - Alliance controlled
	34 = Farm .. in conflict (Horde capturing)
	35 = Farm - Horde controlled
	36 = Stables - Uncontrolled
	37 = Stables .. in conflict (Alliance capturing)
	38 = Stables - Alliance controlled
	39 = Stables .. in conflict (Horde capturing)
	40 = Stables - Horde controlled
--]]


----------------------------
--      Localization      --
----------------------------

local L = AceLibrary("AceLocale-2.2"):new("BigWigsBattlegrounds")
local PaintChips = AceLibrary("PaintChips-2.0")
local RL = AceLibrary("RosterLib-2.0")

L:RegisterTranslations("enUS", function() return {
	
	-- Options
	["Battlegrounds"] = true,
	["Options for the Battlegrounds module."] = true,
	
	["Bars"] = true,
	["Toggle Battlegrounds bars on or off."] = true,
	
	["Resurrection"] = true,
	["Toggle Battlegrounds Resurrection bars on or off.\n\nNote: these timers are often incorrect."] = true,
	
	-- Misc
	-- Internal names in the tables here are the same for all localizations and do not contain spaces
		-- Localized BG zone name returned by GetRealZoneText() -> internal name
	ZonesToInternal =
	{
		["Warsong Gulch"]  = "WSG",
		["Alterac Valley"] = "AV",
		["Arathi Basin"]   = "AB",
		["Azshara Crater"] = "AC",
		["Crimson Ring Arena"] = "Arena",
	},
	
		-- Internal names are the same in SubzonesToLocalized, SubzonesToInternal, SubzonesMSGToInternal
		-- SubzonesToLocalized and SubzonesToInternal2 have few extra though that are not present in SubzonesMSGToInternal
	
		-- Internal name used for sync -> localized BG subzone name returned by GetSubZoneText()
		-- (GetMapLandmarkInfo() apparently returns the same name as GetSubZoneText(). Not sure if always or not.)
	SubzonesToLocalized =
	{
		-- WSG
		["Resurrection"] = "Resurrection",  -- not really a subzone but fits here
		
		-- AV
		
		-- AB
		["Farm"]       = "Farm",
		["Blacksmith"] = "Blacksmith",
		["GoldMine"]   = "Gold Mine",
		["LumberMill"] = "Lumber Mill",
		["Stables"]    = "Stables",
		
		-- AC
			-- Neutral
		["EastMoonwell"]        = "East Moonwell",
		["WestMoonwell"]        = "West Moonwell",
		["TempleOfQuelArkhana"] = "Temple of Quel'Arkhana",
		["HighbornesRest"]      = "Highborne's Rest",
		
			-- Alliance towers
		["HeightZero"] = "Height Zero",
		["Monolith"]   = "Monolith",
		["OwlsNest"]   = "Owl's Nest",
		["LeftWing"]   = "Left Wing",
		["RightWing"]  = "Right Wing",
		
			-- Horde towers
		["GrommashOutpost"]  = "Grommash Outpost",
		["BlindOverlook"]    = "Blind Overlook",
		["TheLastHearth"]    = "The Last Hearth",
		["SouthernIronpost"] = "Southern Ironpost",
		["NorthernIronpost"] = "Northern Ironpost",
		
			-- Resurrection. Not subzones but fit here
		["BaseHorde"]    = "Horde base",
		["BaseAlliance"] = "Alliance base",
	},
	
		-- Localized BG subzone name returned by GetSubZoneText() -> internal name used for sync
		-- Reverse of SubzonesToLocalized, filled below
	SubzonesToInternal = {},
	
		-- BG subzone name from CHAT_MSG_BG_SYSTEM_* messages -> internal name used for sync
	SubzonesMSGToInternal =
	{
		-- AV
		
		-- AB
		["farm"]        = "Farm",
		["blacksmith"]  = "Blacksmith",
		["mine"]        = "GoldMine",
		["lumber mill"] = "LumberMill",
		["stables"]     = "Stables",
		
		-- AC
			-- Neutral
		["East Moonwell"]          = "EastMoonwell",
		["West Moonwell"]          = "WestMoonwell",
		["Temple of Quel'Arkhana"] = "TempleOfQuelArkhana",
		
			-- Alliance towers
		["Height Zero"] = "HeightZero",
		["Monolith"]    = "Monolith",
		["Owl's Nest"]  = "OwlsNest",
		["Left Wing"]   = "LeftWing",
		["Right Wing"]  = "RightWing",
		
			-- Horde towers
		["Grommash Outpost"]  = "GrommashOutpost",
		["Blind Overlook"]    = "BlindOverlook",
		["Last Hearth"]       = "TheLastHearth",     -- system msg has "the", but not "The" as in the subzone name
		["Southern Ironpost"] = "SouthernIronpost",
		["Northern Ironpost"] = "NorthernIronpost",
	},
	
	-- Outside
	trigger_clickTime = "Hello World",
	bar_clickTime = "Battleground",
	
	-- All BGs
	trigger_gameStart60 = "The [Bb]attle for .+ begins in 1 minute%.",
	trigger_gameStart30 = "The [Bb]attle for .+ begins in 30 seconds%. Prepare yourselves!",
	trigger_gameStart60Arena = "1 minute until the Arena battle begins. Prepare yourselves!",
	trigger_gameStart30Arena = "30 seconds until the Arena battle begins.",
	bar_gameStart = "Game Start",
	-- not quite all the BGs (not used for WSG)
	misc_capping = "Opening",    -- spell name on cast bar when capping flags
	bar_capping = "%s (%s)",     -- e.g. "Stables (Playername)"
	
	-- WSG
	trigger_wsgFc     = "The .+ [Ff]lag was picked up by (.+)!",
	trigger_wsgFcDrop = "The .+ [Ff]lag was dropped by (.+)!",
	trigger_wsgFlagCap = ".+ captured the .+ flag!",
	
	bar_wsgFlagSpawn = "Flags spawn",
	bar_wsgResBar = "Resurrection",
	
	--[[AV
	Galv dead
	Bitch dead
	summons
	Coldtooth Mine
	Irondeep Mine
	
	Ivus the Forest Lord
	Lokholar the Icelord
	trigger_AidStationHordeClaim = "Hello World",
	trigger_StormpikeGyHordeClaim = "Hello World",
	trigger_StonehearthGyHordeClaim = "Hello World",
	trigger_SnowfallGyHordeClaim = "Hello World",
	trigger_IcebloodGyHordeClaim = "Hello World",
	trigger_ReliefHutHordeClaim = "Hello World",
	
	trigger_AidStationAllianceClaim = "Hello World",
	trigger_StormpikeGyAllianceClaim = "Hello World",
	trigger_StonehearthGyAllianceClaim = "Hello World",
	trigger_SnowfallGyAllianceClaim = "Hello World",
	trigger_IcebloodGyAllianceClaim = "Hello World",
	trigger_ReliefHutAllianceClaim = "Hello World",
	
	bar_avGyCap = " Gy Cap",
	
	trigger_IcebloodTowerHordeDefense = "Hello World",
	trigger_TowerpointHordeDefense = "Hello World",
	trigger_WestFrostwolfTowerHordeDefense = "Hello World",
	trigger_EastFrostwolfTowerHordeDefense = "Hello World",
	trigger_IcewingBunkerHordeCap = "Hello World",
	trigger_StonehearthBunkerHordeCap = "Hello World",
	trigger_DunBaldarSouthBunkerHordeCap = "Hello World",
	trigger_DunBaldarNorthBunkerHordeCap = "Hello World",
	
	trigger_IcebloodTowerAllianceCap = "Hello World",
	trigger_TowerpointAllianceCap = "Hello World",
	trigger_WestFrostwolfTowerAllianceCap = "Hello World",
	trigger_EastFrostwolfTowerAllianceCap = "Hello World",
	trigger_IcewingBunkerAllianceDefense = "Hello World",
	trigger_StonehearthBunkerAllianceDefense = "Hello World",
	trigger_DunBaldarSouthBunkerAllianceDefense = "Hello World",
	trigger_DunBaldarNorthBunkerAllianceDefense = "Hello World",

	bar_avTowerCap = " Tower Cap",
	]]--
	
	-- AB
		-- Claim
	trigger_abClaim = ".+ claims the (.+)! If left unchallenged, the .+ will control it in 1 minute!",
	
		-- Defend
	trigger_abDefense = ".+ has defended the (.+)",
	
		-- Assault
	trigger_abAssault = ".+ has assaulted the (.+)",
	
		-- Misc
	trigger_abVictory = "Hello World",
	bar_abVictoryEndTimer = "Victory",
	trigger_abDefeat = "Hello World",
	bar_abDefeatEndTimer = "Defeat",
	
	-- AC
		-- Claim
	trigger_acClaim = ".+ claims the (.+)! If left unchallenged, the .+ will control it in 3 minutes!",
	
		-- Assault
	trigger_acAssault = ".+ has assaulted the (.+)",
	
		-- Defense
	trigger_acDefense = ".+ has defended the (.+)",
	
		-- Cap
	trigger_acCap = "The .+ was taken by the .+!",
	
} end)

for k, v in L.SubzonesToLocalized do
	L.SubzonesToInternal[v] = k
end


----------------------------------
--      Module Declaration      --
----------------------------------

BigWigsBattlegrounds = BigWigs:NewModule(L["Battlegrounds"])
BigWigsBattlegrounds.revision = 20070
BigWigsBattlegrounds.external = true
BigWigsBattlegrounds.consoleCmd = L["Battlegrounds"]
BigWigsBattlegrounds.defaultDB = {
	bars = true,
	res = false,
}

BigWigsBattlegrounds.consoleOptions = {
	type = "group",
	name = L["Battlegrounds"],
	desc = L["Options for the Battlegrounds module."],
	args = {
		[L["Bars"]] = {
			type = "toggle",
			name = L["Bars"],
			desc = L["Toggle Battlegrounds bars on or off."],
			get = function() return BigWigsBattlegrounds.db.profile.bars end,
			set = function(v)
				BigWigsBattlegrounds.db.profile.bars = v
			end,
		},
		[L["Resurrection"]] = {
			type = "toggle",
			name = L["Resurrection"],
			desc = L["Toggle Battlegrounds Resurrection bars on or off.\n\nNote: these timers are often incorrect."],
			get = function() return BigWigsBattlegrounds.db.profile.res end,
			set = function(v)
				BigWigsBattlegrounds.db.profile.res = v
			end,
		},
	}
}


------------------------------
-- Variables     			--
------------------------------

-- locals
local timer = {
	-- Common
	gameStart60 = 60,
	gameStart30 = 30,
	res = 30,
	resDelay = 2,
	
	-- WSG
	wsgFlagSpawn = 23,
	wsgUpdateFC = 0.5,
	wsgUpdateEFC = 0.2,
	wsgGreyOutEFC = 2.5,
	
	-- AB
	abFlagCap = 60,
	
	--[[AV
	avGyCap = 240,
	avTowerCap = 240,
	]]--
	
	-- AC
	acFlagCap = 180,
}

local icon = {
	-- Common
	clickTime = "inv_misc_pocketwatch_01",
	gameStart = "inv_misc_pocketwatch_01",
	res = "spell_holy_resurrection",
	flagHorde = "INV_BannerPVP_01",
	flagAlliance = "INV_BannerPVP_02",
	capping = "trade_engineering",

	-- WSG
	
	-- AB
	abEndTimer = "inv_misc_pocketwatch_01",

	--[[AV
	avGyCapHorde = "INV_BannerPVP_01",
	avGyCapAlliance = "INV_BannerPVP_02",
	avTowerCapHorde = "INV_BannerPVP_01",
	avTowerCapAlliance = "INV_BannerPVP_02",
	]]--
	
	-- AC
}

local syncName = {
	res = "BattlegroundsRes",
	efc = "BattlegroundsEFC",
	capStart = "BattlegroundsCapStart",
	capStop = "BattlegroundsCapStop",
}

local color = {
	horde = "Red",
	alliance = "Blue",
	
	hordeGreyedOut = "9B1B1B",
	allianceGreyedOut = "090989",
	
	capping = "Green",
	
	res = "White",
	
	abVictory = "White",
	abDefeat = "Black",
}

local acNeutralBases = {
	EastMoonwell        = true,
	WestMoonwell        = true,
	TempleOfQuelArkhana = true,
}

local EventResDelayed
local EventRes


------------------------------
--      Initialization      --
------------------------------

function BigWigsBattlegrounds:OnRegister()
	for sync in syncName do
		syncName[sync] = syncName[sync] .. BigWigsBattlegrounds:GetVersionIdForSync()
	end
	PaintChips:RegisterHex(color.hordeGreyedOut)
	PaintChips:RegisterHex(color.allianceGreyedOut)
end

function BigWigsBattlegrounds:SetVariables()
	-- Setting this every time joining a BG in case of crossfaction BGs (although I don't know how they will work)
	if UnitFactionGroup("player") == "Horde" then
		self.colorOwnFaction = color.horde
		self.colorOppositeFaction = color.alliance
		self.colorOppositeFactionGreyedOut = color.allianceGreyedOut
	else
		self.colorOwnFaction = color.alliance
		self.colorOppositeFaction = color.horde
		self.colorOppositeFactionGreyedOut = color.hordeGreyedOut
	end
end

function BigWigsBattlegrounds:ResetVariables()
	self.allianceFc = nil
	self.hordeFc = nil
	self.curScanId = 0
	self.capping = false
	self.colorOwnFaction = nil
	self.colorOppositeFaction = nil
	self.colorOppositeFactionGreyedOut = nil
end

function BigWigsBattlegrounds:OnEnable()
	self:ResetVariables()
	self.acHighbornesRestGraveyardId = nil
	
	self:RegisterEvent("ZONE_CHANGED_NEW_AREA")  -- The event does not proc after reloading UI,
	self:ZONE_CHANGED_NEW_AREA()                 -- so call the handler manually. Will be called twice after relog though.
	
	self:ThrottleSync(0, syncName.capStart)  -- In case a player spams right click on a flag
	self:ThrottleSync(0, syncName.capStop)   -- while being constantly interrupted.
	
	self:ThrottleSync(0.5, syncName.efc)
end

function BigWigsBattlegrounds:OnSetup()
end


------------------------------
--      Event Handlers      --
------------------------------

--[[
Might use UPDATE_BATTLEFIELD_STATUS to track joining BGs instead.
But it seems to be less convenient for tracking zone changes if you are not removed from other queues upon joining a BG.
]]
function BigWigsBattlegrounds:ZONE_CHANGED_NEW_AREA()
	local zone = L.ZonesToInternal[GetRealZoneText()]
	if zone and GetBattlefieldInstanceExpiration() == 0 then  -- we are in a non-finished BG
		self:SetVariables()
		self:RegisterEvents(zone)
	else
		self:OnBattlegroundEnd()  -- when leaving by /afk self:UPDATE_BATTLEFIELD_STATUS won't call OnBattlegroundEnd
	end
end

function BigWigsBattlegrounds:RegisterEvents(bg)
	if bg ~= "Arena" then
		self:RegisterEvent("CHAT_MSG_BG_SYSTEM_NEUTRAL", function(msg)
			self:EventBGStartCountdown(msg)
			self["Event"..bg](self, msg)
		end)
		self:RegisterEvent("CHAT_MSG_BG_SYSTEM_HORDE", "Event"..bg)
		self:RegisterEvent("CHAT_MSG_BG_SYSTEM_ALLIANCE", "Event"..bg)
		self:RegisterEvent("BigWigs_RecvSyncBG", function(sync, rest, nick)
			self:DebugMessage("syncBG: " .. sync .. " rest: " .. (rest or "nil") .. " nick: " .. nick)
			self["BigWigs_RecvSyncBG_"..bg](self, sync, rest, nick)
		end)
	else
		self:RegisterEvent("CHAT_MSG_BG_SYSTEM_NEUTRAL", "EventBGStartCountdown")
	end
	self:RegisterEvent("UPDATE_BATTLEFIELD_STATUS")
	if bg == "WSG" then
		self:WSG_InitialScanForFC()
	end
	if bg == "AB" or bg == "AC" then
		self:RegisterEvent("MINIMAP_ZONE_CHANGED")
		self:MINIMAP_ZONE_CHANGED()  -- if we reloaded UI while in a BG the event won't proc until we change a subzone
	end
end

function BigWigsBattlegrounds:OnBattlegroundEnd()
	self:UnregisterAllEvents()
	self:RegisterEvent("ZONE_CHANGED_NEW_AREA")
	self:CancelAllScheduledEvents()
	self:RemoveBars()
	self:ResetVariables()
end

function BigWigsBattlegrounds:UPDATE_BATTLEFIELD_STATUS(msg)
	if GetBattlefieldInstanceExpiration() > 0 then  -- BG has ended
		self:OnBattlegroundEnd()
	end
end

function BigWigsBattlegrounds:EventBGStartCountdown(msg)
	--[[ TODO: 60 sec timer for arena does not work sometimes.
	This happens when CHAT_MSG_BG_SYSTEM_NEUTRAL procs first after loading, and only then ZONE_CHANGED_NEW_AREA.
	Maybe this happens when we are the last player to join,
	so the game starts instantly after joining, and only then our zone changes.
	]]
	if strfind(msg, L["trigger_gameStart60"]) or msg == L["trigger_gameStart60Arena"] then
		self:Bar(L["bar_gameStart"], timer.gameStart60, icon.gameStart)
	elseif strfind(msg, L["trigger_gameStart30"]) or msg == L["trigger_gameStart30Arena"] then
		self:AdjustBar(L["bar_gameStart"], timer.gameStart30, icon.gameStart)
	end
end

function BigWigsBattlegrounds:EventWSG(msg)
	-- Game start - prepare resurrection timers
	if strfind(msg, L["trigger_gameStart30"]) then
		self:ScheduleRepeatingResTimer("Resurrection")
		return
	end
	
	-- Flag was taken
	local _, _, FC = strfind(msg, L["trigger_wsgFc"])
	if FC then
		
		-- Horde picked up the Alliance flag
		if event == "CHAT_MSG_BG_SYSTEM_HORDE" then
			self.hordeFc = FC  -- TODO: replace with self.FC / self.EFC and get rid of "name" params in update FC / EFC funcs
			if UnitFactionGroup("player") == "Horde" then
				self:HPBar(FC, 100, icon.flagAlliance, true, color.horde)
				self:WSG_ScheduleUpdateFC(FC)
			else
				self:HPBar(FC, 100, icon.flagAlliance, true, color.hordeGreyedOut)
				self:WSG_ScheduleUpdateEFC(FC)
			end
		
		-- Alliance picked up the Horde flag
		else
			self.allianceFc = FC
			if UnitFactionGroup("player") == "Alliance" then
				self:HPBar(FC, 100, icon.flagHorde, true, color.alliance)
				self:WSG_ScheduleUpdateFC(FC)
			else
				self:HPBar(FC, 100, icon.flagHorde, true, color.allianceGreyedOut)
				self:WSG_ScheduleUpdateEFC(FC)
			end
		end
		self:SetCandyBarOnClick(self:BarId(FC), function() TargetByName(FC, true) end)
		return
	end
	
	-- Flag was dropped
	_, _, FC = strfind(msg, L["trigger_wsgFcDrop"])
	if FC then
		
		-- Enemy flag was dropped
		if event == "CHAT_MSG_BG_SYSTEM_HORDE"    and UnitFactionGroup("player") == "Alliance" or
		   event == "CHAT_MSG_BG_SYSTEM_ALLIANCE" and UnitFactionGroup("player") == "Horde" then
				self:CancelScheduledEvent("BigWigs_BattlegroundsUpdateFC")
				
		-- Own flag was dropped
		else
				self:CancelScheduledEvent("BigWigs_BattlegroundsUpdateEFC")
				self:CancelScheduledEvent("BigWigs_BattlegroundsGreyOutEFC")
		end
		
		self:RemoveBar(FC)
		
		return
	end
	
	-- Flag was captured
	if strfind(msg, L["trigger_wsgFlagCap"]) then
		self:CancelScheduledEvent("BigWigs_BattlegroundsUpdateFC")
		self:CancelScheduledEvent("BigWigs_BattlegroundsUpdateEFC")
		self:CancelScheduledEvent("BigWigs_BattlegroundsGreyOutEFC")
		self:RemoveBar(self.hordeFc)
		self:RemoveBar(self.allianceFc)
		self.hordeFc, self.allianceFc = nil, nil
		self:Bar(L["bar_wsgFlagSpawn"], timer.wsgFlagSpawn, icon.gameStart, true, "Black")
	end
end

function BigWigsBattlegrounds:EventAV(msg)
end

function BigWigsBattlegrounds:EventAB(msg)
	-- Claim
	local _, _, base = strfind(msg, L["trigger_abClaim"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		self:BarFlagAutoFaction(base, timer.abFlagCap)
		self:ScheduleDelayedResTimer(base, timer.abFlagCap + timer.resDelay)
		return
	end
	
	-- Assault
	_, _, base = strfind(msg, L["trigger_abAssault"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		self:CancelScheduledEvent(EventResDelayed(base))
		self:CancelScheduledEvent(EventRes(base))
		self:ScheduleDelayedResTimer(base, timer.abFlagCap + timer.resDelay)
		self:BarFlagAutoFaction(base, timer.abFlagCap)
		return
	end
	
	-- Defense
	_, _, base = strfind(msg, L["trigger_abDefense"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		self:CancelScheduledEvent(EventResDelayed(base))
		self:CancelScheduledEvent(EventRes(base))
		self:ScheduleDelayedResTimer(base, timer.resDelay)
		return
	end
	
	-- Time to victory/defeat
	if strfind(msg, L["trigger_abVictory"]) then
		self:Bar(L["bar_abVictoryEndTimer"], 60, icon.abEndTimer, true, color.abVictory)
	elseif strfind(msg, L["trigger_abDefeat"]) then
		self:Bar(L["bar_abDefeatEndTimer"], 60, icon.abEndTimer, true, color.abDefeat)
	end
end

function BigWigsBattlegrounds:EventAC(msg)
	-- Game start - prepare resurrection timers. 
	-- The horde timer is random it seems, so wrong. Need more data for the alliance one.
	if strfind(msg, L["trigger_gameStart30"]) then
		self:ScheduleEvent(function()
			self:ScheduleRepeatingResTimer("BaseHorde")
		end, 45)
		self:ScheduleRepeatingResTimer("BaseAlliance")
		return
	end
	
	-- Assault
	local _, _, base = strfind(msg, L["trigger_acAssault"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		if acNeutralBases[base] then
			self:CancelScheduledEvent(EventResDelayed("HighbornesRest"))
			self:CancelScheduledEvent(EventRes("HighbornesRest"))
			self:RemoveBar(L.SubzonesToLocalized["HighbornesRest"])
		end
		self:BarFlagAutoFaction(base, timer.acFlagCap)
		return
	end
	
	-- Defense
	_, _, base = strfind(msg, L["trigger_acDefense"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		if acNeutralBases[base] then
			self:AC_NeutralFlagsControlCheck()
		end
		return
	end
	
	-- Claim (Neutral bases)
	_, _, base = strfind(msg, L["trigger_acClaim"])
	if base then
		base = L.SubzonesMSGToInternal[base]
		self:RemoveBars(L.SubzonesToLocalized[base])
		self:BarFlagAutoFaction(base, timer.acFlagCap)
		return
	end
	
	-- Cap (Neutral bases)
	if strfind(msg, L["trigger_acCap"]) then
		self:AC_NeutralFlagsControlCheck()
	end
end

--[[
Events related to flag capping ("opening").
Marked with * were only tested on vmangos, but not on V+.
Started "opening":                SPELLCAST_START Opening 10000               // or different duration on V+
Got attacked:                     SPELLCAST_INTERRUPTED
Moved:                            SPELLCAST_STOP, SPELLCAST_INTERRUPTED       // slight delay before SPELLCAST_INTERRUPTED
Other "opened" before me:         SPELLCAST_FAILED                            // but only at the end of the cast
Other faction capped before me:   SPELLCAST_FAILED                        *   // but only at the end of the cast
Died:                             SPELLCAST_INTERRUPTED                   *
Finished "opening" and capped:    SPELLCAST_STOP
So SPELLCAST_START and SPELLCAST_INTERRUPTED are enough it seems.
Register them in subzones containing flags to cap.
For cases with late SPELLCAST_FAILED remove capping bars based on CHAT_MSG_BG_SYSTEM_* messages.
]]
function BigWigsBattlegrounds:MINIMAP_ZONE_CHANGED()
	if L.SubzonesToInternal[GetSubZoneText()] then
		self:RegisterEvent("SPELLCAST_START")
		self:RegisterEvent("SPELLCAST_INTERRUPTED")
	else
		if self:IsEventRegistered("SPELLCAST_START") then
		   self:UnregisterEvent  ("SPELLCAST_START")
		end
		if self:IsEventRegistered("SPELLCAST_INTERRUPTED") then
		   self:UnregisterEvent  ("SPELLCAST_INTERRUPTED")
		end
	end
end

function BigWigsBattlegrounds:SPELLCAST_START(name, duration)
	if name == L["misc_capping"] then
		local subzone = GetSubZoneText()
		local subzoneInternal = L.SubzonesToInternal[subzone]
		if subzoneInternal then
			self.capping = true
			self:SyncBG(syncName.capStart .. " " .. subzoneInternal .. " " .. duration)
		end
	end
end

function BigWigsBattlegrounds:SPELLCAST_INTERRUPTED()
	if self.capping then
		self.capping = false
	else
		return  -- ignore any cast interrupts except capping
	end
	local subzone = GetSubZoneText()
	local subzoneInternal = L.SubzonesToInternal[subzone]
	if subzoneInternal then
		self:SyncBG(syncName.capStop .. " " .. subzoneInternal)
	end
end


------------------------------
--      Synchronization	    --
------------------------------

function BigWigsBattlegrounds:SyncBG(sync)
	self:TriggerEvent("BigWigs_SendSyncBG", sync)
end

function BigWigsBattlegrounds:BigWigs_RecvSyncBG_WSG(sync, rest, nick)
	if strfind(sync, syncName.res) then
		self:SyncRes("Resurrection")
	elseif sync == syncName.efc then
		self:SyncEFC(SplitReply(rest))
	end
end

function BigWigsBattlegrounds:BigWigs_RecvSyncBG_AV(sync, rest, nick)
end

function BigWigsBattlegrounds:BigWigs_RecvSyncBG_AB(sync, rest, nick)
	local base, duration = SplitReply(rest)
	if strfind(sync, syncName.res) then
		self:SyncRes(base)
	elseif sync == syncName.capStart then
		self:SyncCapStart(base, duration, nick)
	elseif sync == syncName.capStop then
		self:SyncCapStop(base, nick)
	end
end

function BigWigsBattlegrounds:BigWigs_RecvSyncBG_AC(sync, rest, nick)
	local base, duration = SplitReply(rest)
	if strfind(sync, syncName.res) then
		self:SyncRes(base)
	--[[
	East Moonwell, West Moonwell, Temple of Quel'Arkhana have GetSubZoneText() == "Temple of Quel'Arkhana".
	So we can't distinguish between these 3 bases.
	Ignore them for now.
	TODO: see if it's possible to use coordinates to find the closest landmark in this case.
	]]
	elseif sync == syncName.capStart and base ~= "TempleOfQuelArkhana" then
		self:SyncCapStart(base, duration, nick)
	elseif sync == syncName.capStop  and base ~= "TempleOfQuelArkhana" then
		self:SyncCapStop(base, nick)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------

function BigWigsBattlegrounds:SyncRes(base)
	if not self:IsEventScheduled(EventRes(base)) then
		if self.db.profile.res then
			self:Bar(L.SubzonesToLocalized[base], timer.res, icon.res, true, color.res)
		end
		self:ScheduleRepeatingResTimer(base)
	end
end

function BigWigsBattlegrounds:SyncCapStart(base, duration, player)
	local text = format(L["bar_capping"], L.SubzonesToLocalized[base], player)
	--[[
	If we started capping the bar will appear without waiting for sync first.
	A bit later (depends on ping) we will get sync with ThrottleSync == 0, so it will go through.
	Check if the bar is already running to prevent it from rolling back by this sync.
	]]
	if not self:BarStatus(text) then
		self:Bar(text, duration / 1000, icon.capping, true, color.capping)
		self:SetCandyBarOnClick(self:BarId(text), function() TargetByName(player, true) end)
	end
end

function BigWigsBattlegrounds:SyncCapStop(base, player)
	self:RemoveBar(format(L["bar_capping"], L.SubzonesToLocalized[base], player))
end

function BigWigsBattlegrounds:SyncEFC(name, health)
	if not self:IsEventScheduled("BigWigs_BattlegroundsUpdateEFC") then
		if UnitFactionGroup("player") == "Horde" then
			self:HPBar(name, health, icon.flagHorde, true, color.alliance)
		else
			self:HPBar(name, health, icon.flagAlliance, true, color.horde)
		end
		self:WSG_ScheduleUpdateEFC(name)
	end
	if health ~= self:BarValue(name) then
		self:SetHPBar(name, 100 - health)
	end
	self:SetCandyBarColor(self:BarId(name), self.colorOppositeFaction)
	self:ScheduleEvent("BigWigs_BattlegroundsGreyOutEFC", self.WSG_GreyOutEFC, timer.wsgGreyOutEFC, self, name)
end


------------------------------
-- Utility Functions   		--
------------------------------

-- Temporary solution for "Bars" option
local BarOrig = BigWigsBattlegrounds.Bar
function BigWigsBattlegrounds:Bar(text, time, _icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	if not self.db.profile.bars and _icon ~= icon.res then
		return
	end
	BarOrig(self, text, time, _icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
end

-- Starts a bar with horde or alliance color and flag icon depending on event type
function BigWigsBattlegrounds:BarFlagAutoFaction(base, timer)
	if event == "CHAT_MSG_BG_SYSTEM_HORDE" then
		self:Bar(L.SubzonesToLocalized[base], timer, icon.flagHorde, true, color.horde)
	else
		self:Bar(L.SubzonesToLocalized[base], timer, icon.flagAlliance, true, color.alliance)
	end
end

function BigWigsBattlegrounds:ScheduleRepeatingResTimer(base)
	self:ScheduleRepeatingEvent(EventRes(base), self.ResTimerStart, timer.res, self, base)
end

function BigWigsBattlegrounds:ScheduleDelayedResTimer(base, delay)
	self:ScheduleEvent(EventResDelayed(base), function()
		self:ResTimerStart(base)
		self:ScheduleRepeatingResTimer(base)
	end, delay)
end

function BigWigsBattlegrounds:ResTimerStart(base)
	if self.db.profile.res then
		self:Bar(L.SubzonesToLocalized[base], timer.res, icon.res, true, color.res)
	end
	-- Can be 2 bases with almost the same res timers,
	-- so use different sync names to avoid setting ThrottleSync to 0.
	self:SyncBG(syncName.res .. "_" .. base .. " " .. base)
end

function EventResDelayed (base) return "BigWigs_BattlegroundsResDelayedTimerStart_" .. base end
function EventRes        (base) return "BigWigs_BattlegroundsResTimerStart_"        .. base end

-- Scans the raid for FC and starts a bar if needed.
-- Used if we entered WSG that is already running and has a FC.
function BigWigsBattlegrounds:WSG_InitialScanForFC()
	local faction = UnitFactionGroup("player")
	if GetWorldStateUIInfo(faction == "Horde" and 2 or 1) == 2 then  -- got a FC
		local iconFC = faction == "Horde" and icon.flagAlliance or icon.flagHorde
		local iconFCFull = "Interface\\Icons\\" .. iconFC
		for u = 1, GetNumRaidMembers() do  -- TODO: make it work if we are alone and GetNumRaidMembers() returns 0
			local unit = "raid" .. u
			local b = 1
			local buff = UnitBuff(unit, b)
			while buff do
				if buff == iconFCFull then
					local name = UnitName(unit)
					self:HPBar(name, 100, iconFC, true, self.colorOwnFaction)
					self:SetHPBar(name, 0.1)  -- otherwise if FC is at 100% HP the bar bugs out at 0 until FC's HP drops
					self:SetHPBar(name, 0)
					self:WSG_ScheduleUpdateFC(name)
					return
				end
				b = b + 1
				buff = UnitBuff(unit, b)
			end
		end
	end
end

function BigWigsBattlegrounds:WSG_ScheduleUpdateFC(name)
	self:SetHPBar(name, 0.1)
	self:SetHPBar(name, 0)
	self:WSG_UpdateFC(name)
	self:ScheduleRepeatingEvent("BigWigs_BattlegroundsUpdateFC", self.WSG_UpdateFC, timer.wsgUpdateFC, self, name)
end

function BigWigsBattlegrounds:WSG_UpdateFC(name)
	local id = RL:GetUnitIDFromName(name)
	if not id then return end  -- after reloading UI RL:GetUnitIDFromName returns nil for a second or so
	local healthCurrent = UnitHealth(id) / UnitHealthMax(id) * 100
	if healthCurrent ~= self:BarValue(name) then
		self:SetHPBar(name, 100 - healthCurrent)
	end
end

-- Takes one raid member and their pet (if exists) every timer.wsgUpdateEFC sec and
-- checks their target and target of target for EFC.
function BigWigsBattlegrounds:WSG_ScheduleUpdateEFC(name)
	self:SetHPBar(name, 0.1)
	self:SetHPBar(name, 0)
	self.curScanId = 0
	self:ScheduleRepeatingEvent("BigWigs_BattlegroundsUpdateEFC", self.WSG_UpdateEFC, timer.wsgUpdateEFC, self, name)
	self:ScheduleEvent("BigWigs_BattlegroundsGreyOutEFC", self.WSG_GreyOutEFC, timer.wsgGreyOutEFC, self, name)
end

-- Scans 2-4 (depending on pet presence) units per time for performance reasons.
function BigWigsBattlegrounds:WSG_UpdateEFC(name)
	self.curScanId = self.curScanId + 1
	if self.curScanId > GetNumRaidMembers() then
		self.curScanId = 1
	end
	
	local unit = "raid" .. self.curScanId
	if self:CheckUnitForEFC(unit .. "target",       name) then return end
	if self:CheckUnitForEFC(unit .. "targettarget", name) then return end
	
	if not UnitExists("raidpet" .. self.curScanId)  then return end
	
	unit = "raidpet" .. self.curScanId
	if self:CheckUnitForEFC(unit .. "target",       name) then return end
	if self:CheckUnitForEFC(unit .. "targettarget", name) then return end
end

function BigWigsBattlegrounds:CheckUnitForEFC(unit, name)
	if UnitName(unit) == name and UnitIsPlayer(unit) and UnitFactionGroup(unit) ~= UnitFactionGroup("player") then
		local healthCurrent = UnitHealth(unit) / UnitHealthMax(unit) * 100
		if healthCurrent ~= self:BarValue(name) then
			self:SetHPBar(name, 100 - healthCurrent)
		end
		self:SetCandyBarColor(self:BarId(name), self.colorOppositeFaction)
		self:ScheduleEvent("BigWigs_BattlegroundsGreyOutEFC", self.WSG_GreyOutEFC, timer.wsgGreyOutEFC, self, name)
		self:SyncBG(syncName.efc .. " " .. name .. " " .. healthCurrent)
		return true
	end
	return false
end

function BigWigsBattlegrounds:WSG_GreyOutEFC(name)
	self:SetCandyBarColor(self:BarId(name), self.colorOppositeFactionGreyedOut)
end

--[[
Checks control state for AC's Highborne's Rest graveyard.
Used after a moonwell or the temple was taken under control or defendeded.
Starts a resurrection bar if the graveyard is controlled by either faction.
]]
function BigWigsBattlegrounds:AC_NeutralFlagsControlCheck()
	local _, _, textureId = GetMapLandmarkInfo(self:AC_GetHighbornesRestGraveyardId())
	if textureId == 12 or textureId == 14 then  -- red (horde) or blue (alliance) graveyard sign
		self:ScheduleDelayedResTimer("HighbornesRest", timer.resDelay)
	end
end

function BigWigsBattlegrounds:AC_GetHighbornesRestGraveyardId()  -- 4 (but might change if changes are made to AC)
	if self.acHighbornesRestGraveyardId then
		return self.acHighbornesRestGraveyardId
	end
	for i = 1, GetNumMapLandmarks() do
		local name = GetMapLandmarkInfo(i)
		if name == L.SubzonesToLocalized["HighbornesRest"] then
			self.acHighbornesRestGraveyardId = i
			return i
		end
	end
end

-- Returns parameters from the reply string where they were separated by whitespaces
function SplitReply(reply)
	local res = {}
	local i = 1
	for param in string.gfind(reply, "[^ ]+") do
		res[i] = param
		i = i + 1
	end
	return unpack(res)
end
