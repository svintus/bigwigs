assert(BigWigs, "BigWigs not found!")

------------------------------
--      Are you local?      --
------------------------------

local throt, times = {}, {}
local playerName = nil

local coreSyncs = {
	["BossEngaged"] = 5,
}

----------------------------------
--      Module Declaration      --
----------------------------------

BigWigsComm = BigWigs:NewModule("Comm")
BigWigsComm.revision = 2

------------------------------
--      Initialization      --
------------------------------

function BigWigsComm:OnRegister()
	playerName = UnitName("player")

	for k, v in pairs(coreSyncs) do
		self:BigWigs_ThrottleSync(k, v)
	end
end

function BigWigsComm:OnEnable()
	self:RegisterEvent("CHAT_MSG_ADDON")
	self:RegisterEvent("BigWigs_SendSync")
	self:RegisterEvent("BigWigs_SendSyncBG")
	self:RegisterEvent("BigWigs_ThrottleSync")
end


------------------------------
--      Event Handlers      --
------------------------------

function BigWigsComm:CHAT_MSG_ADDON(prefix, message, type, sender)
	if prefix ~= "BigWigs" or not (type == "RAID" or type == "BATTLEGROUND") then return end

	local _, _, sync, rest = string.find(message, "(%S+)%s*(.*)$")
	if not sync then return end

	if throt[sync] == nil then throt[sync] = 1 end
	if throt[sync] == 0 or not times[sync] or (times[sync] + throt[sync]) <= GetTime() then
		if type == "RAID" then
			self:TriggerEvent("BigWigs_RecvSync", sync, rest, sender)
		else
			self:TriggerEvent("BigWigs_RecvSyncBG", sync, rest, sender)
		end
		times[sync] = GetTime()
	end
end


function BigWigsComm:BigWigs_SendSync(msg, channel)
	local _, _, sync, rest = string.find(msg, "(%S+)%s*(.*)$")

	if not sync then return end

	if throt[sync] == nil then throt[sync] = 1 end
	if throt[sync] == 0 or not times[sync] or (times[sync] + throt[sync]) <= GetTime() then
		channel = channel or "RAID"
		SendAddonMessage("BigWigs", msg, channel)
		self:CHAT_MSG_ADDON("BigWigs", msg, channel, playerName)
	end
end


function BigWigsComm:BigWigs_SendSyncBG(msg)
	self:BigWigs_SendSync(msg, "BATTLEGROUND")
end


function BigWigsComm:BigWigs_ThrottleSync(msg, time)
	assert(msg, "No message passed")
	throt[msg] = time
end

function BigWigsComm:GetThrottleTable() return throt end
function BigWigsComm:GetTimesTable() return times end

