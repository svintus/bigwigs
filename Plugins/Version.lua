assert(BigWigs, "BigWigs not found!")

local BWL = nil
local BZ = AceLibrary("Babble-Zone-2.2")
local L = AceLibrary("AceLocale-2.2"):new("BigWigsVersionQuery")
local tablet = AceLibrary("Tablet-2.0")
local dewdrop = AceLibrary("Dewdrop-2.0")

local COLOR_GREEN = "00ff00"
local COLOR_RED = "ff0000"
local COLOR_WHITE = "ffffff"

-- I changed these names several times and still don't like them, but nothing good comes to mind -_-
-- Used instead of query nick
local WAS_NO_QUERY__VERSION = "was_no_query__version"  -- When sending version upon joining a raid.
local WAS_NO_QUERY__RELOAD  = "was_no_query__reload"   -- When reloading UI or using ForceQuit (used by "Exit now" button),
                                                       -- the character stays in the game at least for some time
													   -- with addons not working.

local UPDATE_PERIOD = 60

local DEBUG_coreIsUpToDate_true      = "BigWigsVersionQuery.coreIsUpToDate = |cFF00FF00true|r"
local DEBUG_coreIsUpToDate_false     = "BigWigsVersionQuery.coreIsUpToDate = |cFFFF0000false|r"
local DEBUG_highestCoreWithKTM_true  = "BigWigsVersionQuery.highestCoreWithKTM = |cFF00FF00true|r"
local DEBUG_highestCoreWithKTM_false = "BigWigsVersionQuery.highestCoreWithKTM = |cFFFF0000false|r"

local GetNumRaidMembersOnline
local GetNumRaidOfficers
local GetNumRaidOfficersOnline
local KTMIsPresent
local SplitReply

---------------------------------
--      Localization           --
---------------------------------

L:RegisterTranslations("enUS", function() return {
	["versionquery"] = true,
	["Version Query"] = true,
	["Commands for querying the raid for Big Wigs versions."] = true,
	["Query already running, please wait 5 seconds before trying again."] = true,
	["An internal query is already running, please wait up to 5 seconds before your one starts."] = true,
	["Querying versions for "] = true,
	["Cancelling the current query because of joining a raid."] = true,
	["Cancelling the current query because of initial syncs launching."] = true,
	["Big Wigs Version Query"] = true,
	["Close window"] = true, -- I know, it's really a Tablet.
	["Showing version for "] = true,
	["Green versions are newer than yours, red are older, and white are the same."] = true,
	["Player"] = true,
	["Version"] = true,
	["Current zone"] = true,
	["<zone>"] = true,
	["Version query done."] = true,
	["Runs a version query on your current zone."] = true,
	["Closes the version query window."] = true,
	["current"] = true,
	["Runs a version query on the given zone."] = true,
	["Zone"] = true,
	["zone"] = true,
	["N/A"] = true,
	["BigWigs"] = true,
	["Runs a version query on the BigWigs core."] = true,
	["Nr Replies"] = true,
	["Ancient"] = true,
            
    ["Your version of Big Wigs VanillaPlus is out of date!\nPlease visit https://bitbucket.org/svintus/bigwigs/get/master.zip to get the latest version (rename the folder to \"BigWigs\")."] = true,
    ["Your version of BigWigs VanillaPlus is out of date!\nPlease visit the website to get the latest version (rename the folder to \"BigWigs\")."] = true,
    ["Close"] = true,
    ["Cancel"] = true,
	
	["Gathering Data, please wait..."] = true,
	["<BigWigs> Everyone has the current version of BigWigs VanillaPlus. I'm proud of you!"] = true,
	["<BigWigs> Players without BigWigs VanillaPlus:"] = true,
	["<BigWigs> no one"] = true,
	["<BigWigs> Players with an outdated version of BigWigs VanillaPlus:"] = true,
	["<BigWigs> Players using a version of BigWigs intended for a different server:"] = true,
	["<BigWigs> Please download the newest version of BigWigs VanillaPlus from https://bitbucket.org/svintus/bigwigs/get/master.zip (rename the folder to \"BigWigs\")"] = true,
    ["Notify old versions"] = true,
	["Lists players with an old version in raid chat."] = true,
	["notifyOldVersions"] = true,
} end )

L:RegisterTranslations("deDE", function() return {
	["versionquery"] = "Versionsabfrage",
	["Version Query"] = "Versionsabfrage",
	["Commands for querying the raid for Big Wigs versions."] = "Kommandos um den Schlachtzug nach verwendeten BigWigs Versionen abzufragen.",
	["Query already running, please wait 5 seconds before trying again."] = "Abfrage läuft bereits, bitte 5 Sekunden warten bis zum nächsten Versuch.",
	["Querying versions for "] = "Frage Versionen ab für ",
	["Big Wigs Version Query"] = "BigWigs Versionsabfrage",
	["Close window"] = "Schlie\195\159e Fenster", -- I know, it's really a Tablet.
	["Showing version for "] = "Zeige Version für ",
	["Green versions are newer than yours, red are older, and white are the same."] = "Grüne Versionen sind neuer, rote sind älter, wei\195\159e sind gleich.",
	["Player"] = "Spieler",
	["Version"] = "Version",
	["Current zone"] = "Momentane Zone",
	["<zone>"] = "<zone>",
	["Version query done."] = "Versionsabfrage beendet.",
	["Runs a version query on your current zone."] = "Versionsabfrage für die momentane Zone starten.",
	["Closes the version query window."] = "Schlie\195\159t das Versionsabfrage-Fenster.",
	["current"] = "gegenw\195\164rtig",
	["Runs a version query on the given zone."] = "Versionsabfrage in für eine gegebene Zone starten.",
	["Zone"] = "Zone",
	["zone"] = "Zone",
	["N/A"] = "N/A",
	["BigWigs"] = "BigWigs",
	["Runs a version query on the BigWigs core."] = "Versionsabfrage für die BigWigs Kernkomponente starten.",
	["Nr Replies"] = "Anzahl der Antworten",
	["Ancient"] = "Alt",
            
    ["Your version of Big Wigs VanillaPlus is out of date!\nPlease visit https://bitbucket.org/svintus/bigwigs/get/master.zip to get the latest version (rename the folder to \"BigWigs\")."] = "Deine Version von Big Wigs VanillaPlus ist veraltet! Bitte downloade die neuste Version von https://bitbucket.org/svintus/bigwigs/get/master.zip",
    ["Close"] = "Schliessen",
    ["Cancel"] = "Abbrechen",
	
	["Gathering Data, please wait..."] = "Daten werden gesammelt, bitte warten...",
	["<BigWigs> Everyone has the current version of BigWigs VanillaPlus. I'm proud of you!"] = "<BigWigs> Alle haben die aktuelle Version von BigWigs VanillaPlus. Ich bin stolz auf euch!",
	["<BigWigs> Players without BigWigs VanillaPlus:"] = "<BigWigs> Spieler ohne BigWigs VanillaPlus:",
	["<BigWigs> no one"] = "<BigWigs> Niemand",
	["<BigWigs> Players with an outdated version of BigWigs VanillaPlus:"] = "<BigWigs> Spieler mit einer veralteten Version von BigWigs VanillaPlus:",
	["<BigWigs> Players using a version of BigWigs intended for a different server:"] = "<BigWigs> Spieler welche eine Version von BigWigs verwenden, welche für einen anderen Server gedacht ist:",
	["<BigWigs> Please download the newest version of BigWigs VanillaPlus from https://bitbucket.org/svintus/bigwigs/get/master.zip (rename the folder to \"BigWigs\")"] = "<BigWig> Bitte downloade die neuste Version von BigWigs VanillaPlus von https://bitbucket.org/svintus/bigwigs/get/master.zip",
	["Notify old versions"] = "Alte Versionen abfragen",
	["Lists players with an old version in raid chat."] = "Liste alle Spieler mit einer alten Version im Raidchat auf.",
	["notifyOldVersions"] = "alteVersionenAbfragen",
} end )


---------------------------------
--      Addon Declaration      --
---------------------------------

BigWigsVersionQuery = BigWigs:NewModule("Version Query")
BigWigsVersionQuery.revision = 20007

BigWigsVersionQuery.consoleCmd = L["versionquery"]
BigWigsVersionQuery.consoleOptions = {
	type = "group",
	name = L["Version Query"],
	desc = L["Commands for querying the raid for Big Wigs versions."],
	args = {
		[L["BigWigs"]] = {
			type = "execute",
			name = L["BigWigs"],
			desc = L["Runs a version query on the BigWigs core."],
			func = function() BigWigsVersionQuery:QueryVersion("BigWigs") end,
		},
		[L["current"]] = {
			type = "execute",
			name = L["Current zone"],
			desc = L["Runs a version query on your current zone."],
			func = function() BigWigsVersionQuery:QueryVersion() end,
		},
		[L["zone"]] = {
			type = "text",
			name = L["Zone"],
			desc = L["Runs a version query on the given zone."],
			usage = L["<zone>"],
			get = false,
			set = function(zone) BigWigsVersionQuery:QueryVersion(zone) end,
		},
		[L["notifyOldVersions"]] = {
			type = "execute",
			name = L["Notify old versions"],
			desc = L["Lists players with an old version in raid chat."],
			func = function() BigWigsVersionQuery:NotifyOldVersions() end,
		},
	}
}

------------------------------
--      Initialization      --
------------------------------

function BigWigsVersionQuery:OnEnable()
	self.queryRunning = nil
	self.responseTable = {}
	--[[
	keys = online raid members' names who have BigWigs the same fork
	values =
	{
		rev = their core revision,
		ktm = their ktm presence. 0 or 1.
	}
	]]
	self.responseTableCore = {} 
	self.zoneRevisions = nil
	self.currentZone = ""
	self.OutOfDateShown = false
	
	self.raidOfficersCache = {}
	
	self.isHiddenQuery = false -- reset after /console reloadui
	self.queryPending = false
	self.zonePending = nil
	self.blamePending = false
	self.queryPendingHidden = false
	
	self.numRaidMembers = 0
	self.numRaidMembersOnline = 0
	self.numRaidOfficers = 0
	self.numRaidOfficersOnline = 0
	self.raidLeader = nil
	
	--[[
	For use in other modules.
	True if IsServerRegisteredForAnyServerProject() and none of RL/Assistants with the same fork has a higher core version.
	Is used to allow putting marks and resetting KTM only to officers with highest core version.
	So that if a new version changed something related to one of those 2, officers with an outdated version don't interfere.
	
	Changing of this to true during a core version query will be delayed till its end.
	]]
	self.coreIsUpToDate = true
	
	--[[
	There is a certain case when preventing KTM resetting using coreIsUpToDate causes an issue:
		officers with the highest core version do simply do not have KTM.
	In that case nobody with this fork will reset KTM.
	highestCoreWithKTM should solve this problem.
	]]
	
	--[[
	True if:
		IsServerRegisteredForAnyServerProject() and
		none of RL/Assistants with the same fork and higher core version has KTM and
		we have KTM.
	Designed to solve the issue described above.
	]]
	self.highestCoreWithKTM = KTMIsPresent() ~= 0
	
	BWL = AceLibrary("AceLocale-2.2"):new("BigWigs")
	
	self:RegisterEvent("RAID_ROSTER_UPDATE")
	--self:RegisterEvent("CHAT_MSG_SYSTEM")
	self:RegisterEvent("PLAYER_LOGOUT")  -- procs on reload in this module, but not on logout
	self:RegisterEvent("BigWigs_RecvSync")
	self:TriggerEvent("BigWigs_ThrottleSync", "BWVQ", 0)
	self:TriggerEvent("BigWigs_ThrottleSync", "BWVR", 0)
	self:TriggerEvent("BigWigs_ThrottleSync", "BWVR2", 0)
	
	self:PopulateRevisions()
	
	--[[
	-- After /reload delay is not needed but InitialSyncs() are needed. They are run from here and work fine (although with a delay).
	-- After relog delay is needed but InitialSyncs() will usually or always be called from RAID_ROSTER_UPDATE (Joined (user or other player)) first. CHAT_MSG_SYSTEM sometimes procs too early.
	-- After joining a raid (user):
	-- 1. BW was turned off -> delay is needed but InitialSyncs() will usually or always be called from RAID_ROSTER_UPDATE. CHAT_MSG_SYSTEM does not proc.
	-- 2. BW was turned on -> CHAT_MSG_SYSTEM procs too early. InitialSyncs() will be called from RAID_ROSTER_UPDATE.
	]]
	self:ScheduleEvent(BigWigsVersionQuery.DelayedInitialization, 1, self)
end

function BigWigsVersionQuery:PopulateRevisions()
	self.zoneRevisions = {}
	for name, module in self.core:IterateModules() do
		if module:IsBossModule() and module.zonename and type(module.zonename) == "string" then
			-- Make sure to get the enUS zone name.
			local zone = BZ:HasReverseTranslation(module.zonename) and BZ:GetReverseTranslation(module.zonename) or module.zonename
			-- Get the abbreviated name from BW Core.
			local zoneAbbr = BWL:HasTranslation(zone) and BWL:GetTranslation(zone) or nil
			if not self.zoneRevisions[zone] or module.revision > self.zoneRevisions[zone] then
				self.zoneRevisions[zone] = module.revision
			end
			if zoneAbbr and (not self.zoneRevisions[zoneAbbr] or module.revision > self.zoneRevisions[zoneAbbr]) then
				self.zoneRevisions[zoneAbbr] = module.revision
			end
		end
	end
	self.zoneRevisions["BigWigs"] = self.core.revision
end

--[[
-- When OnEnable() procs upon joining a raid info filled by UpdateRaidRosterInfo() might have weird results.
-- Usually it has zeros (which also isn't okay), but I had one case when everything but numRaidMembers was zero.
-- So need to delay the whole initializaion for 1 sec, not just version syncs.
-- But usually RAID_ROSTER_UPDATE will proc first (with UpdateRaidRosterInfo() having correct data already) and do what this function is supposed to do.
-- So this function is mostly needed after reload or when enabling BigWigs while already in a raid since RAID_ROSTER_UPDATE won't proc then.
]]
function BigWigsVersionQuery:DelayedInitialization()
	if GetNumRaidMembers() > 0 and not self:IsEventScheduled("BWVQPeriodicVersionCheck") then
		self:InitialSyncs(L["Cancelling the current query because of initial syncs launching."])
		self:StartRepeatingVersionCheck()
	end
end

function BigWigsVersionQuery:InitialSyncs(cancelReason)
	if self.queryRunning then
		if not self.isHiddenQuery then
			self.core:Print(cancelReason)
		end
		self:CancelScheduledEvent("BWVQDone")
		self.queryRunning = nil
	end
	-- send our version to everyone
	self:Sync("BWVR2 " .. self.zoneRevisions["BigWigs"] .. " " .. WAS_NO_QUERY__VERSION .. " " .. self.core.forkName)
	self:Sync("BWVR2 " .. self.zoneRevisions["BigWigs"] .. " " .. WAS_NO_QUERY__VERSION .. " " .. self.core.forkName .. " " .. KTMIsPresent())
	-- and ask for others' ones
	self:QueryVersionCoreHidden()
end

------------------------------
--      Event Handlers      --
------------------------------

function BigWigsVersionQuery:UpdateTablet()
    if not tablet:IsRegistered("BigWigs_VersionQuery") then
		tablet:Register("BigWigs_VersionQuery",
			"children", function() tablet:SetTitle(L["Big Wigs Version Query"])
				self:OnTooltipUpdate() end,
			"clickable", true,
			"showTitleWhenDetached", true,
			"showHintWhenDetached", true,
			"cantAttach", true,
			"menu", function()
					dewdrop:AddLine(
						"text", L["BigWigs"],
						"tooltipTitle", L["BigWigs"],
						"tooltipText", L["Runs a version query on the BigWigs core."],
						"func", function() self:QueryVersion("BigWigs") end)
					dewdrop:AddLine(
						"text", L["Current zone"],
						"tooltipTitle", L["Current zone"],
						"tooltipText", L["Runs a version query on your current zone."],
						"func", function() self:QueryVersion() end)
					dewdrop:AddLine(
						"text", L["Notify old versions"],
						"tooltipTitle", L["Notify old versions"],
						"tooltipText", L["Lists players with an old version in raid chat."],
						"func", function() self:NotifyOldVersions() end)
					dewdrop:AddLine(
						"text", L["Close window"],
						"tooltipTitle", L["Close window"],
						"tooltipText", L["Closes the version query window."],
						"func", function() tablet:Attach("BigWigs_VersionQuery"); dewdrop:Close() end)
				end
		)
	end
	if tablet:IsAttached("BigWigs_VersionQuery") then
		tablet:Detach("BigWigs_VersionQuery")
	else
		tablet:Refresh("BigWigs_VersionQuery")
	end
end

function BigWigsVersionQuery:UpdateVersions()
    -- only check if this version is outdated if it is the same fork
	for name, data in pairs(self.responseTable) do
		if not self.zoneRevisions then return end
		if data.forkName and data.forkName == self.core.forkName and self.zoneRevisions[self.currentZone] and data.rev > self.zoneRevisions[self.currentZone] then
			self:IsOutOfDate()
		end
	end
    
	if self.queryRunning and not self.isHiddenQuery then
        self:UpdateTablet()
    end
end

--[[ Does not change coreIsUpToDate to true while core version query is going because we can briefly get a "wrong" true
when got answers with version <= ours while higher version is present but we haven't got any answer with it yet. ]]
function BigWigsVersionQuery:CheckCoreVersion()
	if not BigWigs:IsServerRegisteredForAnyServerProject() then
		self.coreIsUpToDate = false
		BigWigs:DebugMessage(DEBUG_coreIsUpToDate_false, self)
		self.highestCoreWithKTM = false
		BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_false, self)
		return
	end
	self:RaidOfficersCacheFill()
	local gotOfficerWithHigherVersion = false
	for name, data in self.responseTableCore do
		if self:IsCoreVersionHigher(name, data.rev) then
			gotOfficerWithHigherVersion = true
			self.coreIsUpToDate = false
			BigWigs:DebugMessage(DEBUG_coreIsUpToDate_false, self)
			if data.ktm ~= 0 or KTMIsPresent() == 0 then
				self.highestCoreWithKTM = false
				BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_false, self)
				self:RaidOfficersCacheClean()
				return
			end
		end
	end
	self:RaidOfficersCacheClean()
	--[[
	Possible cases why we got here:
	
	1. No officers had a higher version.
	What happened:
		coreIsUpToDate was not changed,
		highestCoreWithKTM was not changed.
	Actions:
		coreIsUpToDate = true,
		highestCoreWithKTM does not matter but can be set depending on KTMIsPresent result for consistency.
	
	2. At least one officer had a higher version but no officers with higher versions had KTM while we have it.
	What happened:
		coreIsUpToDate was set to false,
		highestCoreWithKTM was not changed.
	Actions:
		coreIsUpToDate remains false,
		highestCoreWithKTM = depending on KTMIsPresent result.
	]]
	if not (self.queryRunning and self.currentZone == "BigWigs") then
		if not gotOfficerWithHigherVersion then
			self.coreIsUpToDate = true
			BigWigs:DebugMessage(DEBUG_coreIsUpToDate_true, self)
		end
		if KTMIsPresent() == 0 then
			self.highestCoreWithKTM = false
			BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_false, self)
		else
			self.highestCoreWithKTM = true
			BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_true, self)
		end
	end
end

function BigWigsVersionQuery:IsOutOfDate()
	if not self.OutOfDateShown then
		self.OutOfDateShown = true
		BigWigs:Print(L["Your version of Big Wigs VanillaPlus is out of date!\nPlease visit https://bitbucket.org/svintus/bigwigs/get/master.zip to get the latest version (rename the folder to \"BigWigs\")."])
        
        local dialog = nil
        StaticPopupDialogs["BigWigsOutOfDateDialog"] = {
            text = L["Your version of BigWigs VanillaPlus is out of date!\nPlease visit the website to get the latest version (rename the folder to \"BigWigs\")."],
            button1 = L["Close"],
            button2 = L["Cancel"],
            OnAccept = function()
                StaticPopup_Hide ("BigWigsOutOfDateDialog")
            end,
            OnCancel = function()
                StaticPopup_Hide ("BigWigsOutOfDateDialog")
            end,
            OnShow = function (self, data)
                local editbox = getglobal(this:GetName().."WideEditBox")
                editbox:SetText("https://bitbucket.org/svintus/bigwigs/get/master.zip")
                editbox:SetWidth(250)
                editbox:ClearFocus()
                editbox:HighlightText() 
                --self.editBox:SetText("Some text goes here")
                getglobal(this:GetName().."Button2"):Hide()
            end,
            hasEditBox = true,
            hasWideEditBox = true,
            maxLetters = 62,
            --EditBox:setText("Text"),
            timeout = 0,
            whileDead = true,
            hideOnEscape = true,
            preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
            hegiht = 700,
        }
        local dialog = StaticPopup_Show ("BigWigsOutOfDateDialog")
        
	end
end


local function Blame()
	-- no BigWigs at all
	local noBigWigs = nil
	local outdated = nil
	local differentFork = nil
	
	if not BigWigsVersionQuery.responseTable then
		BigWigs:Print("that didn't work...")		
		return
	end

	for i = 1, GetNumRaidMembers(), 1 do
		local name = UnitName("Raid" .. i)
		local data = BigWigsVersionQuery.responseTable[name]

		
		if data and data.rev and (data.forkName and data.forkName == BigWigs.forkName or not data.forkName) then
			-- has bigwigs
			if BigWigs.revision and data.rev < BigWigs.revision then
				-- bigwigs is out of date
				if not outdated then
					outdated = name
				else
					outdated = outdated .. ", " .. name
				end
			end
		elseif data and data.forkName and data.forkName ~= BigWigs.forkName then
			-- different fork
			if not differentFork then
				differentFork = name
			else
				differentFork = differentFork .. ", " .. name
			end
		else
			-- does not have bigwigs
			if not noBigWigs then
				noBigWigs = name
			else
				noBigWigs = noBigWigs .. ", " .. name
			end
		end
	end
	
	-- inform raid
	if not noBigWigs and not outdated and not differentFork then
		SendChatMessage(L["<BigWigs> Everyone has the current version of BigWigs VanillaPlus. I'm proud of you!"], "RAID")
	else
		if noBigWigs then
			SendChatMessage(L["<BigWigs> Players without BigWigs VanillaPlus:"], "RAID")
			SendChatMessage("<BigWigs> " .. noBigWigs, "RAID")
		end
		
		if outdated then
			SendChatMessage(L["<BigWigs> Players with an outdated version of BigWigs VanillaPlus:"], "RAID")
			SendChatMessage("<BigWigs> " .. outdated, "RAID")
		end
		
		if differentFork then
			SendChatMessage(L["<BigWigs> Players using a version of BigWigs intended for a different server:"], "RAID")
			SendChatMessage("<BigWigs> " .. differentFork, "RAID")
		end
		
		SendChatMessage(L["<BigWigs> Please download the newest version of BigWigs VanillaPlus from https://bitbucket.org/svintus/bigwigs/get/master.zip (rename the folder to \"BigWigs\")"], "RAID")
	end
end

function BigWigsVersionQuery:NotifyOldVersions()
	if not self:QueryVersion("BigWigs", true) then
		return
	end
	self:ScheduleEvent(Blame, 5)
	BigWigs:Print(L["Gathering Data, please wait..."])
end

function BigWigsVersionQuery:OnTooltipUpdate()
	local zoneCat = tablet:AddCategory(
		"columns", 1,
		"text", L["Zone"],
		"child_justify1", "LEFT"
	)
	zoneCat:AddLine("text", self.currentZone)
	local playerscat = tablet:AddCategory(
		"columns", 1,
		"text", L["Nr Replies"],
		"child_justify1", "LEFT"
	)
	playerscat:AddLine("text", self.responses)
	local cat = tablet:AddCategory(
		"columns", 2,
		"text", L["Player"],
		"text2", L["Version"],
		"child_justify1", "LEFT",
		"child_justify2", "RIGHT"
	)
	for name, data in pairs(self.responseTable) do
		if data.rev == -1 then -- bigwigs installed but module not found
			cat:AddLine("text", name, "text2", "|cff"..COLOR_RED..L["N/A"].."|r")
		elseif data.forkName and data.forkName ~= self.core.forkName then -- different fork
			cat:AddLine("text", name, "text2", "|cff" .. COLOR_WHITE .. data.rev .. " (" .. data.forkName .. ")|r")
		else -- out of date or different fork
			local color = COLOR_WHITE
			if self.zoneRevisions[self.currentZone] and data.rev > self.zoneRevisions[self.currentZone] then
				if data.forkName == self.core.forkName then
					color = COLOR_GREEN
					self:IsOutOfDate()
				end
			elseif self.zoneRevisions[self.currentZone] and data.rev < self.zoneRevisions[self.currentZone] then
				color = COLOR_RED
			end
			if data.forkName then
				cat:AddLine("text", name, "text2", "|cff"..color..data.rev.." ("..data.forkName..")|r")
			else
				cat:AddLine("text", name, "text2", "|cff"..color..data.rev.."|r")
			end
		end
	end

	tablet:SetHint(L["Green versions are newer than yours, red are older, and white are the same."])
end

function BigWigsVersionQuery:QueryVersionAndShowWindow(zone)
    self:QueryVersion(zone)
	self:UpdateVersions()
end
function BigWigsVersionQuery:QueryVersion(zone, blame)
	if self.queryRunning then  -- was user-called QueryVersion since hidden ones cancel or wait for the current query
		if self.isHiddenQuery then    -- there can be chain hidden queries
			self.queryPending = true  -- so push a non-hidden one next for the user
			self.zonePending = zone
			self.blamePending = blame or false
			self.core:Print(L["An internal query is already running, please wait up to 5 seconds before your one starts."])
		else
			self.core:Print(L["Query already running, please wait 5 seconds before trying again."])
		end
		return false
	end
	if not zone or zone == "" or type(zone) ~= "string" then zone = GetRealZoneText() end
	-- If this is a shorthand zone, convert it to enUS full.
	-- Also, if this is a shorthand, we can't really know if the user is enUS or not.

	if not BWL then BWL = AceLibrary("AceLocale-2.2"):new("BigWigs") end
	if BWL ~= nil and zone ~= nil and type(zone) == "string" and BWL:HasReverseTranslation(zone) then
		zone = BWL:GetReverseTranslation(zone)
		-- If there is a translation for this to GetLocale(), get it, so we can
		-- print the zone name in the correct locale.
		if BZ:HasTranslation(zone) then
			zone = BZ:GetTranslation(zone)
		end
	end
    
	if not zone then
		error("The given zone is invalid.")
		return false
	end

	-- ZZZ |zone| should be translated here.
	if not self.isHiddenQuery then
        self.core:Print(L["Querying versions for "].."|cff"..COLOR_GREEN..zone.."|r.")
    end

	-- If this is a non-enUS zone, convert it to enUS.
	if BZ:HasReverseTranslation(zone) then zone = BZ:GetReverseTranslation(zone) end

	self.currentZone = zone

	self.queryRunning = true
	self:ScheduleEvent("BWVQDone", function()
		self.queryRunning = nil
		if not self.isHiddenQuery then
			self.core:Print(L["Version query done."])
		end
		self.isHiddenQuery = false
		if self.currentZone == "BigWigs" then
			self:CheckCoreVersion()  -- since setting coreIsUpToDate to true was paused during the core version query
		end
		if self.queryPending then
			self.queryPending = false
			if self.blamePending then
				self.blamePending = false
				self:NotifyOldVersions()
			else
				self:QueryVersion(self.zonePending)
			end
			self.zonePending = nil
		elseif self.queryPendingHidden then
			self.queryPendingHidden = false
			self:PeriodicVersionCheck()
		end
	end, 5)

	self.responseTable = {}
	if zone == "BigWigs" then
		self.responseTableCore = {}
		-- we are going to run a core version query, so delay the periodic repeating one by UPDATE_PERIOD sec.
		if self:IsEventScheduled("BWVQPeriodicVersionCheck") then  -- I guess this is always true here but not 100% sure
			self:StartRepeatingVersionCheck()
		end
	end

	if not self.zoneRevisions[zone] then
		self.responseTable[UnitName("player")] = { rev = -1, forkName = self.core.forkName }
	else
		self.responseTable[UnitName("player")] = { rev = self.zoneRevisions[zone], forkName = self.core.forkName }
	end
	self.responses = 1
	self:TriggerEvent("BigWigs_SendSync", "BWVQ " .. zone)
	return true
end

function BigWigsVersionQuery:QueryVersionCoreHidden()
	self:UpdateRaidRosterInfo()
	self.isHiddenQuery = true
	self:QueryVersion("BigWigs")
end

--[[ Parses the newest style reply, which is "<rev> <nick> <forkName> [<ktm>] ..."
So it accepts the original "BWVR2" style reply which is "<rev> <nick> <forkName>",
newer one which is "<rev> <nick> <forkName> <ktm>",
and possible future styles with more params separated by a whitespace.
Unsupported extra params will just be ignored.

A reply with more than 3 params will be ignored by clients using the original ParseReply3 and BigWigs_RecvSync.
So we first need to send a reply using the original "BWVR2" style with 3 params for compatibility, and then ours with 3+ params.

I think the proper solution for this would be adding a new reply style supporting 3 and more params.
That would be 4th style already, similar to this "BWVR2", with a lot of repeating code I suppose.
I just do not want to bother with that, at least for now.
]]
function BigWigsVersionQuery:ParseReply3(reply)
	-- If there's no space, it's just a version number we got.
	-- Instead of strfind(reply, "(.+) (.+) (.+) (.+"), splitting by whitespace works fine if there are more or less params than expected.
	local rev, nick, fork, ktm = SplitReply(reply)
	if not rev or not nick or not fork then 
		return reply, nil, nil
	end

	return tonumber(rev), nick, fork, tonumber(ktm) or 0
end

--[[ Parses the new style reply, which is "1111 <nick>" ]]
function BigWigsVersionQuery:ParseReply2(reply)
	-- If there's no space, it's just a version number we got.
	local first, last = string.find(reply, " ")
	if not first or not last then return reply, nil end

	local rev = string.sub(reply, 1, first)
	local nick = string.sub(reply, last + 1, string.len(reply))

	-- We need to check if rev or nick contains ':' - if it does, this is an old
	-- style reply.
	if tonumber(rev) == nil or string.find(rev, ":") or string.find(nick, ":") then
		return self:ParseReply(reply), nil
	end
	return tonumber(rev), nick
end

--[[ Parses the old style reply, which was MC:REV BWL:REV, etc. ]]
function BigWigsVersionQuery:ParseReply(reply)
	if not string.find(reply, ":") then return -1 end
	local zone = BWL:HasTranslation(self.currentZone) and BWL:GetTranslation(self.currentZone) or self.currentZone

	local zoneIndex, zoneEnd = string.find(reply, zone)
	if not zoneIndex then return -1 end

	local revision = string.sub(reply, zoneEnd + 2, zoneEnd + 6)
	local convertedRev = tonumber(revision)
	if revision and convertedRev ~= nil then return convertedRev end

	return -1
end

--[[
-- Version reply syntax history:
--  Old Style:           MC:REV BWL:REV ZG:REV
--  First Working Style: REV
--  New Style:           REV QuereeNick
--]]

function BigWigsVersionQuery:BigWigs_RecvSync(sync, rest, nick)
	if sync == "BWVR2" and nick and rest then
		-- reply received
		local revision, queryNick, fork, ktm = self:ParseReply3(rest)
		if queryNick == UnitName("player") and self.queryRunning then
			if not self.responseTable[nick] then
				self.responses = self.responses + 1
			end
			self.responseTable[nick] = { rev = tonumber(revision), forkName = fork }
			self:UpdateVersions()
			if self.currentZone == "BigWigs" and fork == self.core.forkName then
				self.responseTableCore[nick] = { rev = revision, ktm = ktm }
				if self:IsCoreVersionHigher(nick, revision) then
					self.coreIsUpToDate = false
					BigWigs:DebugMessage(DEBUG_coreIsUpToDate_false, self)
					if ktm ~= 0 then
						self.highestCoreWithKTM = false
						BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_false, self)
					end
				end
			end
		-- a player sent own version after joining the raid
		elseif queryNick == WAS_NO_QUERY__VERSION and nick ~= UnitName("player") and fork == self.core.forkName then
			self.responseTable[nick] = { rev = tonumber(revision), forkName = fork }
			self:UpdateVersions()  -- TODO: can be an issue if self.currentZone ~= "BigWigs" (we are running a zone query)
			self.responseTableCore[nick] = { rev = revision, ktm = ktm }
			if self:UnitByNameIsOfficer(nick) then
				if revision > self.zoneRevisions["BigWigs"] then
					self.coreIsUpToDate = false
					BigWigs:DebugMessage(DEBUG_coreIsUpToDate_false, self)
					if ktm ~= 0 then
						self.highestCoreWithKTM = false
						BigWigs:DebugMessage(DEBUG_highestCoreWithKTM_false, self)
					end
				elseif revision < self.zoneRevisions["BigWigs"] then
					self:CheckCoreVersion()
				end
			end
		-- someone is reloading UI
		elseif queryNick == WAS_NO_QUERY__RELOAD then
			self.responseTableCore[nick] = nil
			if self:IsCoreVersionHigher(nick, revision) then
				self:CheckCoreVersion()
			end
		end
	elseif sync == "BWVQ" and nick ~= UnitName("player") and rest then
		-- query, send reply
		if not self.zoneRevisions[rest] then
			self:TriggerEvent("BigWigs_SendSync", "BWVR2 -1 ".. nick .. " " .. self.core.forkName)
			self:TriggerEvent("BigWigs_SendSync", "BWVR2 -1 ".. nick .. " " .. self.core.forkName .. " " .. KTMIsPresent())
			self:TriggerEvent("BigWigs_SendSync", "BWVR -1 ".. nick)
		else
			self:TriggerEvent("BigWigs_SendSync", "BWVR2 " .. self.zoneRevisions[rest] .. " " .. nick .. " " .. self.core.forkName)
			self:TriggerEvent("BigWigs_SendSync", "BWVR2 " .. self.zoneRevisions[rest] .. " " .. nick .. " " .. self.core.forkName .. " " .. KTMIsPresent())
			self:TriggerEvent("BigWigs_SendSync", "BWVR " .. self.zoneRevisions[rest] .. " " .. nick)
		end
	elseif sync == "BWVR" and self.queryRunning and nick and rest then
		-- reply received
		
		-- Means it's either a old style or new style reply.
		-- The "working style" is just the number, which was the second type of
		-- version reply we had.
		local revision, queryNick = nil, nil, nil
		if tonumber(rest) == nil then
			revision, queryNick = self:ParseReply2(rest)
		else
			revision = tonumber(rest)
		end
		if queryNick == nil or queryNick == UnitName("player") then
			if not self.responseTable[nick] then
				self.responseTable[nick] = { rev = tonumber(revision), forkName = nil }
				self.responses = self.responses + 1
			end
			self:UpdateVersions()
		end
	end
end

--[[
What happens on Alt-F4. (home = MaNGOS Zero 7a4f9d937147 2023-04-19 09:07:46 +0100 (HEAD branch))

Does RAID_ROSTER_UPDATE proc?
home: yes
V+:   yes but only when the char disappears

Is the player shown as offline?
home: no (dirrerent instances)
      yes (same instance), but after a delay. On RAID_ROSTER_UPDATE proc the player is still online
V+:   yes (not in visibility range), but only when the char disappears. On RAID_ROSTER_UPDATE proc the player is already offline
      yes (in visibility range), but after a delay. On RAID_ROSTER_UPDATE proc the player is still online
]]

--[[
What happens on logout. (home = MaNGOS Zero 7a4f9d937147 2023-04-19 09:07:46 +0100 (HEAD branch))

Does RAID_ROSTER_UPDATE proc?
home: yes
V+:   yes

Is the player shown as offline?
home: no (dirrerent instances)
      yes (same instance), but after a delay. On RAID_ROSTER_UPDATE proc the player is still online
V+:   yes (not in visibility range)
      yes (in visibility range)

What does GetNumRaidMembersOnline() show?
home: online
V+:   offline (not in visibility range actually)
      online (in visibility range)

Is it updated 1 sec after RAID_ROSTER_UPDATE procced or no?
home: no (dirrerent instances)
      yes (same instance)
V+:   yes (was initially correct) (not in visibility range)
      yes (in visibility range)
]]

--[[
Note: there is a hypothetical rare scenario when we sent "BigWigs" query (and cleared responseTableCore),
and then an officer went offline or left just before sending an answer.
In this case their revision in self.responseTableCore will be be left as nil. (note 1)
]]

function BigWigsVersionQuery:RAID_ROSTER_UPDATE()
	-- =======  Joined (user or other player)  =======
	-- TODO: what happens if the user joined a bg? Maybe it's fine but need to check
	if GetNumRaidMembers() > self.numRaidMembers then
		if self.numRaidMembers == 0 then  -- user joined, including auto joining after going online sometimes
			--[[
			-- If BWVQPeriodicVersionCheck is already running
			(probably impossible case if it took > 1 sec for RAID_ROSTER_UPDATE to proc after OnEnable(), and DelayedInitialization() had GetNumRaidMembers() > 0):
			-- InitialSyncs() is needed because the one before RAID_ROSTER_UPDATE was probably bad since the user was not considered to be in the raid back then.
			-- So run it and and it will cancel the current query even if it is hidden.
			]]
			BigWigsVersionQuery:InitialSyncs(L["Cancelling the current query because of joining a raid."])
			if not self:IsEventScheduled("BWVQPeriodicVersionCheck") then
				self:StartRepeatingVersionCheck()
			end
		else  -- other player joined -> should send version automatically
			self:UpdateRaidRosterInfo()
		end
	
	-- =======  Left (other player)  =======
	-- (if the user left the raid BigWigs will turn off and RAID_ROSTER_UPDATE won't proc)
	-- TODO: what happens if the user left a bg? Maybe it's fine but need to check
	elseif GetNumRaidMembers() < self.numRaidMembers then
		local raidMembers = { }
		for i = 1, GetNumRaidMembers() do
			raidMembers[GetRaidRosterInfo(i)] = true
		end
		for name in self.responseTableCore do
			-- this player with the same fork left
			-- can't know for sure if that was an officer with a higher version because of (note 1)
			if not raidMembers[name] then
				self.responseTableCore[name] = nil
				break
			end
		end
		local officersBefore = self.numRaidOfficers
		self:UpdateRaidRosterInfo()
		if self.numRaidOfficers < officersBefore then  -- an officer left
			self:CheckCoreVersion()
		end
	
	-- =======  Changed the leader or gave/removed assist or one of them left  =======
	-- RAID_ROSTER_UPDATE procs only once when the user comes online and automatically becomes a new leader in a raid where everyone was offline.
	elseif self:UnitByNameIsOfficer(self.raidLeader) ~= 2 or GetNumRaidOfficers() ~= self.numRaidOfficers then
		self:UpdateRaidRosterInfo()
		self:CheckCoreVersion()
	
	-- =======  Went online (other player)  =======
	-- Should send version automatically
	elseif GetNumRaidMembersOnline() > self.numRaidMembersOnline and self.numRaidMembersOnline > 0 then
		self:UpdateRaidRosterInfo()
	
	-- =======  Went online (user)  =======
	-- See upper (Joined (user or other player))
	
	-- =======  Went offline (other player)  =======
	--[[ 
	No need to check for (GetNumRaidMembersOnline() == 0) (the user left the raid instead of someone going offline),
	because BigWigs turns off upon leaving and RAID_ROSTER_UPDATE won't proc. 
	]]
	--[[
	On V+ if a player who is in visibility range goes offline UpdateRaidRosterInfo() doesn't have not have updated info on RAID_ROSTER_UPDATE proc and still sees the player as online.
	In this case we don't know if someone has actually left or no.
	So we won't get into this "elseif" block.
	Possible options in this case:
	1. Leave it like this because everything else except players going offline seems to work fine.
	2. Leave PeriodicVersionCheck() every UPDATE_PERIOD sec only.
	3. Run QueryVersionCoreHidden() every time when RAID_ROSTER_UPDATE procs with 1 sec delay, queryPendingHidden = true if any query is already running. PeriodicVersionCheck() seems to be not needed in this case?
	4. Leave it like this but add "else" block with QueryVersionCoreHidden() with 1 sec delay, queryPendingHidden = true if any query is already running. Though it will also run on changing players' positions, loot rules and maybe something else.
	Chose option 4.
	In this case missing this "elseif" block can spoil other ones below (if there are any) on the next RAID_ROSTER_UPDATE.
	For example it can be assists/RL change, but we will get into this "elseif" instead if it goes first because by that time the player who went offline might be already be shown as offline.
	So this "elseif" should be the last before "else".
	]]
	elseif GetNumRaidMembersOnline() < self.numRaidMembersOnline then
		for i = 1, GetNumRaidMembers() do
			local name, _, _, _, _, _, _, online = GetRaidRosterInfo(i)
			-- a player with the same fork went offline
			-- can't know for sure if that was an officer with a higher version because of (note 1)
			if not online and self.responseTableCore[name] then
				self.responseTableCore[name] = nil
				break
			end
		end
		local officersOnlineBefore = self.numRaidOfficersOnline
		self:UpdateRaidRosterInfo()
		if self.numRaidOfficersOnline < officersOnlineBefore then  -- an officer went offline
			self:CheckCoreVersion()
		end
	
	-- =======  Went offline (other player)  =======
	-- In case we did not get into the upper "elseif" block when a player was in visibility range and did logout.
	-- Though this block will also run on changing players' positions, loot rules and maybe something else.
	else
		self:ScheduleEvent(function()
			if not self.queryRunning then
				self:QueryVersionCoreHidden()
			else
				self.queryPendingHidden = true
			end
		end, 1)
	end
end

-- Procs on reload, but not on logout
-- Tells others that we are reloading
function BigWigsVersionQuery:PLAYER_LOGOUT()
	self:Sync("BWVR2 " .. self.zoneRevisions["BigWigs"] .. " " .. WAS_NO_QUERY__RELOAD .. " " .. self.core.forkName)
	self:Sync("BWVR2 " .. self.zoneRevisions["BigWigs"] .. " " .. WAS_NO_QUERY__RELOAD .. " " .. self.core.forkName .. " " .. KTMIsPresent())
end

local ForceQuitOrig = ForceQuit
function ForceQuit()
	ForceQuitOrig()
	BigWigsVersionQuery:PLAYER_LOGOUT()  -- this code is actually executed even after calling ForceQuitOrig
end


------------------------------
--      Utility	Functions   --
------------------------------

function BigWigsVersionQuery:StartRepeatingVersionCheck()
	self:ScheduleRepeatingEvent("BWVQPeriodicVersionCheck", self.PeriodicVersionCheck, UPDATE_PERIOD, self)
end

-- Runs a version query in case we missed something when using RAID_ROSTER_UPDATE
-- Can be useful when a player did Alt-F4 but the character still stays in the game for several minutes
function BigWigsVersionQuery:PeriodicVersionCheck()
	if not self.queryRunning then	
		self:QueryVersionCoreHidden()
	elseif not self.isHiddenQuery then
		self.queryPendingHidden = true
	end
end

function BigWigsVersionQuery:IsCoreVersionHigher(name, rev)
	return rev > self.zoneRevisions["BigWigs"] and self:UnitByNameIsOfficer(name)
end

function BigWigsVersionQuery:RaidOfficersCacheFill()
	for i = 1, GetNumRaidMembers() do
		local name, rank = GetRaidRosterInfo(i)
		if rank > 0 then
			self.raidOfficersCache[name] = rank
		end
	end
end

function BigWigsVersionQuery:RaidOfficersCacheClean()
	for name in self.raidOfficersCache do
		self.raidOfficersCache[name] = nil
	end
end

function BigWigsVersionQuery:UpdateRaidRosterInfo()
	self.numRaidMembers = GetNumRaidMembers()
	self.numRaidMembersOnline = 0
	self.numRaidOfficers = 0
	self.numRaidOfficersOnline = 0
	self.raidLeader = nil
	for i = 1, self.numRaidMembers do
		local name, rank, _, _, _, _, _, online = GetRaidRosterInfo(i)
		if online then
			self.numRaidMembersOnline = self.numRaidMembersOnline + 1
		end
		if rank > 0 then  -- assistant or leader
			self.numRaidOfficers = self.numRaidOfficers + 1
			if online then
				self.numRaidOfficersOnline = self.numRaidOfficersOnline + 1
			end
			if rank == 2 then  -- leader
				self.raidLeader = name
			end
		end
	end
end

function GetNumRaidMembersOnline()
	local count = 0
	for i = 1, GetNumRaidMembers() do
		if UnitIsConnected("raid"..i) then
			count = count + 1
		end
	end
	return count
end

-- Returns the amount of players with assist or leader
function GetNumRaidOfficers()
	local count = 0
	for i = 1, GetNumRaidMembers() do
		local _, rank = GetRaidRosterInfo(i)
		if rank > 0 then
			count = count + 1
		end
	end
	return count
end

-- Returns the amount of online players with assist or leader
function GetNumRaidOfficersOnline()
	local count = 0
	for i = 1, GetNumRaidMembers() do
		local _, rank, _, _, _, _, _, online = GetRaidRosterInfo(i)
		if rank > 0 and online then
			count = count + 1
		end
	end
	return count
end

-- Returns 2 for leader, 1 for assistant, false otherwise
function BigWigsVersionQuery:UnitByNameIsOfficer(name)
	if next(self.raidOfficersCache) ~= nil then
		return self.raidOfficersCache[name] or false
	end
	for i = 1, GetNumRaidMembers() do
		local n, rank = GetRaidRosterInfo(i)
		if name == n then
			return rank > 0 and rank
		end
	end
	return false
end

-- Returns 1 if klhtm.net.clearraidthreat is available, 0 otherwise.
function KTMIsPresent()
	return klhtm and klhtm.net and klhtm.net.clearraidthreat and 1 or 0
end

-- Returns parameters from the reply string where they were separated by whitespaces
function SplitReply(reply)
	local res = {}
	local i = 1
	for param in string.gfind(reply, "[^ ]+") do
		res[i] = param
		i = i + 1
	end
	return unpack(res)
end
