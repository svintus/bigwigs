
assert(BigWigs, "BigWigs not found!")

--[[
	Support for OtherProject Project (OtherProject.tld)
	
	Servers:
		- OtherProjectServer1
		- OtherProjectServer2

	Server name has to be the value you get from GetRealmName()
]]

local project = "VanillaPlus"
BigWigs:RegisterServer(project, "Hogger")

-- Supported Boss Modules
