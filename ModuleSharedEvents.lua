assert(BigWigs, "BigWigs not found!")

local deformat = AceLibrary("Deformat-2.0")
local L = AceLibrary("AceLocale-2.2"):new("BigWigs ModuleSharedEvents")

----------------------------
--      Localization      --
----------------------------

--[[
TODO: when addons are loaded check if other addons (KTM and such) have modified some of the global strings.
If they did then modify the corresponding paterns here too for better precision in problematic strings.
Like ".+'s" to ".+ 's".
Although it is not possible to modify the translation strings for a registered translation,
so will need to find a different way.
]]

L:RegisterTranslations("enUS", function() return {
	["trigger_hitYou"] = "Your (.+) hits (.+) for",
	["trigger_critYou"] = "Your (.+) crits (.+) for",
	["trigger_hitOther"] = ".+'s (.+) hits (.+) for",
	["trigger_critOther"] = ".+'s (.+) crits (.+) for",
	["trigger_interruptYou"] = "You interrupt (.+%S) ?'s .+%.",    -- For example when using Counterspell, Feral Charge
	["trigger_interruptOther"] = ".+ interrupts (.+%S) ?'s .+%.",  -- Same
	["trigger_justiceYou"] = "Your Judgement of Justice failed. (.+) is immune%.",   -- Immune to the debuff,
	["trigger_justiceOther"] = ".+'s Judgement of Justice fails. (.+) is immune%.",  -- not necessarily to the interrupt.
	["misc_interruptAbilitiesCommon"] =
	{
		["Kick"] = true,
		["Pummel"] = true,
		["Shield Bash"] = true,
		["Earth Shock"] = true,
		["Whirling Axe"] = true,
	},
} end)

--------------------------------
--      Class Prototype       --
--------------------------------

local ModuleSharedEvents = AceLibrary("AceOO-2.0").Class("AceEvent-2.0")

ModuleSharedEvents.prototype.revision = 1

BigWigs.ModuleSharedEvents = ModuleSharedEvents

---------------------------------------------------------------------------

function ModuleSharedEvents.prototype:init(module)
	ModuleSharedEvents.super.prototype.init(self)
	self.module = module
	self:RegisterEvent("Ace2_AddonEnabled")
end

function ModuleSharedEvents.prototype:ToString()
	return self.module:ToString() .. " ModuleSharedEvents"
end

---------------------------------------------------------------------------

function ModuleSharedEvents.prototype:Ace2_AddonEnabled(module)
	if self.module == module then
		self:UnregisterEvent("Ace2_AddonEnabled")
		self:RegisterEvent("Ace2_AddonDisabled")
		-- workaround to trigger OnSetup if enabled manually
		BigWigs:SetupModule(self.module:ToString())
	end
end

-- Deactivating a module unregisters its events, but not the events from module.moduleEvents.
-- So unregister them manually.
function ModuleSharedEvents.prototype:Ace2_AddonDisabled(module)
	if self.module == module then
		self:UnregisterAllEvents()
		self:RegisterEvent("Ace2_AddonEnabled")
	end
end

---------------------------------------------------------------------------

-- Registered in module:SetupModule
function ModuleSharedEvents.prototype:PLAYER_REGEN_DISABLED()
	self.module:CheckForEngage()
end

--[[
module:Engage starts periodic CheckForWipe checks, so this calling it
in PLAYER_REGEN_ENABLED and CHAT_MSG_COMBAT_FRIENDLY_DEATH is not nessessary it seems.
Especially if many people die at once, a lot of scans for being in combat might cause micro lag.
Might change in future if SendWipeSync is implemented and module:Engage does not start CheckForWipe anymore.
]]

-- Registered in module:Engage
function ModuleSharedEvents.prototype:PLAYER_REGEN_ENABLED()
	--self.module:CheckForWipe()
end

-- Registered in module:Engage
function ModuleSharedEvents.prototype:CHAT_MSG_COMBAT_FRIENDLY_DEATH(msg)
	--self.module:CheckForWipe(msg)
	self:CheckForUnitDeath(msg)
end

-- Registered in module:Engage
function ModuleSharedEvents.prototype:CHAT_MSG_COMBAT_HOSTILE_DEATH(msg)
	self.module:CheckForBossDeath(msg)
	if self.interruptibleBars then
		self:CheckForInterrupt_Death(msg)
	end
end

---------------------------------------------------------------------------

-- Called by ModuleSharedEvents.prototype:CHAT_MSG_COMBAT_FRIENDLY_DEATH
function ModuleSharedEvents.prototype:CheckForUnitDeath(msg)
	local name = deformat(msg, UNITDIESOTHER)  -- "%s dies."
	if name then
		self.module:SendUnitDeathSync(name)
	elseif msg == UNITDIESSELF then            -- "You die."
		self.module:SendUnitDeathSync(UnitName("player"))
	end
end

---------------------------------------------------------------------------

-- Registered in module:RegisterBarForInterrupts
function ModuleSharedEvents.prototype:CheckForInterrupt_CommonYou(msg)
	local _, _, ability, mob = strfind(msg, L["trigger_hitYou"])
	if ability and mob then
		if L["misc_interruptAbilitiesCommon"][ability] and self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
	_, _, ability, mob = strfind(msg, L["trigger_critYou"])
	if ability and mob then
		if L["misc_interruptAbilitiesCommon"][ability] and self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
	_, _, mob = strfind(msg, L["trigger_interruptYou"])
	if mob then
		if self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
end

-- Registered in module:RegisterBarForInterrupts
function ModuleSharedEvents.prototype:CheckForInterrupt_CommonOther(msg)
	local _, _, ability, mob = strfind(msg, L["trigger_hitOther"])
	if ability and mob then
		if L["misc_interruptAbilitiesCommon"][ability] and self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
	_, _, ability, mob = strfind(msg, L["trigger_critOther"])
	if ability and mob then
		if L["misc_interruptAbilitiesCommon"][ability] and self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
	_, _, mob = strfind(msg, L["trigger_interruptOther"])
	if mob then
		if self.interruptibleBars[mob] then
			self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
		end
		return
	end
end

-- Registered in module:RegisterBarForInterrupts
function ModuleSharedEvents.prototype:CheckForInterrupt_JudgementYou(msg)
	local _, _, mob = strfind(msg, L["trigger_justiceYou"])
	if mob and self.interruptibleBars[mob] then
		self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
	end
end

-- Registered in module:RegisterBarForInterrupts
function ModuleSharedEvents.prototype:CheckForInterrupt_JudgementOther(msg)
	local _, _, mob = strfind(msg, L["trigger_justiceOther"])
	if mob and self.interruptibleBars[mob] then
		self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
	end
end

-- Called by ModuleSharedEvents.prototype:CHAT_MSG_COMBAT_HOSTILE_DEATH
function ModuleSharedEvents.prototype:CheckForInterrupt_Death(msg)
	local mob = deformat(msg, UNITDIESOTHER)  -- "%s dies."
	if self.interruptibleBars[mob] then
		self.module:SendMobInterruptSync(self.interruptibleBars[mob].mobReverseTranslation)
	end
end

---------------------------------------------------------------------------

-- Registered in module:Engage
function ModuleSharedEvents.prototype:BigWigs_RecvSync(sync, rest, nick)
	local _, _, bossSyncOneWord, name = strfind(rest, "(%S+) (.+)")
	if bossSyncOneWord ~= self.module.bossSyncOneWord then
		return
	end
	if strfind(sync, self.module:GetUnitDeathSync()) then
		self.module:UnitDeath(name)
	elseif strfind(sync, self.module:GetMobInterruptSync()) then
		self.module:Interrupt(name)
	end
end
