------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.onyxia.onyxia
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20022 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = { "flamebreath", "deepbreath", "wingbuffet", "fireballYou", "fireballOthers", "fireballBar", "fireballIcons", "phase", "onyfear", "gaze", "announce", "icon", "bosskill" }

 module.defaultDB = {
	fireballOthers = false,
 }


-- locals

module.timer = {
	firstFear = 9.3,
	fear = 29.5,
	fearCast = 1.5,
	wingbuffet = 1,
	flamebreath = 2,
	deepbreath = 5,
	fireball = 3,
	fireballDelay = 0.5,
	gaze = 8,
}
local timer = module.timer

module.icon = {
	wingbuffet = "INV_Misc_MonsterScales_14",
	fear = "Spell_Shadow_Possession",
	deepbreath = "Spell_Fire_SelfDestruct",
	deepbreath_sign = "Spell_Fire_Lavaspawn",
	fireball = "Spell_Fire_FlameBolt",
	flamebreath = "Spell_Fire_Fire",
	gaze = "Ability_Hunter_AspectMastery",
}
local icon = module.icon

module.syncName = {
	deepbreath = "OnyDeepBreath",
	phase2 = "OnyPhaseTwo",
	phase3 = "OnyPhaseThree",
	flamebreath = "OnyFlameBreath",
	fireball = "OnyFireball",
	fear = "OnyBellowingRoar",
	gaze = "OnyGaze",
	gazeStop = "OnyGazeStop",
}
local syncName = module.syncName

module.transitioned = false
module.phase = 0
module.iconNumber = 8


------------------------------
-- Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.phase2 then
		self:Phase2()
	elseif sync == syncName.phase3 then
		self:Phase3()
	elseif sync == syncName.deepbreath then
		self:DeepBreath()
	elseif sync == syncName.flamebreath then
		self:FlameBreath()
	elseif sync == syncName.fireball then
		self:ScheduleEvent(self.Fireball, timer.fireballDelay, self)
	elseif sync == syncName.fear then
		self:Fear()
	elseif sync == syncName.gaze and rest then
		self:Gaze(rest)
	elseif sync == syncName.gazeStop and rest then
		self:GazeGone(rest)
	end
end


------------------------------
-- Sync Handlers	    --
------------------------------
function module:Phase2()
	if module.phase < 2 then
		module.transitioned = true --to stop sending new syncs
		module.phase = 2
		if self.db.profile.phase then
			self:Message(L["msg_phase2"], "Important", false, "Alarm")
		end
	end
end

function module:Phase3()
	if self.db.profile.phase and module.phase < 3 then
		self:Message(L["msg_phase3"], "Important", true, "Beware")
		self:Bar(L["bar_fearNext"], timer.firstFear + timer.fearCast, icon.fear, true, "Orange")
		module.phase = 3
	end
end

function module:DeepBreath()
	if self.db.profile.deepbreath then
		self:Message(L["msg_deepBreath"], "Important", true, "RunAway")
		self:Bar(L["bar_deepBreath"], timer.deepbreath, icon.deepbreath, true, BigWigsColors.db.profile.significant)
		self:WarningSign(icon.deepbreath_sign, timer.deepbreath, true)
	end
end

function module:FlameBreath()
	if self.db.profile.flamebreath then
		self:Bar(L["bar_flameBreath"], timer.flamebreath, icon.flamebreath)
	end
end

function module:Fireball()
	local name
	for i = 1, GetNumRaidMembers() do
		if UnitName("raid"..i.."target") == self.translatedName then
			name = UnitName("raid"..i.."targettarget")
			break
		end
		if UnitName("raidpet"..i.."target") == self.translatedName then
			name = UnitName("raid"..i.."targettarget")
			break
		end
	end
	
	if not name then  -- This happened on phase 3 transition
		name = ""     -- Does she finish casting Fireball in this case? If not then just return instead
	end
	
	if name == UnitName("player") then
		if self.db.profile.fireballYou then
			self:Message(L["msg_fireballYou"], "Important", true, "Alert")
			self:Message(format(L["msg_fireballOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.fireball, 3)
		end
	elseif self.db.profile.fireballOthers then
		self:Message(format(L["msg_fireballOther"], name), "Attention")
	end
	
	if self.db.profile.fireballBar then
		self:Bar(format(L["bar_fireball"], name), timer.fireball - timer.fireballDelay, icon.fireball)
	end
	
	if self.db.profile.fireballIcons then
		self:Icon(name, self.iconNumber, 14)
	end
	
	self.iconNumber = self.iconNumber - 1
	if self.iconNumber < 4 then
		self.iconNumber = 8
	end
end

function module:Fear()
	if self.db.profile.onyfear then
		self:RemoveBar(L["bar_fearNext"]) -- remove timer bar

		self:Message(L["msg_fear"], "Important", true, "Alarm")
		self:Bar(L["bar_fearCast"], timer.fearCast, icon.fear, true, "Orange") -- add cast bar
		self:DelayedBar(timer.fearCast, L["bar_fearNext"], timer.fear, icon.fear, true, "Orange") -- delayed timer bar
		self:WarningSign(icon.fear, 5)
	end
end

function module:Gaze(name)
	if self.db.profile.gaze then
		self:Bar(string.format(L["bar_gaze"], name), timer.gaze, icon.gaze, true, BigWigsColors.db.profile.significant)
		if name == UnitName("player") then
			self:Message(L["msg_gazeYou"], "Personal", true, "Alert")
			self:Message(format(L["msg_gazeOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.gaze, timer.gaze)
		else
			self:Message(string.format(L["msg_gazeOther"], name), "Attention")
		end
	end
	
	if self.db.profile.icon then
		self:Icon(name, -1, timer.gaze)
	end
	
	if self.db.profile.announce then
		self:TriggerEvent("BigWigs_SendTell", name, L["msg_gazeWhisper"])
	end
end

function module:GazeGone(name)
	if self.db.profile.gaze then
		self:RemoveBar(string.format(L["bar_gaze"], name))
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Phase2()
	module:Phase3()
	module:DeepBreath()
	module:FlameBreath()
	module:Fireball()
	module:Fear()
	module:Gaze(UnitName("player"))
	module:GazeGone(UnitName("player"))	

	module:BigWigs_RecvSync(syncName.phase2)
	module:BigWigs_RecvSync(syncName.phase3)
	module:BigWigs_RecvSync(syncName.deepbreath)
	module:BigWigs_RecvSync(syncName.flamebreath)
	module:BigWigs_RecvSync(syncName.fireball)
	module:BigWigs_RecvSync(syncName.fear)
	module:BigWigs_RecvSync(syncName.gaze, UnitName("player"))
	module:BigWigs_RecvSync(syncName.gazeStop, UnitName("player"))
end
