
--------------------------------------
-- Create all modules for this raid	--
--------------------------------------

BigWigs.bossmods.onyxia = {}
BigWigs.bossmods.onyxia.onyxia = "Onyxia"
BigWigs.bossmods.onyxia.onyxianWarder = "Onyxian Warder"

BigWigs:ModuleDeclaration(BigWigs.bossmods.onyxia.onyxia, "Onyxia's Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.onyxia.onyxianWarder, "Onyxia's Lair")
