local bossName = BigWigs.bossmods.onyxia.onyxianWarder
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
    return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)

------------------------------
-- Variables                --
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 2 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
-- Initialization           --
------------------------------

-- called after module is enabled
function module:OnEnable()
    self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_SELF_DAMAGE",     "Event")
    self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_PARTY_DAMAGE",    "Event")
    self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE", "Event")

    self:ThrottleSync(6, syncName.fireNova)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
end

-- called after boss is engaged
function module:OnEngage()
    self:FireNova()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
-- Event Handlers           --
------------------------------
function module:Event(msg)
    if strfind(msg, L["trigger_fireNova"]) then
        self:Sync(syncName.fireNova)
    end
end


----------------------------------
-- Module Test Function         --
----------------------------------

-- automated test
function module:TestModule()
    module:OnEnable()
    module:OnSetup()
    module:OnEngage()

    module:TestModuleCore()

    -- check event handlers
    module:Event(format(L["trigger_fireNova"]))

    module:OnDisengage()
    module:TestDisable()
end

-- visual test
function module:TestVisual()
    BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
