------------------------------
-- Variables                --
------------------------------

local bossName = BigWigs.bossmods.onyxia.onyxianWarder
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 2
module.enabletrigger = module.translatedName
module.toggleoptions = {"fireNova" --[[, "bosskill"]]}
module.trashMod = true

-- locals
module.timer = {
    fireNova = 12,
}
local timer = module.timer

module.icon = {
    fireNova = "Spell_Fire_SealOfFire"
}
local icon = module.icon

module.syncName = {
    fireNova = "OnyxianWarderFireNova",
}
local syncName = module.syncName


------------------------------
--      Synchronization     --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.fireNova then
        self:FireNova()
    end
end


------------------------------
-- Sync Handlers            --
------------------------------
function module:FireNova()
    if self.db.profile.fireNova then
        self:Bar(L["bar_fireNova"], timer.fireNova, icon.fireNova)
    end
end

----------------------------------
-- Module Test Function         --
----------------------------------

-- automated test
function module:TestModuleCore()
    -- check core functions
    module:FireNova()

    module:BigWigs_RecvSync(syncName.fireNova)
end
