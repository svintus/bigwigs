------------------------------
-- Localization              --
------------------------------

local bossName = BigWigs.bossmods.onyxia.onyxianWarder
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
    cmd = "OnyxianWarder",

    -- commands
    fireNova_cmd = "fireNova",
    fireNova_name = "Fire Nova bar",
    fireNova_desc = "Toggles showing bar for Fire Nova.",

    -- triggers
    trigger_fireNova = "Onyxian Warder ?'s Fire Nova",

    -- bars
    bar_fireNova = "Fire Nova",

    -- messages

    -- misc

} end )
