------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.chromaggus
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20018 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"enrage", "frenzy", "breath", "breathcd", "vulnerability", "bosskill"}


-- locals
module.timer = {
	firstBreath = 30,
	secondBreath = 60,
	breathInterval = 60,
	unknownBreathInterval = 30,
	breathCast = 4,
	frenzy = 8,
	nextFrenzy = 11,
	vulnerability = 45,
}
local timer = module.timer

module.icon = {
	unknown = "INV_Misc_QuestionMark",
	breath1 = "Spell_Arcane_PortalOrgrimmar",
	breath2 = "Spell_Nature_Acid_01",
	breath3 = "Spell_Fire_Fire",
	breath4 = "Spell_Shadow_ChillTouch",
	breath5 = "Spell_Frost_ChillingBlast",
	frenzy = "Ability_Druid_ChallangingRoar",
	tranquil = "Spell_Nature_Drowsy",
	vulnerability = "Spell_Shadow_BlackPlague",
}
local icon = module.icon

module.syncName = {
	breath = "ChromaggusBreath",
	breathCasted = "ChromaggusBreathCasted",
	frenzy = "ChromaggusFrenzyStart",
	frenzyOver = "ChromaggusFrenzyStop",
	engage = "ChromaggusEngage",
	enrage = "ChromaggusEnrage",
	vulnerabilityChanged = "ChromaggusVulnerabilityChanged",
}
local syncName = module.syncName

module.lastFrenzy = 0
local _, playerClass = UnitClass("player")
module.playerClass = playerClass
module.breathCache = {}  -- in case your raid wipes

module.vulnerability = nil
module.enrageAnnounced = nil
module.frenzied = nil
module.lastVuln = 0


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.breath and rest then
		self:Breath(rest)
	elseif sync == syncName.breathCasted then
		self:BreathCasted()
	elseif sync == syncName.frenzy then
		self:Frenzy()
	elseif sync == syncName.frenzyOver then
		self:FrenzyGone()
	elseif sync == syncName.enrage then
		self:Enrage()
	elseif sync == syncName.vulnerabilityChanged then
		self:VulnerabilityChanged()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Breath(number)
	--[[if number and self.db.profile.breath then
		local spellName = L:HasTranslation("breath"..number) and L["breath"..number] or nil
		if not spellName then 
			return 
		end
		
        if table.getn(module.breathCache) < 2 then
			module.breathCache[table.getn(module.breathCache)+1] = spellName
        end
		
        local b = "breath"..number
		self:RemoveBar(L["icon"..number]) -- remove timer bar
		self:Bar(string.format( L["bar_breathCast"], spellName), timer.breathCast, L["icon"..number]) -- show cast bar
		self:Message(string.format(L["msg_breath"], spellName), "Important")
		
		self:DelayedMessage(timer.breathInterval - 5, string.format(L["msg_breathSoon"], spellName), "Important", nil, nil, true)
		self:DelayedBar(timer.breathCast, spellName, timer.breathInterval, L["icon"..number], true, L["breathcolor"..number]) -- delayed timer bar
        
        if self.db.profile.breathcd then
            self:DelayedSound(timer.breathInterval - 10, "Ten")
            self:DelayedSound(timer.breathInterval - 3, "Three")
            self:DelayedSound(timer.breathInterval - 2, "Two")
            self:DelayedSound(timer.breathInterval - 1, "One")
        end
	end]]
	if number and self.db.profile.breath then
		local spellName = L:HasTranslation("breath"..number) and L["breath"..number] or nil
		if not spellName then 
			return 
		end
		
        if table.getn(module.breathCache) < 2 then
			module.breathCache[table.getn(module.breathCache)+1] = spellName
        end
		
		self:RemoveBar(L["breath"..number])
		self:RemoveBar(L["bar_breathNext"])
		self:RemoveBar(L["bar_breathCastUnknown"])
		self:Bar(string.format( L["bar_breathCast"], spellName), timer.breathCast, L["icon"..number])
		self:Message(string.format(L["msg_breath"], spellName), "Important")
		
		self:DelayedMessage(timer.unknownBreathInterval - 5, L["msg_breathUnknown"], "Important", nil, nil, true)
		self:DelayedBar(timer.breathCast, L["bar_breathNext"], timer.unknownBreathInterval - timer.breathCast, icon.unknown)
	end
	if self.db.profile.breathcd then
		self:DelayedSound(timer.unknownBreathInterval - 10, "Ten")
		self:DelayedSound(timer.unknownBreathInterval - 3, "Three")
		self:DelayedSound(timer.unknownBreathInterval - 2, "Two")
		self:DelayedSound(timer.unknownBreathInterval - 1, "One")
	end
end

function module:BreathCasted()
	local nextBreath = timer.unknownBreathInterval - timer.breathCast
	if self.db.profile.breath then
		self:RemoveBar(L["bar_breathCastUnknown"])
		self:Bar(L["bar_breathNext"], nextBreath, icon.unknown)
		self:DelayedMessage(nextBreath - 5, L["msg_breathUnknown"], "Important", nil, nil, true)
		self:DelayedBar(nextBreath, L["bar_breathCastUnknown"], timer.breathCast, icon.unknown)
	end
	if self.db.profile.breathcd then
		self:DelayedSound(nextBreath - 10, "Ten")
		self:DelayedSound(nextBreath - 3, "Three")
		self:DelayedSound(nextBreath - 2, "Two")
		self:DelayedSound(nextBreath - 1, "One")
	end
end

function module:Frenzy()
	if self.db.profile.frenzy and not module.frenzied then
		self:Message(L["msg_frenzy"], "Attention")
		self:Bar(L["bar_frenzy"], timer.frenzy, icon.frenzy, true, BigWigsColors.db.profile.frenzy)
		
		if module.playerClass == "HUNTER" then
			self:WarningSign(icon.tranquil, timer.frenzy, true)
		end
	end
	module.frenzied = true
	module.lastFrenzy = GetTime()
end

function module:FrenzyGone()
	if self.db.profile.frenzy and module.frenzied then
		self:RemoveBar(L["bar_frenzy"])
        if module.lastFrenzy ~= 0 then
            local NextTime = (module.lastFrenzy + timer.nextFrenzy) - GetTime()
			self:Bar(L["bar_frenzyNext"], NextTime, icon.frenzy, true, BigWigsColors.db.profile.frenzyNext)
		end
	end
    self:RemoveWarningSign(icon.tranquil, true)
	module.frenzied = nil
end

function module:Enrage()
	if not module.enrageAnnounced and self.db.profile.enrage then
		self:Message(L["msg_enrage"], "Important", true, "Beware")
	end
	
	module.enrageAnnounced = true
end

function module:VulnerabilityChanged()
	if module.vulnerability and self.db.profile.vulnerability then
		self:Message(L["msg_vulnerabilityChanged"], "Positive")
		self:RemoveBar(format(L["bar_vulnerability"], module.vulnerability))
		self:Bar(format(L["bar_vulnerability"], "???"), timer.vulnerability, icon.vulnerability)
	end
	
	module.lastVuln = GetTime()
	module.vulnerability = nil
end


------------------------------
--      Utility	Functions   --
------------------------------

function module:IdentifyVulnerability(school)
    if not self.db.profile.vulnerability or not type(school) == "string" then return end
    if (module.lastVuln + 3) > GetTime() then return end -- 5 seconds delay
	
    module.vulnerability = school
    self:Message(format(L["msg_vulnerability"], school), "Positive")
    if module.lastVuln then
        self:RemoveBar(format(L["bar_vulnerability"], "???"))
        self:Bar(format(L["bar_vulnerability"], school), (module.lastVuln + timer.vulnerability) - GetTime(), icon.vulnerability)
    end
end

function module:Engaged()
	--[[if self.db.profile.breath then
		local firstBarName  = L["bar_breathFirst"]
		local firstBarMSG   = L["msg_breathUnknown"]
		local secondBarName = L["bar_breathSecond"]
		local secondBarMSG  = L["msg_breathUnknown"]
		if table.getn(module.breathCache) == 2 then
			-- if we have 2 breaths cached this session means we have wiped already and that after discovering the two breath types
			firstBarName  = string.format(L["bar_breathCast"], module.breathCache[1])
			firstBarMSG   = string.format(L["msg_breath"], module.breathCache[1])
			secondBarName = string.format(L["bar_breathCast"], module.breathCache[2])
			secondBarMSG  = string.format(L["msg_breath"], module.breathCache[2])
		elseif table.getn(module.breathCache) == 1 then
			-- we wiped before but know at least the first breath
			firstBarName  = string.format(L["bar_breathCast"], module.breathCache[1])
			firstBarMSG   = string.format(L["msg_breath"], module.breathCache[1])
		end
		self:DelayedMessage(timer.firstBreath - 5, firstBarMSG, "Attention", nil, nil, true)
		self:Bar(firstBarName, timer.firstBreath, icon.unknown, true, BigWigsColors.db.profile.start)
		self:DelayedMessage(timer.secondBreath - 5, secondBarMSG, "Attention", nil, nil, true)
		self:Bar(secondBarName, timer.secondBreath, icon.unknown, true, BigWigsColors.db.profile.start)
	end
	if self.db.profile.breathcd then
		self:DelayedSound(timer.firstBreath - 10, "Ten", "b1_10")
		self:DelayedSound(timer.firstBreath - 3, "Three", "b1_3")
		self:DelayedSound(timer.firstBreath - 2, "Two", "b1_2")
		self:DelayedSound(timer.firstBreath - 1, "One", "b1_1")
		
		self:DelayedSound(timer.secondBreath - 10, "Ten", "b2_10")
		self:DelayedSound(timer.secondBreath - 3, "Three", "b2_3")
		self:DelayedSound(timer.secondBreath - 2, "Two", "b2_2")
		self:DelayedSound(timer.secondBreath - 1, "One", "b2_1")
	end]]
	if self.db.profile.breath then
		self:DelayedMessage(timer.firstBreath - 5, L["msg_breathUnknown"], "Attention", nil, nil, true)
		self:Bar(L["bar_breathNext"], timer.firstBreath, icon.unknown, true, BigWigsColors.db.profile.start)
		self:DelayedBar(timer.firstBreath, L["bar_breathCastUnknown"], timer.breathCast, icon.unknown)
	end
	if self.db.profile.breathcd then
		self:DelayedSound(timer.firstBreath - 10, "Ten", "b1_10")
		self:DelayedSound(timer.firstBreath - 3, "Three", "b1_3")
		self:DelayedSound(timer.firstBreath - 2, "Two", "b1_2")
		self:DelayedSound(timer.firstBreath - 1, "One", "b1_1")
	end
	if self.db.profile.frenzy then
		self:Bar(L["bar_frenzyNext"], timer.nextFrenzy, icon.frenzy, true, BigWigsColors.db.profile.frenzyNext) 
	end
	if self.db.profile.vulnerability then
		self:Bar(format(L["bar_vulnerability"], "???"), timer.vulnerability, icon.vulnerability)
	end
    
    module.lastVuln = GetTime()
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Engaged()
	module:IdentifyVulnerability("Fire")
	module:VulnerabilityChanged()
	module:Enrage()
	module:Frenzy()
	module:FrenzyGone()
	module:Breath(1)
	module:BreathCasted()

	module:BigWigs_RecvSync(syncName.breath, 1)
	module:BigWigs_RecvSync(syncName.breathCasted)
	module:BigWigs_RecvSync(syncName.frenzy)
	module:BigWigs_RecvSync(syncName.frenzyOver)
	module:BigWigs_RecvSync(syncName.enrage)
	module:BigWigs_RecvSync(syncName.vulnerabilityChanged)
	
	module.breathCache = {}
end
