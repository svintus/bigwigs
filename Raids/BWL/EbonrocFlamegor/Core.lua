------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.ebonrocflamegor
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

module.ebonroc = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")["Ebonroc"]
module.flamegor = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")["Flamegor"]

-- module variables
module.revision = 10 -- To be overridden by the module!
module.enabletrigger = {module.ebonroc, module.flamegor} -- string or table {boss, add1, add2}
module.toggleoptions = {

-- Ebonroc
"wingBuffetCastEbonroc",  "wingBuffetNextEbonroc", "shadowFlameCastEbonroc",   "shadowFlameNextEbonroc",  "frenzyEbonroc",  -1,

-- Flamegor
"wingBuffetCastFlamegor", "wingBuffetNextFlamegor", "shadowFlameCastFlamegor", "shadowFlameNextFlamegor", "frenzyFlamegor", -1,

-- Common
"frenzyNext", "embraceBar", "embraceMsg", "embraceSign", "shadowsYou", "shadowsOthers", "flamesYou", "flamesOthers", "bosskill"

}


module.defaultDB = {
	embraceMsg = false,
	embraceSign = false,
	shadowsOthers = false,
	flamesOthers = false,
}

-- locals
module.timer = {
	wingBuffet = 20,
	wingBuffetCast = 1,
	shadowFlame = 20,
	shadowFlameCast = 2,
	frenzy = 9,
	frenzyFirst = 8,  -- 7.9 - ~10.7
	frenzyNext = 9,  -- 8.8 - 11.9
	embrace = 15,
	embraceFirst = 19.7,
	embraceWarning = 3,
	shadows = 8,
	flames = 8,
}
local timer = module.timer

module.icon = {
	wingBuffet = "INV_Misc_MonsterScales_14",
	shadowFlame = "Spell_Fire_Incinerate",
	frenzy = "Ability_Druid_ChallangingRoar",
	tranquil = "Spell_Nature_Drowsy",
	embrace = "Spell_Shadow_AntiShadow",
	shadows = "Spell_Shadow_GatherShadows",
	flames = "Spell_Fire_MoltenBlood",
}
local icon = module.icon

--[[
Sometimes both dragons use the same spell (Wing Buffet or Shadow Flame) within a short period of time from each other.
So using different syncs for each dragon, otherwise would have to set ThrottleSync to 0 or so.
Frenzy was not seen to be used by both dragons at once though, so different syncs for it are just in case.
]]
module.syncName = {
	wingBuffetEbonroc = "EbonrocFlamegorWingBuffetEbonroc",
	wingBuffetFlamegor = "EbonrocFlamegorWingBuffetFlamegor",
	shadowFlameEbonroc = "EbonrocFlamegorShadowflameEbonroc",
	shadowFlameFlamegor = "EbonrocFlamegorShadowflameFlamegor",
	frenzyEbonroc = "EbonrocFlamegorFrenzyEbonroc",
	frenzyFlamegor = "EbonrocFlamegorFrenzyFlamegor",
	frenzyStopEbonroc = "EbonrocFlamegorFrenzyStopEbonroc",
	frenzyStopFlamegor = "EbonrocFlamegorFrenzyStopFlamegor",
	embrace = "EbonrocFlamegorEmbrace",
	shadows = "EbonrocFlamegorShadows",
	shadowsStop = "EbonrocFlamegorShadowsStop",
	flames = "EbonrocFlamegorFlames",
	flamesStop = "EbonrocFlamegorFlamesStop",
}
local syncName = module.syncName

module.lastFrenzy = 0
module.playerClass = ({UnitClass"player"})[2]
module.embraceCounter = -1


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.wingBuffetEbonroc then
		self:WingBuffet(module.ebonroc)
	elseif sync == syncName.wingBuffetFlamegor then
		self:WingBuffet(module.flamegor)
	elseif sync == syncName.shadowFlameEbonroc then
		self:ShadowFlame(module.ebonroc)
	elseif sync == syncName.shadowFlameFlamegor then
		self:ShadowFlame(module.flamegor)
	elseif sync == syncName.frenzyEbonroc then
		self:Frenzy(module.ebonroc)
	elseif sync == syncName.frenzyFlamegor then
		self:Frenzy(module.flamegor)
	elseif sync == syncName.frenzyStopEbonroc then
		self:FrenzyStop(module.ebonroc)
	elseif sync == syncName.frenzyStopFlamegor then
		self:FrenzyStop(module.flamegor)
	elseif sync == syncName.embrace then
		self:Embrace(tonumber(rest))
	elseif sync == syncName.shadows then
		self:Shadows(rest)
	elseif sync == syncName.shadowsStop then
		self:ShadowsStop(rest)
	elseif sync == syncName.flames then
		self:Flames(rest)
	elseif sync == syncName.flamesStop then
		self:FlamesStop(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------

function module:WingBuffet(boss)
	if boss == module.ebonroc  and self.db.profile.wingBuffetCastEbonroc or
	   boss == module.flamegor and self.db.profile.wingBuffetCastFlamegor then
		self:Message(format(L["msg_wingBuffet"], boss, timer.wingBuffet), "Important")
		self:RemoveBar(format(L["bar_wingBuffetNext"], boss))
		self:Bar(format(L["bar_wingBuffetCast"], boss), timer.wingBuffetCast, icon.wingBuffet)
	end
	if boss == module.ebonroc  and self.db.profile.wingBuffetNextEbonroc or
	   boss == module.flamegor and self.db.profile.wingBuffetNextFlamegor then
		self:DelayedBar(timer.wingBuffetCast, format(L["bar_wingBuffetNext"], boss), timer.wingBuffet - timer.wingBuffetCast, icon.wingBuffet)
	end
end

function module:ShadowFlame(boss)
	if boss == module.ebonroc  and self.db.profile.shadowFlameCastEbonroc or
	   boss == module.flamegor and self.db.profile.shadowFlameCastFlamegor then
		self:Message(format(L["msg_shadowFlame"], boss), "Important", true, "Alarm")
		self:RemoveBar(format(L["bar_shadowFlameNext"], boss))
		self:Bar(format(L["bar_shadowFlameCast"], boss), timer.shadowFlameCast, icon.shadowFlame)

	end
	if boss == module.ebonroc  and self.db.profile.shadowFlameNextEbonroc or
	   boss == module.flamegor and self.db.profile.shadowFlameNextFlamegor then
		self:DelayedBar(timer.shadowFlameCast, format(L["bar_shadowFlameNext"], boss), timer.shadowFlame - timer.shadowFlameCast, icon.shadowFlame)
	end
end

function module:Frenzy(boss)
	if boss == module.ebonroc  and self.db.profile.frenzyEbonroc or
	   boss == module.flamegor and self.db.profile.frenzyFlamegor then
		self:Message(format(L["msg_frenzy"], boss), "Important", false, "Alert")
		self:RemoveBar(format(L["bar_frenzyNext"], boss))
		local barText = format(L["bar_frenzy"], boss)
		local onClickFunc = function() TargetByName(boss, true) end
		self:Bar(barText, timer.frenzy, icon.frenzy, true, BigWigsColors.db.profile.frenzy)
		self:SetCandyBarOnClick(self:BarId(barText), onClickFunc)
		if module.playerClass == "HUNTER" then
			self:WarningSign(icon.tranquil, timer.frenzy, true)
			self:WarningSignOnClick(onClickFunc)
		end
	end
	self.lastFrenzy = GetTime()
end

function module:FrenzyStop(boss)
	if boss == module.ebonroc  and self.db.profile.frenzyEbonroc or
	   boss == module.flamegor and self.db.profile.frenzyFlamegor then
		self:RemoveBar(format(L["bar_frenzy"], boss))
		self:RemoveWarningSign(icon.tranquil, true)
	end
	if self.lastFrenzy ~= 0 and self.db.profile.frenzyNext then
		--[[
		Another dragon might already have Frenzy here, gained shortly before the current one (<boss>) lost it.
		Happened with 8.8 sec between Frenzies.
		In this case there should be no bar for the next Frenzy.
		Also there seem to be no reason showing the bar if there is a very small amount of time left before the next Frenzy,
		if the bar would appear for like half a second.
		]]
		local nextTime = self.lastFrenzy + timer.frenzyNext - GetTime()
		local anotherBoss = boss == module.ebonroc and module.flamegor or module.ebonroc
		local _, _, _, running = self:BarStatus(format(L["bar_frenzy"], anotherBoss))  -- false if at 0.0 and fading
		if not running and nextTime > 1 then
			self:Bar(L["bar_frenzyNext"], nextTime, icon.frenzy, true, BigWigsColors.db.profile.frenzyNext)
		end
	end
end

function module:Embrace(number, isFirst)
	if number and self.embraceCounter < number then
		self.embraceCounter = number
		local timerNext = isFirst and timer.embraceFirst or timer.embrace
		if self.db.profile.embraceBar then
			self:RemoveBar(format(L["bar_embrace"], self.embraceCounter))
			self:Bar(format(L["bar_embrace"], self.embraceCounter + 1), timerNext, icon.embrace, true, "magenta")
		end
		if self.db.profile.embraceMsg then
			self:DelayedMessage(timerNext - timer.embraceWarning, format(L["msg_embrace"], self.embraceCounter + 1, timer.embraceWarning), "Attention", false, "Alarm")
		end
		if self.db.profile.embraceSign then
			self:RemoveWarningSign(icon.embrace)
			self:DelayedWarningSign(timerNext - timer.embraceWarning, icon.embrace, timer.embraceWarning)
		end
	end
end

function module:Shadows(name)
	if name == UnitName("player") and self.db.profile.shadowsYou then
		self:Message(L["msg_shadowsYou"], "Personal", true, "Alarm")
		self:WarningSign(icon.shadows, timer.shadows)
	end
	if self.db.profile.shadowsOthers then
		if name ~= UnitName("player") then
			self:Message(format(L["msg_shadowsOther"], name), "Attention")
		end
		self:Bar(format(L["bar_shadows"], name), timer.shadows, icon.shadows, true, BigWigsColors.db.profile.significant)
	end
end

function module:ShadowsStop(name)
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.shadows)
	end
	self:RemoveBar(format(L["bar_shadows"], name))
end

function module:Flames(name)
	if name == UnitName("player") and self.db.profile.flamesYou then
		self:Message(L["msg_flamesYou"], "Personal", true, "Alarm")
		self:WarningSign(icon.flames, timer.flames)
	end
	if self.db.profile.flamesOthers then
		if name ~= UnitName("player") then
			self:Message(format(L["msg_flamesOther"], name), "Attention")
		end
		self:Bar(format(L["bar_flames"], name), timer.flames, icon.flames, true, BigWigsColors.db.profile.significant)
	end
end

function module:FlamesStop(name)
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.flames)
	end
	self:RemoveBar(format(L["bar_flames"], name))
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:WingBuffet(module.ebonroc)
	module:WingBuffet(module.flamegor)
	module:ShadowFlame(module.ebonroc)
	module:ShadowFlame(module.flamegor)
	module:Frenzy(module.ebonroc)
	module:Frenzy(module.flamegor)
	module:FrenzyStop(module.ebonroc)
	module:FrenzyStop(module.flamegor)
	module:Embrace(0, true)
	module:Embrace(1)
	module:Shadows(UnitName("player"))
	module:ShadowsStop(UnitName("player"))
	module:Shadows(UnitName("player") .. 1)
	module:ShadowsStop(UnitName("player") .. 1)
	module:Flames(UnitName("player"))
	module:FlamesStop(UnitName("player"))
	module:Flames(UnitName("player") .. 1)
	module:FlamesStop(UnitName("player") .. 1)

	module:BigWigs_RecvSync(syncName.wingBuffetEbonroc)
	module:BigWigs_RecvSync(syncName.wingBuffetFlamegor)
	module:BigWigs_RecvSync(syncName.shadowFlameEbonroc)
	module:BigWigs_RecvSync(syncName.shadowFlameFlamegor)
	module:BigWigs_RecvSync(syncName.frenzyEbonroc)
	module:BigWigs_RecvSync(syncName.frenzyFlamegor)
	module:BigWigs_RecvSync(syncName.frenzyStopEbonroc)
	module:BigWigs_RecvSync(syncName.frenzyStopFlamegor)
	module:BigWigs_RecvSync(syncName.embrace, 2)
	module:BigWigs_RecvSync(syncName.shadows, UnitName("player"))
	module:BigWigs_RecvSync(syncName.shadowsStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.shadows, UnitName("player") .. 1)
	module:BigWigs_RecvSync(syncName.shadowsStop, UnitName("player") .. 1)
	module:BigWigs_RecvSync(syncName.flames, UnitName("player"))
	module:BigWigs_RecvSync(syncName.flamesStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.flames, UnitName("player") .. 1)
	module:BigWigs_RecvSync(syncName.flamesStop, UnitName("player") .. 1)
	
	self.lastFrenzy = 0
	self.embraceCounter = -1
end
