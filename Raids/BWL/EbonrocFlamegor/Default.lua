local bossName = BigWigs.bossmods.bwl.ebonrocflamegor
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 10 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE")
	
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS", "CheckForFrenzy")
	--[[
	An emote was once noticed to be fake for Wing Buffet:
	both dragons used the emote when nobody was in melee range and/or LoS,
	but there were no corresponding combat messages.
	
	I suppose an emote is shown when a boss is supposed to cast (or try to cast) a spell,
	and a combat message is shown when the boss actually casts the spell.
	
	Also had a weird case with Frenzy:
	02:49:31.623 Flamegor gains Frenzy. CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS
	02:49:40.515 Flamegor goes into a frenzy! CHAT_MSG_MONSTER_EMOTE
	02:49:40.518 Frenzy fades from Flamegor. CHAT_MSG_SPELL_AURA_GONE_OTHER
	02:49:40.674 Flamegor gains Frenzy. CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS
	The new Frenzy emote came while the current Frenzy was still active (was about to end though).
	
	Therefore I decided not to use emotes as triggers for Frenzy and only use combat messages.
	
	Although emotes could help in case when Frenzy is not removed
	and starts again on the same dragon before the current one ended.
	In this case I suppose there will be no combat message, but will be an emote.
	This happened with different dragons, not sure if can be with the same dragon (I guess yes).
	But Frenzy not being removed for the whole duration is most likely a wipe situation,
	and considering the above-mentioned issues with emotes I still do not want to bother with them, at least for now.
	]]
	--self:RegisterEvent("CHAT_MSG_MONSTER_EMOTE", "CheckForFrenzy")
	
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "CheckForFrenzyStop")
	self:RegisterEvent("CHAT_MSG_SPELL_BREAK_AURA", "CheckForFrenzyStop")
	
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE", "Debuff")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Debuff")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Debuff")
	
	self:RegisterEvent("PLAYER_DEAD")
	
	self:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH", "CheckForBossDeath") -- override module prototype
	
	self:ThrottleSync(2, syncName.embrace)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.lastFrenzy = 0
	self.embraceCounter = -1
end

-- called after boss is engaged
function module:OnEngage()
	if self.db.profile.frenzyNext then
		self:Bar(L["bar_frenzyNext"], timer.frenzyFirst, icon.frenzy, true, BigWigsColors.db.profile.frenzyNext)
	end
	self:Embrace(0, true)
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------

function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(msg)
	if msg == L["trigger_wingBuffetEbonroc"] then
		self:Sync(syncName.wingBuffetEbonroc)
	elseif msg == L["trigger_wingBuffetFlamegor"] then
		self:Sync(syncName.wingBuffetFlamegor)
	elseif msg == L["trigger_shadowFlameEbonroc"] then
		self:Sync(syncName.shadowFlameEbonroc)
	elseif msg == L["trigger_shadowFlameFlamegor"] then
		self:Sync(syncName.shadowFlameFlamegor)
	end
end

function module:CheckForFrenzy(msg)
	if msg == L["trigger_frenzyGainEbonroc1"] or msg == L["trigger_frenzyGainEbonroc2"] then
		self:Sync(syncName.frenzyEbonroc)
	elseif msg == L["trigger_frenzyGainFlamegor1"] or msg == L["trigger_frenzyGainFlamegor2"] then
		self:Sync(syncName.frenzyFlamegor)
	end
end

function module:CheckForFrenzyStop(msg)
	if msg == L["trigger_frenzyStopEbonroc1"] or strfind(msg, L["trigger_frenzyStopEbonroc2"]) then
		self:Sync(syncName.frenzyStopEbonroc)
	elseif msg == L["trigger_frenzyStopFlamegor1"] or strfind(msg, L["trigger_frenzyStopFlamegor2"]) then
		self:Sync(syncName.frenzyStopFlamegor)
	end
end

function module:Debuff(msg)
	if strfind(msg, L["trigger_embrace1"]) or strfind(msg, L["trigger_embrace2"]) then
		self:Sync(syncName.embrace .. " " .. self.embraceCounter + 1)
	elseif msg == L["trigger_shadowsYou"] then
		self:Sync(syncName.shadows .. " " .. UnitName("player"))
	elseif msg == L["trigger_shadowsYouStop"] then
		self:Sync(syncName.shadowsStop .. " " .. UnitName("player"))
	elseif msg == L["trigger_flamesYou"] then
		self:Sync(syncName.flames .. " " .. UnitName("player"))
	elseif msg == L["trigger_flamesYouStop"] then
		self:Sync(syncName.flamesStop .. " " .. UnitName("player"))
	else
		local _, _, name = strfind(msg, L["trigger_shadowsOther"])
		if name then
			self:Sync(syncName.shadows .. " " .. name)
			return
		end
		_, _, name = strfind(msg, L["trigger_shadowsOtherStop"])
		if name then
			self:Sync(syncName.shadowsStop .. " " .. name)
			return
		end
		_, _, name = strfind(msg, L["trigger_flamesOther"])
		if name then
			self:Sync(syncName.flames .. " " .. name)
			return
		end
		_, _, name = strfind(msg, L["trigger_flamesOtherStop"])
		if name then
			self:Sync(syncName.flamesStop .. " " .. name)
			return
		end
	end
end

function module:PLAYER_DEAD()
	self:ShadowsStop(UnitName("player"))
	self:FlamesStop(UnitName("player"))
end

function module:CheckForBossDeath(msg)
	if msg == format(UNITDIESOTHER, module.ebonroc) or msg == format(UNITDIESOTHER, module.flamegor) then
		self:SendBossDeathSync()
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_wingBuffetEbonroc"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_wingBuffetFlamegor"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_shadowFlameEbonroc"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_shadowFlameFlamegor"])
	module:CheckForFrenzy(L["trigger_frenzyGainEbonroc1"])
	module:CheckForFrenzy(L["trigger_frenzyGainEbonroc2"])
	module:CheckForFrenzy(L["trigger_frenzyGainFlamegor1"])
	module:CheckForFrenzy(L["trigger_frenzyGainFlamegor2"])
	module:CheckForFrenzyStop(L["trigger_frenzyStopEbonroc1"])
	module:CheckForFrenzyStop(L["trigger_frenzyStopEbonroc2"])
	module:CheckForFrenzyStop(L["trigger_frenzyStopFlamegor1"])
	module:CheckForFrenzyStop(L["trigger_frenzyStopFlamegor2"])
	module:Debuff(L["trigger_embrace1"])
	module:Debuff(L["trigger_embrace2"])
	module:Debuff(L["trigger_shadowsYou"])
	module:Debuff(L["trigger_shadowsYouStop"])
	module:Debuff(L["trigger_shadowsOther"])
	module:Debuff(L["trigger_shadowsOtherStop"])
	module:Debuff(L["trigger_flamesYou"])
	module:Debuff(L["trigger_flamesYouStop"])
	module:Debuff(L["trigger_flamesOther"])
	module:Debuff(L["trigger_flamesOtherStop"])
	module:PLAYER_DEAD()
	module:CheckForBossDeath(format(UNITDIESOTHER, module.ebonroc))
	module:CheckForBossDeath(format(UNITDIESOTHER, module.flamegor))

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
