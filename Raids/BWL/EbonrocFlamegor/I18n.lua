------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.bwl.ebonrocflamegor
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "EbonrocFlamegor",
	
	-- commands
	-- ebonroc
	wingBuffetCastEbonroc_cmd = "wingBuffetCastEbonroc",
	wingBuffetCastEbonroc_name = "Wing Buffet cast alert (Ebonroc)",
	wingBuffetCastEbonroc_desc = "Warn when Ebonroc casts Wing Buffet.",
	
	wingBuffetNextEbonroc_cmd = "wingBuffetNextEbonroc",
	wingBuffetNextEbonroc_name = "Wing Buffet next (Ebonroc)",
	wingBuffetNextEbonroc_desc = "Show bar for next Wing Buffet (Ebonroc).",

	shadowFlameCastEbonroc_cmd = "shadowFlameCastEbonroc",
	shadowFlameCastEbonroc_name = "Shadow Flame cast alert (Ebonroc)",
	shadowFlameCastEbonroc_desc = "Warn when Ebonroc casts Shadow Flame.",
	
	shadowFlameNextEbonroc_cmd = "shadowFlameNextEbonroc",
	shadowFlameNextEbonroc_name = "Shadow Flame next (Ebonroc)",
	shadowFlameNextEbonroc_desc = "Show bar for next Shadow Flame (Ebonroc).",
	
	frenzyEbonroc_cmd = "frenzyEbonroc",
	frenzyEbonroc_name = "Frenzy alert (Ebonroc)",
	frenzyEbonroc_desc = "Warn when Ebonroc is frenzied.\n\nNote: the bar and the warning sign can be clicked to target the boss with Frenzy.",
	
	-- flamegor
	wingBuffetCastFlamegor_cmd = "wingBuffetCastFlamegor",
	wingBuffetCastFlamegor_name = "Wing Buffet cast alert (Flamegor)",
	wingBuffetCastFlamegor_desc = "Warn when Flamegor casts Wing Buffet.",
	
	wingBuffetNextFlamegor_cmd = "wingBuffetNextFlamegor",
	wingBuffetNextFlamegor_name = "Wing Buffet next (Flamegor)",
	wingBuffetNextFlamegor_desc = "Show bar for next Wing Buffet (Flamegor).",

	shadowFlameCastFlamegor_cmd = "shadowFlameCastFlamegor",
	shadowFlameCastFlamegor_name = "Shadow Flame cast alert (Flamegor)",
	shadowFlameCastFlamegor_desc = "Warn when Flamegor casts Shadow Flame.",
	
	shadowFlameNextFlamegor_cmd = "shadowFlameNextFlamegor",
	shadowFlameNextFlamegor_name = "Shadow Flame next (Flamegor)",
	shadowFlameNextFlamegor_desc = "Show bar for next Shadow Flame (Flamegor).",
	
	frenzyFlamegor_cmd = "frenzyFlamegor",
	frenzyFlamegor_name = "Frenzy alert (Flamegor)",
	frenzyFlamegor_desc = "Warn when Flamegor is frenzied.\n\nNote: the bar and the warning sign can be clicked to target the boss with Frenzy.",
	
	-- common
	frenzyNext_cmd = "frenzyNext",
	frenzyNext_name = "Frenzy next bar",
	frenzyNext_desc = "Show bar for next Frenzy.",
	
	embraceBar_cmd = "embraceBar",
	embraceBar_name = "Embrace of Shadows/Flames bar",
	embraceBar_desc = "Show bar and counter for Embrace of Shadows/Flames.",
	
	embraceMsg_cmd = "embraceMsg",
	embraceMsg_name = "Embrace of Shadows/Flames warning message",
	embraceMsg_desc = "Show message 3 seconds before Embrace of Shadows/Flames.",
	
	embraceSign_cmd = "embraceSign",
	embraceSign_name = "Embrace of Shadows/Flames warning sign",
	embraceSign_desc = "Show sign 3 seconds before Embrace of Shadows/Flames.",
	
	shadowsYou_cmd = "shadowsYou",
	shadowsYou_name = "Shadows of Ebonroc on you alert",
	shadowsYou_desc = "Warn when you have Shadows of Ebonroc.",
	
	shadowsOthers_cmd = "shadowsOthers",
	shadowsOthers_name = "Shadows of Ebonroc on others announcement",
	shadowsOthers_desc = "Announce and show bar when others have Shadows of Ebonroc.",
	
	flamesYou_cmd = "flamesYou",
	flamesYou_name = "Flames of Flamegor on you alert",
	flamesYou_desc = "Warn when you have Flames of Flamegor.",
	
	flamesOthers_cmd = "flamesOthers",
	flamesOthers_name = "Flames of Flamegor on others announcement",
	flamesOthers_desc = "Announce and show bar when others have Flames of Flamegor.",
	
	-- triggers
	trigger_wingBuffetEbonroc = "Ebonroc begins to cast Wing Buffet.",
	trigger_wingBuffetFlamegor = "Flamegor begins to cast Wing Buffet.",
	trigger_shadowFlameEbonroc = "Ebonroc begins to cast Shadow Flame.",
	trigger_shadowFlameFlamegor = "Flamegor begins to cast Shadow Flame.",
	trigger_frenzyGainEbonroc1 = "Ebonroc gains Frenzy.",
	trigger_frenzyGainEbonroc2 = "Ebonroc goes into a frenzy!",
	trigger_frenzyGainFlamegor1 = "Flamegor gains Frenzy.",
	trigger_frenzyGainFlamegor2 = "Flamegor goes into a frenzy!",
	trigger_frenzyStopEbonroc1 = "Frenzy fades from Ebonroc.",
	trigger_frenzyStopEbonroc2 = "Ebonroc ?'s Frenzy is removed.",
	trigger_frenzyStopFlamegor1 = "Frenzy fades from Flamegor.",
	trigger_frenzyStopFlamegor2 = "Flamegor ?'s Frenzy is removed.",
	trigger_embrace1 = "afflicted by Embrace of Shadows",
	trigger_embrace2 = "afflicted by Embrace of Flames",
	trigger_shadowsYou = "You are afflicted by Shadows of Ebonroc.",
	trigger_shadowsYouStop = "Shadows of Ebonroc fades from you.",
	trigger_shadowsOther = "(.+) is afflicted by Shadows of Ebonroc",
	trigger_shadowsOtherStop = "Shadows of Ebonroc fades from (.+)",
	trigger_flamesYou = "You are afflicted by Flames of Flamegor.",
	trigger_flamesYouStop = "Flames of Flamegor fades from you.",
	trigger_flamesOther = "(.+) is afflicted by Flames of Flamegor",
	trigger_flamesOtherStop = "Flames of Flamegor fades from (.+)",
	
	-- messages
	msg_wingBuffet = "%s: Wing Buffet! Next one in %d seconds!",
	msg_shadowFlame = "%s: Shadow Flame incoming!",
	msg_frenzy = "%s: Frenzy! Tranq now!",
	msg_embrace = "Embrace %d in %d sec!",
	msg_shadowsYou = "You have Shadows of Ebonroc!",
	msg_shadowsOther = "%s has Shadows of Ebonroc!",
	msg_flamesYou = "You have Flames of Flamegor!",
	msg_flamesOther = "%s has Flames of Flamegor!",
	
	-- bars
	bar_wingBuffetCast = "%s: Wing Buffet",
	bar_wingBuffetNext = "%s: Next Wing Buffet",
	bar_shadowFlameCast = "%s: Shadow Flame",
	bar_shadowFlameNext = "%s: Next Shadow Flame",
	bar_frenzy = "%s: Frenzy",
	bar_frenzyNext = "Next Frenzy",
	bar_embrace = "Embrace %d",
	bar_shadows = "Shadows of Ebonroc: %s",
	bar_flames = "Flames of Flamegor: %s",
	
	-- misc
	
} end )
