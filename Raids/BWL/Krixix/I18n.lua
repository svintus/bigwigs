------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.bwl.krixix
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Krixix",
	
	-- commands
	elementalBlast_cmd = "elementalBlast",
	elementalBlast_name = "Elemental Blast bar",
	elementalBlast_desc = "Show bar when the boss begins to cast Elemental Blast.",
	
	shrinkRayCast_cmd = "shrinkRayCast",
	shrinkRayCast_name = "Shrink Ray cast",
	shrinkRayCast_desc = "Show bar when the boss begins to cast Shrink Ray.",
	
	shrinkRayNext_cmd = "shrinkRayNext",
	shrinkRayNext_name = "Shrink Ray next",
	shrinkRayNext_desc = "Show bar for next Shrink Ray.",
	
	disruptingRayCast_cmd = "disruptingRayCast",
	disruptingRayCast_name = "Disrupting Ray cast",
	disruptingRayCast_desc = "Show bar when the boss begins to cast Disrupting Ray.",
	
	disruptingRayNext_cmd = "disruptingRayNext",
	disruptingRayNext_name = "Disrupting Ray next",
	disruptingRayNext_desc = "Show bar for next Disrupting Ray.",
	
	deathRayCast_cmd = "deathRayCast",
	deathRayCast_name = "Death Ray cast",
	deathRayCast_desc = "Show bar when the boss begins to cast Death Ray.",
	
	deathRayNext_cmd = "deathRayNext",
	deathRayNext_name = "Death Ray next",
	deathRayNext_desc = "Show bar for next Death Ray.",
	
	mirrorsSystemMsg_cmd = "mirrorsSystemMsg",
	mirrorsSystemMsg_name = "Mirrors System message",
	mirrorsSystemMsg_desc = "Show message when Mirrors System is up/down.",
	
	mirrorsSystemSign_cmd = "mirrorsSystemSign",
	mirrorsSystemSign_name = "Mirrors System sign",
	mirrorsSystemSign_desc = "Show sign when the boss has Mirrors System.",
	
	mirrorsSystemDuration_cmd = "mirrorsSystemDuration",
	mirrorsSystemDuration_name = "Mirrors System duration bar",
	mirrorsSystemDuration_desc = "Show bar with Mirrors System duration.",
	
	mirrorsSystemCharges_cmd = "mirrorsSystemCharges",
	mirrorsSystemCharges_name = "Mirrors System charges bar",
	mirrorsSystemCharges_desc = "Show bar with Mirrors System charges.",
	
	-- triggers
	trigger_elementalBlast = "Master Elemental Shaper Krixix begins to cast Elemental Blast.",
	trigger_shrinkRay      = "Master Elemental Shaper Krixix begins to cast Shrink Ray.",
	trigger_disruptingRay  = "Master Elemental Shaper Krixix begins to cast Disrupting Ray.",
	trigger_deathRay       = "Master Elemental Shaper Krixix begins to cast Death Ray.",
	trigger_mirrorsSystem = "Master Elemental Shaper Krixix gains Mirrors System.",
	trigger_mirrorsSystemStop = "Mirrors System fades from Master Elemental Shaper Krixix.",
	trigger_reflect = "is reflected back by Master Elemental Shaper Krixix",
	
	-- messages
	msg_mirrorsSystem = "Mirrors System up!",
	msg_mirrorsSystemStop = "Mirrors System down!",
	
	-- bars
	bar_elementalBlast = "Elemental Blast",
	bar_shrinkRayCast = "Shrink Ray",
	bar_shrinkRayNext = "Next Shrink Ray",
	bar_disruptingRayCast = "Disrupting Ray",
	bar_disruptingRayNext = "Next Disrupting Ray",
	bar_deathRayCast = "Death Ray",
	bar_deathRayNext = "Next Death Ray",
	bar_mirrorsSystemDuration = "Mirrors System",
	bar_mirrorsSystemCharges = "Mirrors System Charges",
	
	-- misc
	
} end )
