------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.krixix
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 15 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.toggleoptions = {
	"elementalBlast", -1,
	"shrinkRayCast", "shrinkRayNext", -1,
	"disruptingRayCast", "disruptingRayNext", -1,
	"deathRayCast", "deathRayNext", -1,
	"mirrorsSystemMsg", "mirrorsSystemSign", "mirrorsSystemDuration", --[["mirrorsSystemCharges",]]
	"bosskill"
}


-- locals
module.timer = {
	elementalBlast = 2,
	shrinkRay = 45,
	shrinkRayFirst = 35,
	shrinkRayCast = 3,
	disruptingRay = 45,
	disruptingRayFirst = 20,
	disruptingRayCast = 2,
	deathRay = 45,
	deathRayFirst = 50,
	deathRayCast = 1,
	mirrorsSystem = 20,
}
local timer = module.timer

module.icon = {
	elementalBlast = "Spell_Nature_AbolishMagic",
	shrinkRay = "INV_Misc_EngGizmos_06",
	disruptingRay = "INV_Misc_EngGizmos_03",
	deathRay = "INV_Misc_EngGizmos_01",
	mirrorsSystem = "Spell_Nature_AstralRecalGroup",
}
local icon = module.icon

module.syncName = {
	elementalBlast = "KrixixElementalBlast",
	shrinkRay = "KrixixShrinkRay",
	disruptingRay = "KrixixDisruptingRay",
	deathRay = "KrixixDeathRay",
	mirrorsSystem = "KrixixMirrorsSystem",
	mirrorsSystemStop = "KrixixMirrorsSystemStop",
	reflect = "KrixixReflect",
}
local syncName = module.syncName

module.chargesUsed = 0
module.chargesMax = 20


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.elementalBlast then
		self:ElementalBlast()
	elseif sync == syncName.shrinkRay then
		self:ShrinkRay()
	elseif sync == syncName.disruptingRay then
		self:DisruptingRay()
	elseif sync == syncName.deathRay then
		self:DeathRay()
	elseif sync == syncName.mirrorsSystem then
		self:MirrorsSystem()
	elseif sync == syncName.mirrorsSystemStop then
		self:MirrorsSystemStop()
	elseif strfind(sync, syncName.reflect) then
		self:MirrorsSystemCounterChange(tonumber(rest))
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------

function module:ElementalBlast()
	if self.db.profile.elementalBlast then
		self:Bar(L["bar_elementalBlast"], timer.elementalBlast, icon.elementalBlast, true, BigWigsColors.db.profile.interrupt)
	end
end

function module:ShrinkRay()
	self:RemoveBar(L["bar_shrinkRayNext"])
	if self.db.profile.shrinkRayCast then
		self:Bar(L["bar_shrinkRayCast"], timer.shrinkRayCast, icon.shrinkRay)
	end
	if self.db.profile.shrinkRayNext then
		self:DelayedBar(timer.shrinkRayCast, L["bar_shrinkRayNext"], timer.shrinkRay - timer.shrinkRayCast, icon.shrinkRay)
	end
end

function module:DisruptingRay()
	self:RemoveBar(L["bar_disruptingRayNext"])
	if self.db.profile.disruptingRayCast then
		self:Bar(L["bar_disruptingRayCast"], timer.disruptingRayCast, icon.disruptingRay)
	end
	if self.db.profile.disruptingRayNext then
		self:DelayedBar(timer.disruptingRayCast, L["bar_disruptingRayNext"], timer.disruptingRay - timer.disruptingRayCast, icon.disruptingRay)
	end
end

function module:DeathRay()
	self:RemoveBar(L["bar_deathRayNext"])
	if self.db.profile.deathRayCast then
		self:Bar(L["bar_deathRayCast"], timer.deathRayCast, icon.deathRay)
	end
	if self.db.profile.deathRayNext then
		self:DelayedBar(timer.deathRayCast, L["bar_deathRayNext"], timer.deathRay - timer.deathRayCast, icon.deathRay)
	end
end

function module:MirrorsSystem()
	self.chargesUsed = 0  -- should always be zero here already, so just in case
	if self.db.profile.mirrorsSystemMsg then
		self:Message(L["msg_mirrorsSystem"], "Important", false, "Alarm")
	end
	if self.db.profile.mirrorsSystemSign then
		self:WarningSign(icon.mirrorsSystem, timer.mirrorsSystem)
	end
	if self.db.profile.mirrorsSystemDuration then
		self:Bar(L["bar_mirrorsSystemDuration"], timer.mirrorsSystem, icon.mirrorsSystem)
	end
	if self.db.profile.mirrorsSystemCharges then
		self:TriggerEvent("BigWigs_StartCounterBar", self, L["bar_mirrorsSystemCharges"], module.chargesMax, "Interface\\Icons\\" .. icon.mirrorsSystem)
		self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_mirrorsSystemCharges"], 0)
	end
end

function module:MirrorsSystemStop()
	self:RemoveWarningSign(icon.mirrorsSystem)
	self:RemoveBar(L["bar_mirrorsSystemDuration"])
	self:RemoveBar(L["bar_mirrorsSystemCharges"])
	if self.db.profile.mirrorsSystemMsg then
		self:Message(L["msg_mirrorsSystemStop"], "Positive")
	end
	self.chargesUsed = 0
end

function module:MirrorsSystemCounterChange(number)
	if number and self.chargesUsed < number then
		self.chargesUsed = number
		self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_mirrorsSystemCharges"], number)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:ElementalBlast()
	module:ShrinkRay()
	module:DisruptingRay()
	module:DeathRay()
	module:MirrorsSystem()
	module:MirrorsSystemCounterChange(1)
	module:MirrorsSystemStop()
	
	module:BigWigs_RecvSync(syncName.elementalBlast)
	module:BigWigs_RecvSync(syncName.shrinkRay)
	module:BigWigs_RecvSync(syncName.disruptingRay)
	module:BigWigs_RecvSync(syncName.deathRay)
	module:BigWigs_RecvSync(syncName.mirrorsSystem)
	module:BigWigs_RecvSync(syncName.reflect, 2)
	module:BigWigs_RecvSync(syncName.mirrorsSystemStop)
	self.chargesUsed = 0
end
