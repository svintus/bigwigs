local bossName = BigWigs.bossmods.bwl.krixix
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 15 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE", "Cast")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER")
	
	--[[ There are no reflect messages like on Azuregos, so this does not work.
	Reflected spells still hit the boss, so Mirrors System is not a classical reflection.
	self:RegisterEvent("CHAT_MSG_SPELL_SELF_DAMAGE", "Reflect")
	self:RegisterEvent("CHAT_MSG_SPELL_PET_DAMAGE", "Reflect")
	self:RegisterEvent("CHAT_MSG_SPELL_PARTY_DAMAGE", "Reflect")
	self:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE", "Reflect") ]]
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.chargesUsed = 0
end

-- called after boss is engaged
function module:OnEngage()
	if self.db.profile.shrinkRayNext then
		self:Bar(L["bar_shrinkRayNext"], timer.shrinkRayFirst, icon.shrinkRay)
	end
	if self.db.profile.disruptingRayNext then
		self:Bar(L["bar_disruptingRayNext"], timer.disruptingRayFirst, icon.disruptingRay)
	end
	if self.db.profile.deathRayNext then
		self:Bar(L["bar_deathRayNext"], timer.deathRayFirst, icon.deathRay)
	end
	self:RegisterBarForInterrupts(L["bar_elementalBlast"])
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------

function module:Cast(msg)
	if msg == L["trigger_elementalBlast"] then
		self:Sync(syncName.elementalBlast)
	elseif msg == L["trigger_shrinkRay"] then
		self:Sync(syncName.shrinkRay)
	elseif msg == L["trigger_disruptingRay"] then
		self:Sync(syncName.disruptingRay)
	elseif msg == L["trigger_deathRay"] then
		self:Sync(syncName.deathRay)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(msg)
	if msg == L["trigger_mirrorsSystem"] then
		self:Sync(syncName.mirrorsSystem)
	end
end

function module:CHAT_MSG_SPELL_AURA_GONE_OTHER(msg)
	if msg == L["trigger_mirrorsSystemStop"] then
		self:Sync(syncName.mirrorsSystemStop)
	end
end

function module:Reflect(msg)
	if strfind(msg, L["trigger_reflect"]) then
		local chargesUsed = self.chargesUsed + 1
		self:Sync(syncName.reflect .. "_" .. chargesUsed .. " " .. chargesUsed)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:Cast(L["trigger_elementalBlast"])
	module:Cast(L["trigger_shrinkRay"])
	module:Cast(L["trigger_disruptingRay"])
	module:Cast(L["trigger_deathRay"])
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(L["trigger_mirrorsSystem"])
	module:CHAT_MSG_SPELL_AURA_GONE_OTHER(L["trigger_mirrorsSystemStop"])
	module:Reflect(L["trigger_reflect"])

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
