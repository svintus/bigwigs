local bossName = BigWigs.bossmods.bwl.firemaw
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 20020 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()	
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_SELF_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_PARTY_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE", "Event")
	
	self:ThrottleSync(10, syncName.wingbuffet)
	self:ThrottleSync(10, syncName.shadowflame)
	self:ThrottleSync(18, syncName.orbSpawn)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.orbNum = 1
end

-- called after boss is engaged
function module:OnEngage()
	if self.db.profile.wingbuffet then
		self:DelayedMessage(timer.firstWingbuffet - 5, L["msg_wingBuffetSoon"], "Attention", nil, nil, true)
		self:Bar(L["bar_wingBuffetFirst"], timer.firstWingbuffet, icon.wingbuffet)
	end
	if self.db.profile.shadowflame then
		self:Bar(L["bar_shadowFlameNext"], timer.firstShadowflame, icon.shadowflame)
	end
	if self.db.profile.orbSpawn then
		self:Bar(L["bar_orbSpawn"], timer.orbSpawnFirst, icon.orb, true, "orange")
	end
	self:ScheduleEvent(self.OrbSpawnSoon, timer.orbSpawnFirst - timer.orbSpawnSoon, self)
	self:ScheduleEvent(function()
		--[[
		Not using sync for this unless SendWipeSync() is implemented and works properly.
		Otherwise players who are not on the boss will keep sending syncs after a wipe.
		self:Sync(syncName.orbSpawn)
		self:ScheduleRepeatingEvent(self.Sync, timer.orbSpawn, self, syncName.orbSpawn)
		]]
		self:OrbSpawn()
		self:ScheduleRepeatingEvent(self.OrbSpawn, timer.orbSpawn, self)
	end, timer.orbSpawnFirst)
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end

------------------------------
--      Event Handlers      --
------------------------------

function module:Event(msg)
	if strfind(msg, L["trigger_flameBuffet"]) then
		self:FlameBuffet()
	elseif msg == L["trigger_wingBuffet"] then
		self:Sync(syncName.wingbuffet)
	elseif msg == L["trigger_shadowFlame"] then 
		self:Sync(syncName.shadowflame)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:Event(L["trigger_wingBuffet"])
	module:Event(L["trigger_shadowFlame"])
	module:Event(gsub(L["trigger_flameBuffet"], "?", ""))

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
