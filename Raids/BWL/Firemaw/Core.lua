------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.firemaw
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20020 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"wingbuffet", "shadowflame", "flamebuffet", "orbSpawn", "orbDespawn", "bosskill"}


-- locals
module.timer = {
	firstWingbuffet = 30,
	wingbuffet = 30,
	wingbuffetCast = 1,
	firstShadowflame = 25,
	shadowflame = 14.5,  -- 14.5 - 15
	shadowflameCast = 2,
	--[[
	From the boss.
	Delayed a bit if casting Shadow Flame when the timer is up (don't know about Wing buffet, probably too).
	Orb spams it every 1.5 sec.
	]]
	flameBuffet = 4.5,
	orbSpawn = 45,
	orbSpawnFirst = 21,
	orbDespawn = 120,
	orbSpawnSoon = 5,
}
local timer = module.timer

module.icon = {
	wingbuffet = "INV_Misc_MonsterScales_14",
	shadowflame = "Spell_Fire_Incinerate",
	flameBuffet = "Spell_Fire_Fireball",
	orb = "inv_misc_orb_05",
}
local icon = module.icon

module.syncName = {
	wingbuffet = "FiremawWingBuffetX",
	shadowflame = "FiremawShadowflameX",
	orbSpawn = "FiremawOrbSpawn",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.wingbuffet then
        self:WingBuffet()
	elseif sync == syncName.shadowflame then
		self:ShadowFlame()
	elseif sync == syncName.orbSpawn then
		self:OrbSpawn()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:WingBuffet() 
	if 	self.db.profile.wingbuffet then
		self:Message(L["msg_wingBuffet"], "Important")
		self:RemoveBar(L["bar_wingBuffetNext"]) -- remove timer bar
		self:Bar(L["bar_wingBuffetCast"], timer.wingbuffetCast, icon.wingbuffet) -- show cast bar
		self:DelayedBar(timer.wingbuffetCast, L["bar_wingBuffetNext"], timer.wingbuffet - timer.wingbuffetCast, icon.wingbuffet) -- delayed timer bar
        self:DelayedMessage(timer.wingbuffet - 5, L["msg_wingBuffetSoon"], "Attention", nil, nil, true)
	end
end

function module:ShadowFlame()
	if self.db.profile.shadowflame then
        self:Message(L["msg_shadowFlame"], "Important", true, "Alarm")
		self:RemoveBar(L["bar_shadowFlameNext"]) -- remove timer bar
		self:Bar(L["bar_shadowFlameCast"], timer.shadowflameCast, icon.shadowflame) -- show cast bar
        self:DelayedBar(timer.shadowflameCast, L["bar_shadowFlameNext"], timer.shadowflame - timer.shadowflameCast, icon.shadowflame) -- delayed timer bar
	end
end

function module:OrbSpawn()
	if self.db.profile.orbSpawn then
		self:Bar(L["bar_orbSpawn"], timer.orbSpawn, icon.orb, true, "orange")
	end
	if self.db.profile.orbDespawn then
		self:Bar(format(L["bar_orbDespawn"], self.orbNum), timer.orbDespawn, icon.orb, true, "white")
	end
	self:ScheduleEvent(self.OrbSpawnSoon, timer.orbSpawn - timer.orbSpawnSoon, self)
	self.orbNum = self.orbNum + 1
	if self.orbNum > 3 then
		self.orbNum = 1
	end
end

function module:OrbSpawnSoon()
	if self.db.profile.orbSpawn then
		self:Message(L["msg_orbSpawnSoon"], "Urgent", false, "Alarm")
		self:WarningSign(icon.orb, 1)
	end
end


------------------------------
-- Utility			    	--
------------------------------

function module:FlameBuffet()
	if self.db.profile.flamebuffet then
		--[[ There is no Sync and therefore no ThrottleSync here.
		So manually prevent the bar from restarting for every target hit (or resisted, whatever) by Flame Buffet.
		Otherwise it causes micro-freeze on weak PC each time when Flame Buffet hits ~40 units. ]]
		local registered, time, elapsed = self:BarStatus(L["bar_flameBuffet"])
		if not registered or time - elapsed < 0.5 then
			self:Bar(L["bar_flameBuffet"], timer.flameBuffet, icon.flameBuffet)
		end
	end
end

----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	module.orbNum = 1
	
	-- check core functions
	module:WingBuffet()
	module:ShadowFlame()
	module:OrbSpawn()
	module:OrbSpawnSoon()
	module:FlameBuffet()

	module:BigWigs_RecvSync(syncName.wingbuffet)
	module:BigWigs_RecvSync(syncName.shadowflame)
	module:BigWigs_RecvSync(syncName.orbSpawn)
end
