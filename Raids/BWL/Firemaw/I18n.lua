------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.bwl.firemaw
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Firemaw",
	
	-- commands
	flamebuffet_cmd = "flamebuffet",
	flamebuffet_name = "Flame Buffet alert",
	flamebuffet_desc = "Warn when Flamegor or an orb casts Flame Buffet.\nNot much useful because the boss and the orbs use the same spell and mess up the timer.",

	wingbuffet_cmd = "wingbuffet",
	wingbuffet_name = "Wing Buffet alert",
	wingbuffet_desc = "Warn when Flamegor casts Wing Buffet.",

	shadowflame_cmd = "shadowflame",
	shadowflame_name = "Shadow Flame alert",
	shadowflame_desc = "Warn when Flamegor casts Shadow Flame.",
	
	orbSpawn_cmd = "orbSpawn",
	orbSpawn_name = "Orb spawn alert",
	orbSpawn_desc = "Bar and warning for estimated orb spawn.\n\nNote: the timer is static and does not adjust itself if the real spawn time is different.",
	
	orbDespawn_cmd = "orbDespawn",
	orbDespawn_name = "Orbs despawn bars",
	orbDespawn_desc = "Bars for orbs despawn.\n\nNote: the timer is static and does not adjust itself if the real spawn or despawn time is different.",
	
	-- triggers
	trigger_wingBuffet = "Firemaw begins to cast Wing Buffet.",
	trigger_shadowFlame = "Firemaw begins to cast Shadow Flame.",
	trigger_flameBuffet = "Firemaw ?'s Flame Buffet",
	
	-- messages
	msg_wingBuffet = "Wing Buffet! Next one in 30 seconds!",
	msg_wingBuffetSoon = "TAUNT now! Wing Buffet soon!",
	msg_shadowFlame = "Shadow Flame incoming!",
	msg_orbSpawnSoon = "Orb spawn in 5 sec!",
	
	-- bars
	bar_wingBuffetCast = "Wing Buffet",
	bar_wingBuffetNext = "Next Wing Buffet",
	bar_wingBuffetFirst = "Initial Wing Buffet",
	bar_shadowFlameCast = "Shadow Flame",
	bar_shadowFlameNext = "Next Shadow Flame",
	bar_flameBuffet = "Flame Buffet",
	bar_orbSpawn = "Orb spawn",
	bar_orbDespawn = "Orb %d despawn",
	
	-- misc

} end)

L:RegisterTranslations("deDE", function() return {
	cmd = "Feuerschwinge",
	-- commands
	flamebuffet_name = "Alarm f\195\188r Flammenpuffer",
	flamebuffet_desc = "Warnung f\195\188r Flammenpuffer.",

	wingbuffet_name = "Alarm f\195\188r Fl\195\188gelsto\195\159",
	wingbuffet_desc = "Warnung, wenn Ebonroc Fl\195\188gelsto\195\159 wirkt.",

	shadowflame_name = "Alarm f\195\188r Schattenflamme",
	shadowflame_desc = "Warnung, wenn Ebonroc Schattenflamme wirkt.",
	
	-- triggers
	trigger_wingBuffet = "Ebonroc beginnt Fl\195\188gelsto\195\159 zu wirken.",
	trigger_shadowFlame = "Ebonroc beginnt Schattenflamme zu wirken.",
	trigger_flameBuffetAfflicted = "von Flammenpuffer betroffen",
	trigger_flameBuffetResisted = "Flammenpuffer(.+) widerstanden",
	trigger_flameBuffetImmune = "Flammenpuffer(.+) immun",
	trigger_flameBuffetAbsorbYou = "Ihr absorbiert Firemaws Flammenpuffer",
	trigger_flameBuffetAbsorbOther = "Flammenpuffer von Firemaw wird absorbiert von",
	
	-- messages
	msg_wingBuffet = "Fl\195\188gelsto\195\159! N\195\164chster in 30 Sekunden!",
	msg_wingBuffetSoon = "Jetzt TAUNT! Fl\195\188gelsto\195\159 bald!",
	msg_shadowFlame = "Schattenflamme bald!",
	
	-- bars
	bar_wingBuffetCast = "Fl\195\188gelsto\195\159",
	bar_wingBuffetNext = "N\195\164chster Fl\195\188gelsto\195\159",
	bar_wingBuffetFirst = "Erster Fl\195\188gelsto\195\159",
	bar_shadowFlameCast = "Schattenflamme",
	bar_shadowFlameNext = "Mögliche Schattenflamme",
	bar_flameBuffet = "Flammenpuffer",
	
	-- misc

} end)
