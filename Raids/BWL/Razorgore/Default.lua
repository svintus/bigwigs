local bossName = BigWigs.bossmods.bwl.razorgore
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)

------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 20039 -- To be overridden by the module!

------------------------------
-- Initialization      		--
------------------------------

module:RegisterYellEngage(L["trigger_engage"])

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_MONSTER_YELL")
	self:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_BUFF")
	self:RegisterEvent("CHAT_MSG_MONSTER_EMOTE")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_HOSTILEPLAYER_DAMAGE", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE", "Events")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE", "EventVolley")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_PARTY_DAMAGE", "EventVolley")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_SELF_DAMAGE", "EventVolley")
	self:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE")
	self:RegisterEvent("CHAT_MSG_COMBAT_FRIENDLY_DEATH")

	self:ThrottleSync(5, syncName.egg)
	self:ThrottleSync(5, syncName.orb)
	self:ThrottleSync(5, syncName.orbOver)
	self:ThrottleSync(3, syncName.volley)
	self:ThrottleSync(5, syncName.volleyHit)
	self:ThrottleSync(timer.eternalLivingflame - 1, syncName.eternalLivingflame)
end


-- called after module is enabled and after each wipe
function module:OnSetup()
	self.phase = 0
	self.enraged = false
	self.previousorb = nil
	self.eggs = 0
end

-- called after boss is engaged
function module:OnEngage()
	self:ScheduleRepeatingEvent("orbcontrol_check", self.OrbControlCheck, 0.2, self)
	self:Engaged()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
-- Event Handlers      		--
------------------------------
function module:CHAT_MSG_MONSTER_YELL(msg)
	if msg == L["trigger_destroyEgg1"] or msg == L["trigger_destroyEgg2"] or msg == L["trigger_destroyEgg3"] then
		self:Sync(syncName.egg .. " " .. tostring(self.eggs + 1))
	end
end

function module:CHAT_MSG_SPELL_FRIENDLYPLAYER_BUFF(msg)
	if string.find(msg, L["trigger_egg"]) then
		self:Sync(syncName.eggStart)
	end
end

function module:CHAT_MSG_MONSTER_EMOTE(msg, name)
	msg = gsub(msg, "%%s", name)
	if msg == L["trigger_destroyEggEmote"] then
		-- as of now, this does also fire on finished 'Destroy Egg' cast.
		-- but only after a successful one and the range is shitty of this emote.
		self:Sync(syncName.egg .. " " .. tostring(self.eggs + 1))
	elseif msg == L["trigger_phase2"] then
		-- there is a really funny emote text bug on the current version on Nostalris, I'll only use this in case they fix it
		self:Sync(syncName.phase2)
	end
end

function module:Events(msg)
	local _, _, mcother = string.find(msg, L["trigger_mindControlOtherGain"])
	local _, _, mcotherend = string.find(msg, L["trigger_mindControlOtherGone"])
	local _, _, polyother = string.find(msg, L["trigger_polymorphOtherGain"])
	local _, _, polyotherend = string.find(msg, L["trigger_polymorphOtherGone"])
	local _, _, orbother = string.find(msg, L["trigger_orbControlOther"])
	local _, _, mentalStabilityOther = string.find(msg, L["trigger_mentalStabilityOther"])
	local _, _, mentalStabilityOtherStop = string.find(msg, L["trigger_mentalStabilityOtherStop"])

	if mcother then
		self:MindControl(mcother)
	elseif msg == L["trigger_mindControlYouGain"] then
		self:MindControl(UnitName("player"))
	elseif mcotherend then
		self:MindControlGone(mcotherend)
	elseif msg == L["trigger_mindControlYouGone"] then
		self:MindControlGone(UnitName("player"))
	elseif polyother then
		self:Polymorph(polyother)
	elseif msg == L["trigger_polymorphYouGain"] then
		self:Polymorph(UnitName("player"))
	elseif polyotherend then
		self:PolymorphGone(polyotherend)
	elseif msg == L["trigger_polymorphYouGone"] then
		self:PolymorphGone(UnitName("player"))
	--[[elseif orbother then
		self:Sync(syncName.orb .. " " .. orbother)
	elseif msg == L["trigger_orbControlYou"] then
		self:Sync(syncName.orb .. " " .. UnitName("player"))]]
	elseif string.find(msg, L["trigger_conflagration"]) then
		self:Conflagration()
	elseif msg == L["trigger_mentalStabilityYou"] then
		self:Sync(syncName.mentalStability .. "_" .. UnitName("player") .. " " .. UnitName("player"))
	elseif mentalStabilityOther then
		self:Sync(syncName.mentalStability .. "_" .. mentalStabilityOther .. " " .. mentalStabilityOther)
	elseif msg == L["trigger_mentalStabilityYouStop"] then
		self:Sync(syncName.mentalStabilityStop .. "_" .. UnitName("player") .. " " .. UnitName("player"))
	elseif mentalStabilityOtherStop then
		self:Sync(syncName.mentalStabilityStop .. "_" .. mentalStabilityOtherStop .. " " .. mentalStabilityOtherStop)
	elseif msg == L["trigger_eternalLivingflameYou"] then
		self:Sync(syncName.eternalLivingflame)
		self:EternalLivingflameYou()
	elseif msg == L["trigger_eternalLivingflameYouStop"] then
		self:EternalLivingflameYouStop()
	elseif strfind(msg, L["trigger_eternalLivingflame"]) then
		self:Sync(syncName.eternalLivingflame)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(msg)
	if msg == L["trigger_enrage"] then
		self:Sync(syncName.enrage)
	end
end

function module:EventVolley(msg)
	if msg == L["trigger_volley"] then
		self:Sync(syncName.volley)
	-- Fireball Volley is instant during Enrage so track dmg taken to show the bar for the next volley
	elseif strfind(msg, L["trigger_volleyHit"]) and self.enraged then
		self:Sync(syncName.volleyHit)
	end
end

function module:CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE(msg)
	if msg == L["trigger_volley"] then
		self:Sync(syncName.volley)
	end
end

function module:CHAT_MSG_COMBAT_FRIENDLY_DEATH(msg)
	local _, _, name = strfind(msg, L["trigger_deathOther"])
	if not name then  -- "You die."
		name = UnitName("player")
	end
	self:Sync(format("%s_%s %s", syncName.playerDead, name, name))
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_MONSTER_YELL(L["trigger_destroyEgg1"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_destroyEgg2"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_destroyEgg3"])
	module:CHAT_MSG_SPELL_FRIENDLYPLAYER_BUFF(L["trigger_egg"])
	module:CHAT_MSG_MONSTER_EMOTE(gsub(L["trigger_destroyEggEmote"], self.translatedName, "%%s"), self.translatedName)
	module:CHAT_MSG_MONSTER_EMOTE(L["trigger_phase2"], "")
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(L["trigger_enrage"])
	module:EventVolley(L["trigger_volley"])
	module.enraged = true
	module:EventVolley(L["trigger_volleyHit"])
	module.enraged = false
	module:CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE(L["trigger_volley"])
	module:Events(L["trigger_mindControlOtherGain"])
	module:Events(L["trigger_mindControlOtherGone"])
	module:Events(L["trigger_polymorphOtherGain"])
	module:Events(L["trigger_polymorphOtherGain"])
	module:Events(L["trigger_polymorphOtherGone"])
	module:Events(L["trigger_orbControlOther"])
	module:Events(L["trigger_mindControlYouGain"])
	module:Events(L["trigger_polymorphYouGain"])
	module:Events(L["trigger_polymorphYouGone"])
	module:Events(L["trigger_mentalStabilityYou"])
	module:Events(gsub(L["trigger_mentalStabilityOther"], "%(%.%+%)", UnitName("player") .. 1))
	module:Events(L["trigger_mentalStabilityYouStop"])
	module:Events(gsub(L["trigger_mentalStabilityOtherStop"], "%(%.%+%)", UnitName("player") .. 1))
	module:Events(L["trigger_eternalLivingflame"])
	module:Events(L["trigger_eternalLivingflameYou"])
	module:Events(L["trigger_eternalLivingflameYouStop"])
	module:CHAT_MSG_COMBAT_FRIENDLY_DEATH(gsub(L["trigger_deathOther"], "%(%.%+%)", UnitName("player")))
	module:CHAT_MSG_COMBAT_FRIENDLY_DEATH(gsub(L["trigger_deathOther"], "%(%.%+%)", UnitName("player") .. 1))

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
