------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.razorgore
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20039 -- To be overridden by the module!
local controller = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")["Grethok the Controller"]
module.enabletrigger = { module.translatedName, controller } -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = { "phase", "mobs", "eggs", "polymorph", "mc", "icon", "orb", "mentalStability", "fireballvolley", "fireballvolleyNext", "ktm", "conflagration", "conflagrationNext", "eternalLivingflame", "enrage", "bosskill" }


-- locals
module.timer = {
	mobspawn = 45,
	mc = 15,
	polymorph = 20,
	conflagration = 10,
	conflagrationNext = 23,
	eternalLivingflameFirst = 35,
	eternalLivingflame = 50,
	volley = 3,
	volleyNext = 15,
	egg = 1,
	orb = 90,
	mentalStability = 90,
}
local timer = module.timer

module.icon = {
	mobspawn = "Spell_Holy_PrayerOfHealing",
	egg = "INV_Misc_MonsterClaw_02",
	eggDestroyed = "inv_egg_01",
	orb = "INV_Misc_Gem_Pearl_03",
	volley = "Spell_Fire_FlameBolt",
	conflagration = "Spell_Fire_Incinerate",
	eternalLivingflame = "Spell_Fire_Fire",
	mindControl = "Spell_Shadow_ShadowWordDominate",
	polymorph = "Spell_Nature_Brilliance",
	mentalStability = "Spell_Arcane_MindMastery",
}
local icon = module.icon

module.syncName = {
	egg = "RazorgoreEgg",
	eggStart = "RazorgoreEggStart",
	orb = "RazorgoreOrbStart",
	orbOver = "RazorgoreOrbStop",
	volley = "RazorgoreVolleyCast",
	volleyHit = "RazorgoreVolleyHit",
	phase2 = "RazorgorePhaseTwo",
	eternalLivingflame = "RazorgoreEternalLivingflame",
	mentalStability = "RazorgoreMentalStability",
	mentalStabilityStop = "RazorgoreMentalStabilityStop",
	enrage = "RazorgoreEnrage",
	playerDead = "RazorgorePlayerDead",
}
local syncName = module.syncName

module.doCheckForWipe = nil

------------------------------
-- Override CheckForWipe  	--
------------------------------
function module:CheckForWipe(event)
    if module.doCheckForWipe then
        BigWigs:DebugMessage("doCheckForWipe")
        BigWigs:CheckForWipe(self)
    end
end


------------------------------
-- Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.egg and rest then
		self:EggDestroyed(rest)
	elseif sync == syncName.eggStart then
		self:DestroyingEgg()
	elseif sync == syncName.orb then
		self:OrbStart(rest)
	elseif sync == syncName.orbOver then
		self:OrbEnd()
	elseif sync == syncName.volley then
		self:Volley()
	elseif sync == syncName.volleyHit then
		self:VolleyHit()
	elseif sync == syncName.phase2 then
		self:Phase2()
	elseif sync == syncName.eternalLivingflame then
		self:EternalLivingflame()
	elseif strfind(sync, syncName.mentalStability) then
		self:MentalStability(rest)
	elseif strfind(sync, syncName.mentalStabilityStop) then
		self:MentalStabilityStop(rest)
	elseif sync == syncName.enrage then
		self:Enrage()
	elseif strfind(sync, syncName.playerDead) then
		self:PlayerDead(rest)
	end
end

------------------------------
-- Sync Handlers	    --
------------------------------
function module:EggDestroyed(number)
	number = tonumber(number)
	if number == (self.eggs + 1) and self.eggs <= 30 then
		self.eggs = self.eggs + 1
		if self.db.profile.eggs then
			self:Message(string.format(L["msg_egg"], self.eggs), "Positive")
		end
		self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_eggsDestroyed"], (30 - self.eggs))
	elseif number == (self.eggs + 1) and number == 30 and self.phase ~= 2 then
		self:Sync(syncName.phase2)
	end
end

function module:DestroyingEgg()
	--self:CancelScheduledEvent("destroyegg_check")
	--self:ScheduleEvent("destroyegg_check", self.DestroyEggCheck, 3, self)
	if self.db.profile.eggs then
		self:Bar(L["bar_egg"], timer.egg, icon.egg, true, "white")
	end
	self:Sync(syncName.egg .. " " .. tostring(self.eggs + 1))
end

function module:OrbStart(name)
	if name == self.previousorb then
		return
	end
	self:CancelScheduledEvent("destroyegg_check")

	if self.db.profile.orb then
		if self.previousorb ~= nil then
			self:RemoveBar(string.format(L["bar_orb"], self.previousorb))
		end
		-- set the timer to controller's Mental Stability remaining time if possible
		local _, duration, elapsed = self:BarStatus(format(L["bar_mentalStability"], name))
		local timeLeft = timer.orb
		if duration and elapsed then  -- both will be nil if Mental Stability bars are disabled
			timeLeft = duration - elapsed
		end
		self:Bar(string.format(L["bar_orb"], name), timeLeft, icon.orb, true, "white")
		self:SetCandyBarOnClick("BigWigsBar " .. string.format(L["bar_orb"], name), function(name, button, extra)
			TargetByName(extra, true)
		end, name)
	end
	self.previousorb = name
end

function module:OrbEnd()
	self:CancelScheduledEvent("destroyegg_check")

	if self.db.profile.orb and self.previousorb then
		self:RemoveBar(string.format(L["bar_orb"], self.previousorb))
	end
	self.previousorb = nil
	if self.db.profile.fireballvolley then
		self:RemoveBar(L["bar_volley"])
	end
	if self.db.profile.eggs then
		self:RemoveBar(L["bar_egg"])
	end
end

function module:Volley()
	if self.db.profile.fireballvolley then
		self:Bar(L["bar_volley"], timer.volley, icon.volley)
		self:Message(L["msg_volley"], "Urgent")
		-- Force the sign in case the boss starts casting Fireball Volley while you are standing in Eternal Livingflame.
		-- In that case it is needed to show that you have to run not just out of the fire, but also LoS the Fireball Volley.
		self:WarningSign(icon.volley, timer.volley, true)
	end
	if self.db.profile.fireballvolleyNext and self.phase == 2 then
		self:DelayedBar(timer.volley, L["bar_volleyNext"], timer.volleyNext - timer.volley, icon.volley)
	end
end

function module:VolleyHit()
	if self.db.profile.fireballvolley and self.enraged then
		--[[
		There may be a following situation: finished casting volley -> enrage -> flying volley hits someone.
		In that case the next volley bar will reset that we don't want.
		So check elapsed time and if it is too small then don't restart the bar.
		]]
		local _, _, elapsed = self:BarStatus(L["bar_volleyNext"])
		if elapsed and elapsed < 4 then
			return
		else
			self:Bar(L["bar_volleyNext"], timer.volleyNext, icon.volley)
		end
	end
end

function module:Conflagration()
	if self.db.profile.conflagration then
		self:Bar(L["bar_conflagration"], timer.conflagration, icon.conflagration)
	end
	if self.db.profile.conflagrationNext and self.phase == 2 then
		self:DelayedBar(timer.conflagration, L["bar_conflagrationNext"], timer.conflagrationNext - timer.conflagration, icon.conflagration)
	end
end

function module:EternalLivingflame()
	if self.db.profile.eternalLivingflame then
		self:Message(L["msg_eternalLivingflame"], "Attention")
		self:Bar(L["bar_eternalLivingflame"], timer.eternalLivingflame, icon.eternalLivingflame)
	end
end

function module:EternalLivingflameYou()
	if self.db.profile.eternalLivingflame then
		self:WarningSign(icon.eternalLivingflame, 60)  -- not 60 sec actually but until you move out of it
	end
end

function module:EternalLivingflameYouStop()
	if self.db.profile.eternalLivingflame then
		self:RemoveWarningSign(icon.eternalLivingflame)
	end
end

function module:Phase2()
	if self.phase < 2 then
		self.phase = 2
		self:CancelScheduledEvent("destroyegg_check")
		self:CancelScheduledEvent("orbcontrol_check")
		if self.previousorb ~= nil and self.db.profile.orb then
			self:RemoveBar(string.format(L["bar_orb"], self.previousorb))
		end
		if self.db.profile.eggs then
			self:RemoveBar(L["bar_egg"])
		end
		self:RemoveBar(L["bar_eggsDestroyed"])
		if self.db.profile.phase then
			self:Message(L["msg_phase2"], "Attention")
		end
		self:RemoveBars(gsub(L["bar_mentalStability"], "%%s", ".+"))
		if self.db.profile.eternalLivingflame then
			self:Bar(L["bar_eternalLivingflame"], timer.eternalLivingflameFirst, icon.eternalLivingflame)
		end

		self:KTM_SetTarget(self.translatedName)
		if self.db.profile.ktm then
			self:KTM_Reset()
		end
	end
end

function module:MentalStability(name)
	if self.db.profile.mentalStability then
		if name == UnitName("player") then
			self:Message(format(L["msg_mentalStabilityYou"], name), "Personal", true, "Alarm")
			self:Message(format(L["msg_mentalStabilityOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.mentalStability, 2)
		else
			self:Message(format(L["msg_mentalStabilityOther"], name), "Attention")
		end
		self:Bar(format(L["bar_mentalStability"], name), timer.mentalStability, icon.mentalStability)
	end
end

function module:MentalStabilityStop(name)
	self:RemoveBar(format(L["bar_mentalStability"], name))
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.mentalStability)
	end
end

function module:Enrage()
	self.enraged = true
	if self.db.profile.enrage then
		self:Message(L["msg_enrage"], "Important")
	end
end

function module:PlayerDead(name)
	if name == self.previousorb then
		self:OrbEnd()
	end
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.mentalStability)
		self:EternalLivingflameYouStop()
	end
end


----------------------------------
-- Utility			    		--
----------------------------------
function module:Engaged()
	if self.db.profile.phase then
		self:Message(L["msg_engage"], "Attention")
	end
	if self.db.profile.mobs then
		self:Bar(L["bar_mobs"], timer.mobspawn, icon.mobspawn, true, BigWigsColors.db.profile.start)
		self:DelayedMessage(timer.mobspawn - 5, L["msg_mobsSoon"], "Important")
	end
	self:TriggerEvent("BigWigs_StartCounterBar", self, L["bar_eggsDestroyed"], 30, "Interface\\Icons\\" .. icon.eggDestroyed)
	self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_eggsDestroyed"], (30 - 0.1))
	
	-- do not check for wipe until the first wave of mobs since we get out of combat in between
	module.doCheckForWipe = false
	local function DelayCheckForWipe()
		module.doCheckForWipe = true
	end
	self:ScheduleEvent("BigWigsRazorgoreDelayedCheckForWipe", DelayCheckForWipe, timer.mobspawn + 5, self)
end

function module:MindControl(name)
	if self.db.profile.icon then
		self:Icon(name)
	end

	if self.db.profile.mc then
		self:Message(string.format(L["msg_mindControlOther"], name), "Important")
		self:Bar(string.format(L["bar_mindControl"], name), timer.mc, icon.mindControl, true, BigWigsColors.db.profile.mindControl)
		self:SetCandyBarOnClick("BigWigsBar " .. string.format(L["bar_mindControl"], name), function(name, button, extra) TargetByName(extra, true) end, name)
	end
end

function module:MindControlGone(name)
	self:RemoveBar(string.format(L["bar_mindControl"], name))
end

function module:Polymorph(name)
	if self.db.profile.polymorph then
		self:Message(string.format(L["msg_polymorphOther"], name), "Important")
		self:Bar(string.format(L["bar_polymorph"], name), timer.polymorph, icon.polymorph)
		self:SetCandyBarOnClick("BigWigsBar " .. string.format(L["bar_polymorph"], name), function(name, button, extra) TargetByName(extra, true) end, name)
	end
end

function module:PolymorphGone(name)
	self:RemoveBar(string.format(L["bar_polymorph"], name))
end

--[[
There can be a rare scenario when the orb sync won't work:
orb control ended, the same player got Mental Stability and started controlling again before OrbControlCheck() procced.
In this case we will have UnitName("raid" .. 1) == self.previousorb and sync won't run.
Maybe need to get a timer or check if the bar is still running for this case.
Although the bar can be turned off in the settings, so would need an independent timer.

Add: this is probably not possible anymore since when Mental Stability ends it stuns the controller so he can't retake the control quickly even if he gets another Mental Stability. Can that stun be resisted or removed though?

Also can Mental Stability be put on a player who already has it to refresh the duration?
That would require to update the timer for both Mental Stability and the orb control bars.
Although that won't be displayed in combatl og I guess so the only way would be sync from the controller.
]]
function module:OrbControlCheck()
	for i = 1, GetNumRaidMembers() do
		if UnitName("raidpet" .. i) == self.translatedName then
			if UnitName("raid" .. i) ~= self.previousorb then
				self:Sync(syncName.orb .. " " .. UnitName("raid" .. i))
			end
			return
		end
	end
	if GetSubZoneText() == L["misc_subzone"] and self.previousorb then
		self:Sync(syncName.orbOver)
	end
end

function module:DestroyEggCheck()
	local bosscontrol = false
	for i = 1, GetNumRaidMembers() do
		if UnitName("raidpet" .. i) == self.translatedName then
			bosscontrol = true
			break
		end
	end
	if bosscontrol then
		--self:TriggerEvent("BigWigs_SendSync", "RazorgoreEgg "..tostring(self.eggs + 1))
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:BigWigs_RecvSync(syncName.egg, 1)
	module:BigWigs_RecvSync(syncName.eggStart)
	module:BigWigs_RecvSync(syncName.orbOver)
	module:BigWigs_RecvSync(syncName.volley)
	module.enraged = true
	module:BigWigs_RecvSync(syncName.volleyHit)
	module.enraged = false
	module:BigWigs_RecvSync(syncName.phase2)
	module:BigWigs_RecvSync(syncName.eternalLivingflame)
	module:BigWigs_RecvSync(syncName.mentalStability, UnitName("player"))
	module:BigWigs_RecvSync(syncName.mentalStabilityStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.enrage)
	module:BigWigs_RecvSync(syncName.playerDead, UnitName("player"))
end
