local bossName = BigWigs.bossmods.bwl.shredder
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 14 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

module:RegisterYellEngage(L["trigger_engage"])

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF")
	self:RegisterEvent("CHAT_MSG_MONSTER_YELL")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE", "EventGrenades")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "EventGrenades")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "EventGrenades")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE", "EventGrenades")
	
	self:ThrottleSync(4.5, syncName.grenades)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.iceSprinkler = false
	self.announceGrenadesInterval = true
end

-- called after boss is engaged
function module:OnEngage()
	if self.db.profile.enrage then
		self:Bar(L["bar_enrage"], timer.enrage, icon.enrage, true, BigWigsColors.db.profile.enrage)
	end	
	if self.db.profile.grenades then
		self:Bar(L["bar_grenades"], timer.grenadesFirst, icon.grenades, true, "Orange")
	end
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(msg)
	if msg == L["trigger_cokeEjection"] then
		self:Sync(syncName.cokeEjection)
	end
end

function module:CHAT_MSG_MONSTER_YELL(msg)
	if msg == L["trigger_iceSprinklerSoon"] then
		self:Sync(syncName.iceSprinklerSoon)
	elseif msg == L["trigger_enrage"] then
		self:Sync(syncName.enrage)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(msg)
	if msg == L["trigger_iceSprinkler"] then
		self:Sync(syncName.iceSprinkler)
	end
end

function module:CHAT_MSG_SPELL_AURA_GONE_OTHER(msg)
	if msg == L["trigger_iceSprinklerStop"] then
		self:Sync(syncName.iceSprinklerStop)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE(msg)
	if msg == L["trigger_sawblades"] then
		self:Sync(syncName.sawblades)
	end
end

function module:EventGrenades(msg)
	if strfind(msg, L["trigger_stickyOilTar"]) or strfind(msg, L["trigger_thoriumGrenades"]) then
		self:Sync(syncName.grenades)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(L["trigger_cokeEjection"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_iceSprinklerSoon"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_enrage"])
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(L["trigger_iceSprinkler"])
	module:CHAT_MSG_SPELL_AURA_GONE_OTHER(L["trigger_iceSprinklerStop"])
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE(L["trigger_sawblades"])
	self.announceGrenadesInterval = true
	module:EventGrenades(L["trigger_stickyOilTar"])
	self.announceGrenadesInterval = false
	module:EventGrenades(L["trigger_thoriumGrenades"])
	
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
