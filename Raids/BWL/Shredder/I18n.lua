------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.bwl.shredder
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Shredder",
	
	-- commands
	cokeEjection_cmd = "cokeEjection",
	cokeEjection_name = "Coke Ejection",
	cokeEjection_desc = "Announce when the boss begins to cast Coke Ejection.",
	
	iceSprinkler_cmd = "iceSprinkler",
	iceSprinkler_name = "Ice Sprinkler",
	iceSprinkler_desc = "Announcement for Ice Sprinkler.",
	
	sawblades_cmd = "sawblades",
	sawblades_name = "Sawblades",
	sawblades_desc = "Timer for Sawblades return.",	
	
	grenades_cmd = "grenades",
	grenades_name = "Grenades + Sticky Oil Tar",
	grenades_desc = "Timer for throwing Heavy Thorium Grenades + Sticky Oil Tar.\n\nNote: it is not very accurate.",
	
	enrage_cmd = "enrage",
	enrage_name = "Enrage",
	enrage_desc = "4:59 min enrage timer and announcement.",
	
	-- triggers
	trigger_engage = "Combat protocol loaded!",
	trigger_cokeEjection = "Elementium Decapitator Mk III begins to cast Coke Ejection.",
	trigger_iceSprinklerSoon = "Fire detected! Loading extinguish protocol.",
	trigger_iceSprinkler = "Elementium Decapitator Mk III gains Ice Sprinkler.",
	trigger_iceSprinklerStop = "Ice Sprinkler fades from Elementium Decapitator Mk III.",
	trigger_sawblades = "Sawblade 1 is afflicted by Saw Blade.",
	trigger_stickyOilTar = "afflicted by Sticky Oil Tar",
	trigger_thoriumGrenades = "Elementium Decapitator Mk III ?'s Heavy Thorium Grenade",
	trigger_enrage = "Engine overheat detected! Excess pressure detected! Openning  all relief valves...",
	
	-- messages
	msg_cokeEjection = "Casting Coke Ejection!",
	msg_iceSprinklerSoon = "Ice Sprinkler incoming!",
	msg_grenades = "Grenades and Sticky Oil Tar thrown!",
	msg_grenadesTimer = "%g sec Grenades timer!",
	msg_enrage = "Enrage!",
	
	-- bars
	bar_cokeEjection = "Coke Ejection",
	bar_iceSprinkler = "Ice Sprinkler",
	bar_iceSprinklerSoon = "Ice Sprinkler incoming",
	bar_sawblades = "Sawblades return",
	bar_grenades = "Grenades & Sticky Oil Tar",
	bar_enrage = "Enrage",
	
	-- misc	
	
} end )
