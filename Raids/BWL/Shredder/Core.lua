------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.shredder
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 14 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"cokeEjection", "iceSprinkler", "sawblades", "grenades", "enrage", "bosskill"}


-- locals
module.timer = {
	cokeEjection = 2.5,
	iceSprinklerSoon = 3,
	iceSprinkler = 60,
	sawblades = 20,
	grenadesFast = 5,
	grenadesSlow = 7.5,
	grenadesFirst = 14.5,
	enrage = 299,
}
local timer = module.timer

module.icon = {
	cokeEjection = "Spell_Fire_MeteorStorm",
	iceSprinkler = "Spell_Frost_FrostNova",
	sawblades = "INV_Misc_SawBlade_01",
	grenades = "INV_Misc_Bomb_08",
	enrage = "Spell_Shadow_UnholyFrenzy",
}
local icon = module.icon

module.syncName = {
	cokeEjection = "ShredderCokeEjection",
	iceSprinklerSoon = "ShredderIceSprinklerSoon",
	iceSprinkler = "ShredderIceSprinkler",
	iceSprinklerStop = "ShredderIceSprinklerStop",
	sawblades = "ShredderSawblades",
	grenades = "ShredderGrenades",
	enrage = "ShredderEnrage",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.cokeEjection then
		self:CokeEjection()
	elseif sync == syncName.iceSprinklerSoon then
		self:IceSprinklerSoon()
	elseif sync == syncName.iceSprinkler then
		self:IceSprinkler()
	elseif sync == syncName.iceSprinklerStop then
		self:IceSprinklerStop()
	elseif sync == syncName.sawblades then
		self:Sawblades()
	elseif sync == syncName.grenades then
		self:Grenades()
	elseif sync == syncName.enrage then
		self:Enrage()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:CokeEjection()
	if self.db.profile.cokeEjection then
		self:Message(L["msg_cokeEjection"], "Attention")
		self:Bar(L["bar_cokeEjection"], timer.cokeEjection, icon.cokeEjection)
	end
end

function module:IceSprinklerSoon()
	if self.db.profile.iceSprinkler then
		self:Message(L["msg_iceSprinklerSoon"], "Attention")
		self:Bar(L["bar_iceSprinklerSoon"], timer.iceSprinklerSoon, icon.iceSprinkler)
	end
end

function module:IceSprinkler()
	self.iceSprinkler = true
	self.announceGrenadesInterval = true
	if self.db.profile.iceSprinkler then
		self:Bar(L["bar_iceSprinkler"], timer.iceSprinkler, icon.iceSprinkler, true, "blue")
	end
end

function module:IceSprinklerStop()
	self.iceSprinkler = false
	self.announceGrenadesInterval = true
end

function module:Sawblades()
	if self.db.profile.sawblades then
		self:Bar(L["bar_sawblades"], timer.sawblades, icon.sawblades)
	end
end

function module:Grenades()
	if self.db.profile.grenades then
		self:Message(L["msg_grenades"], "Orange")
		if self.iceSprinkler then
			self:Bar(L["bar_grenades"], timer.grenadesSlow, icon.grenades, true, "Orange")
			if self.announceGrenadesInterval then
				self:Message(format(L["msg_grenadesTimer"], timer.grenadesSlow), "Positive")
			end
		else
			self:Bar(L["bar_grenades"], timer.grenadesFast, icon.grenades, true, "Orange")
			if self.announceGrenadesInterval then
				self:Message(format(L["msg_grenadesTimer"], timer.grenadesFast), "Attention")
			end
		end
	end
	self.announceGrenadesInterval = false
end

function module:Enrage()
	if self.db.profile.enrage then
		self:RemoveBar(L["bar_enrage"])
		self:Message(L["msg_enrage"], "Important")
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:CokeEjection()
	module:IceSprinklerSoon()
	module:IceSprinkler()
	module:IceSprinklerStop()
	module:Sawblades()
	self.announceGrenadesInterval = true
	module:Grenades()
	self.announceGrenadesInterval = false
	module:Grenades()
	module:Enrage()
	
	module:BigWigs_RecvSync(syncName.cokeEjection)
	module:BigWigs_RecvSync(syncName.iceSprinklerSoon)
	module:BigWigs_RecvSync(syncName.iceSprinkler)
	module:BigWigs_RecvSync(syncName.iceSprinklerStop)
	module:BigWigs_RecvSync(syncName.sawblades)
	self.announceGrenadesInterval = true
	module:BigWigs_RecvSync(syncName.grenades)
	self.announceGrenadesInterval = false
	module:BigWigs_RecvSync(syncName.grenades)
	module:BigWigs_RecvSync(syncName.enrage)
end
