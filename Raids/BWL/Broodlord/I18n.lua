------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.bwl.broodlord
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Broodlord",

	-- commands
	ms_cmd = "ms",
	ms_name = "Mortal Strike",
	ms_desc = "Warn when someone gets Mortal Strike and starts a clickable bar for easy selection.",
	bw_cmd = "bw",
	bw_name = "Blast Wave",
	bw_desc = "Shows a bar with the possible Blast Wave cooldown.\n\n(Disclaimer: this varies anywhere from 20 to 24 seconds. Chosen shortest interval for safety.)",
	reflection_cmd = "reflection",
	reflection_name = "Reflections",
	reflection_desc = "Alert when the boss gains a reflection.",
	trap_cmd = "trap",
	trap_name = "Traps",
	trap_desc = "Announcement and bars for disarmed Suppression Devices.",

	-- triggers
	trigger_engage = "None of your kind should be here",
	trigger_mortalStrike = "^(.+) (.+) afflicted by Mortal Strike",
	trigger_blastWave = "^(.+) (.+) afflicted by Blast Wave",
	trigger_trapYou = "You perform Disarm Trap on Suppression Device.",
	trigger_trapOther = "(.+) performs Disarm Trap on Suppression Device.",
	trigger_reflection = "Broodlord Lashlayer gains (.+) Reflection.",
	trigger_deathYou = "You die\.",
	trigger_deathOther = "(.+) dies\.",

	-- messages
	msg_mortalStrikeYou = "Mortal Strike on you!",
	msg_mortalStrikeOther = "Mortal Strike on %s!",
	msg_blastWave = "Blast Wave soon!",
	msg_trap = "Trap disarmed!",
	msg_reflection = "%s Reflection!",

	-- bars
	bar_mortalStrike = "Mortal Strike: %s",
	bar_blastWave = "Blast Wave",
	bar_trap = "Trap %d reactivation",
	bar_reflection = "%s Reflection",

	-- misc
	misc_you = "You",
	misc_are = "are",
	misc_schoolsSync =
	{
		["Arcane"] = "arcane",
		["Fire"] = "fire",
		["Frost"] = "frost",
		["Holy"] = "holy",
		["Nature"] = "nature",
		["Shadow"] = "shadow",
	},
	misc_schoolsLocalized = {},
}
end)

L:RegisterTranslations("deDE", function() return {
	-- commands
	ms_name = "T\195\182dlicher Sto\195\159",
	ms_desc = "Warnung wenn ein Spieler von Tödlicher Sto\195\159 betroffen ist und startet einen anklickbaren Balken für eine einfache Auswahl.",
	bw_name = "Druckwelle",
	bw_desc = "Zeigt einen Balken mit der möglichen Druckwellenabklingzeit.\n\n(Hinweis: Diese variiert von 8 bis 15 Sekunden. Zur Sicherheit wurde der kürzeste Intervall gewählt.)",

	-- triggers
	trigger_engage = "Euresgleichen sollte nicht hier sein!",
	trigger_mortalStrike = "^(.+) (.+) von T\195\182dlicher Sto\195\159 betroffen",
	trigger_blastWave = "^(.+) (.+) von Druckwelle betroffen",
	trigger_deathYou = "Ihr sterbt.",
	trigger_deathOther = "(.+) stirbt.",

	-- messages
	msg_mortalStrikeYou = "T\195\182dlicher Sto\195\159 auf Dir!",
	msg_mortalStrikeOther = "T\195\182dlicher Sto\195\159 auf %s!",
	msg_blastWave = "Druckwelle bald!",

	-- bars
	bar_mortalStrike = "T\195\182dlicher Sto\195\159: %s",
	bar_blastWave = "Druckwelle",

	-- misc
	misc_you = "Ihr",
	misc_are = "seid",
	misc_schoolsSync = {},
	misc_schoolsLocalized = {},
}
end)

for k, v in L.misc_schoolsSync do
	L.misc_schoolsLocalized[v] = k
end
