------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.broodlord
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20017 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = { "ms", "bw", "trap", "reflection", "bosskill" }


-- locals
module.timer = {
	blastWave = 20,
	mortalStrike = 5,
	reflection = 20,
	--[[
	Reactivation on trash:
		35 - 39
	Reactivation when the boss is pulled:
		54 - 61
	Disarmable again (after getting disarmed) on trash:
		124 - 133
	Disarmable again (after getting disarmed) when the boss is pulled:
		unknown
	Samples for the above numbers are very small, like < 10 each.
	]]
	trap = 54,
}
local timer = module.timer

module.icon = {
	blastWave = "Spell_Holy_Excorcism_02",
	mortalStrike = "Ability_Warrior_SavageBlow",
	trap = "Spell_Fire_WindsofWoe",
	reflections =
	{
		["arcane"] = "Spell_Nature_WispSplode",
		["fire"] = "Spell_Fire_FireArmor",
		["frost"] = "Spell_Frost_FrostArmor02",
		["holy"] = "Spell_Holy_BlessingOfProtection",
		["nature"] = "Spell_Nature_SpiritArmor",
		["shadow"] = "Spell_Shadow_RagingScream",
	},
}
local icon = module.icon

module.syncName = {
	trap = "BroodlordTrap",
	reflection = "BroodlordReflection",
}
local syncName = module.syncName

module.lastBlastWave = 0
module.lastMS = 0
module.MS = ""


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if strfind(sync, syncName.trap) then
		self:Trap()
	elseif sync == syncName.reflection then
		self:Reflection(rest)
	end
end


------------------------------
-- Utility		     		--
------------------------------
function module:MortalStrike(name)
	if self.db.profile.ms then
		if name == UnitName("player") then
			self:Message(L["msg_mortalStrikeYou"], "Core", true, "Beware")
			self:WarningSign(icon.mortalStrike, timer.mortalStrike)
		else
			self:Message(string.format(L["msg_mortalStrikeOther"], name), "Core", true, "Alarm")
		end

		self:Bar(string.format(L["bar_mortalStrike"], name), timer.mortalStrike, icon.mortalStrike, true, BigWigsColors.db.profile.significant)
		self:SetCandyBarOnClick("BigWigsBar " .. string.format(L["bar_mortalStrike"], name), function(name, button, extra) TargetByName(extra, true) end, name)
	end
end

function module:BlastWave()
	if self.db.profile.bw then
		self:Bar(L["bar_blastWave"], timer.blastWave, icon.blastWave)
	end
end

function module:Trap()
	if self.db.profile.trap then
		local i = 1
		while self:BarStatus(format(L["bar_trap"], i)) do  -- find a free bar number
			i = i + 1
		end
		self:Message(L["msg_trap"], "Positive")
		self:Bar(format(L["bar_trap"], i), timer.trap, icon.trap)
	end
end

function module:Reflection(school)
	if self.db.profile.reflection then
		local schoolLocalized = L.misc_schoolsLocalized[school]
		self:Message(format(L["msg_reflection"], schoolLocalized), "Attention")
		self:Bar(format(L["bar_reflection"], schoolLocalized), timer.reflection, icon.reflections[school])
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:MortalStrike(UnitName("player"))
	module:BlastWave()
	module:Trap()
	module:Trap()
	module:Reflection(L.misc_schoolsSync["Frost"])
end
