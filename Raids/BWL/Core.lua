
--------------------------------------
-- Create all modules for this raid	--
--------------------------------------

BigWigs.bossmods.bwl = {}


BigWigs.bossmods.bwl.razorgore = "Razorgore the Untamed"
BigWigs.bossmods.bwl.shredder = "Elementium Decapitator Mk III"
BigWigs.bossmods.bwl.vaelastrasz = "Vaelastrasz the Chained"
BigWigs.bossmods.bwl.broodlord = "Broodlord Lashlayer"
BigWigs.bossmods.bwl.firemaw = "Firemaw"
BigWigs.bossmods.bwl.krixix = "Master Elemental Shaper Krixix"
BigWigs.bossmods.bwl.ebonroc = "Ebonroc"
BigWigs.bossmods.bwl.flamegor = "Flamegor"
BigWigs.bossmods.bwl.ebonrocflamegor = "Ebonroc and Flamegor"
BigWigs.bossmods.bwl.chromaggus = "Chromaggus"
BigWigs.bossmods.bwl.nefarian = "Nefarian"

BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.razorgore, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.shredder, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.vaelastrasz, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.broodlord, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.firemaw, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.krixix, "Blackwing Lair")
--BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.ebonroc, "Blackwing Lair")
--BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.flamegor, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.ebonrocflamegor, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.chromaggus, "Blackwing Lair")
BigWigs:ModuleDeclaration(BigWigs.bossmods.bwl.nefarian, "Blackwing Lair")