------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.bwl.nefarian
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20022 -- To be overridden by the module!
local victor = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")["Lord Victor Nefarius"]
module.enabletrigger = {module.translatedName, victor} -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"mc", "shadowflame", "fear", "classcall", "otherwarn", "bosskill"}

-- locals
module.timer = {
	mobspawn1 = 21,
	mobspawn2 = 14,
	classcall = 30,
	mc = 15,
	shadowflame = 17.8,  -- 17.8 - 24.9
	shadowflameCast = 2,
	fear = 25.4,  -- 25.4 - 30.0
	fearCast = 1.5,
	landing = 12,
	firstClasscall =   42.5,  -- 42.5 - 46.0  after trigger_landingNow
	firstFear =        38.4,  -- 38.4 - 41.0  after trigger_landingNow
	firstShadowflame = 31.7,  -- 31.7 - 36.5  after trigger_landingNow
}
local timer = module.timer

module.icon = {
	mobspawn = "Spell_Holy_PrayerOfHealing",
	classcall = "Spell_Shadow_Charm",
	mc = "Spell_Shadow_Charm",
	fear = "Spell_Shadow_Possession",
	shadowflame = "Spell_Fire_Incinerate",
	landing = "INV_Misc_Head_Dragon_Black",
}
local icon = module.icon

module.syncName = {
	preEngage = "NefarianPreEngage",
	shadowflame = "NefarianShadowflame",
	fear = "NefarianFear",
	landing = "NefarianLandingNOW",
	landingSoon = "NefarianLandingSoon",
	addDead = "NefCounter",
	mindControl = "NefarianMindControl",
	zerg = "NefarianZerg"
}
local syncName = module.syncName


module.warnpairs = {
	[L["trigger_shamans"]] = {L["msg_shaman"], true},
	[L["trigger_druid"]] = {L["msg_druid"], true},
	[L["trigger_warlock"]] = {L["msg_warlock"], true},
	[L["trigger_priest"]] = {L["msg_priest"], true},
	[L["trigger_hunter"]] = {L["msg_hunter"], true},
	[L["trigger_warrior"]] = {L["msg_warrior"], true},
	[L["trigger_rogue"]] = {L["msg_rogue"], true},
	[L["trigger_paladin"]] = {L["msg_paladin"], true},
	[L["trigger_mage"]] = {L["msg_mage"], true},
} 
module.nefCounter = nil
module.nefCounterMax = 80 -- how many adds have to be killed to trigger phase 2?  // saw 79 and 80. 92-94 total
module.phase = nil
module.doCheckForWipe = nil


------------------------------
-- Override CheckForWipe  	--
------------------------------
function module:CheckForWipe(event)
    if module.doCheckForWipe then
        BigWigs:CheckForWipe(self)
    end
end


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.preEngage then
		self:PreEngage()
    elseif sync == syncName.shadowflame then
		self:Shadowflame()
	elseif sync == syncName.fear then
		self:Fear()
	elseif sync == syncName.landingSoon then
		self:LandingSoon()
    elseif sync == syncName.landing then
		self:Landing()
	elseif strfind(sync, syncName.addDead) and rest then
		self:NefCounter(rest)
	elseif sync == syncName.mindControl and rest then
		self:MindControl(rest)
	elseif sync == syncName.zerg then
		self:Zerg()
    end
end

------------------------------
--      Sync Handlers	    --
------------------------------

function module:PreEngage()
	self:Bar(L["bar_mobSpawn"], timer.mobspawn1, icon.mobspawn)
end

function module:Shadowflame()
	if self.db.profile.shadowflame then
		self:RemoveBar(L["bar_shadowFlame"]) -- remove timer bar
		self:Bar(L["bar_shadowFlame"], timer.shadowflameCast, icon.shadowflame) -- show cast bar
		self:Message(L["msg_shadowFlame"], "Important", true, "Alarm")
		self:DelayedBar(timer.shadowflameCast, L["bar_shadowFlame"], timer.shadowflame - timer.shadowflameCast, icon.shadowflame) -- delayed timer bar
	end
end

function module:Fear()
	if self.db.profile.fear then
        self:RemoveBar(L["bar_fear"]) -- remove timer bar
		self:Message(L["msg_fearCast"], "Important", true, "Alert")
		self:Bar(L["msg_fear"], timer.fearCast, icon.fear, true, "Orange") -- show cast bar
		self:DelayedBar(timer.fearCast, L["bar_fear"], timer.fear - timer.fearCast, icon.fear) -- delayed timer bar
        --self:WarningSign(icon.fear, 5)
	end
end

function module:LandingSoon()
	if self.db.profile.otherwarn then
		self:Message(L["msg_landingSoon"], "Important", true, "Long")
	end
end

function module:Landing()
	if not self.phase2 then
        self.phase2 = true
		self:TriggerEvent("BigWigs_StopCounterBar", self, L["misc_drakonidsDead"])
		
        self:Bar(L["msg_landing"], timer.landing, icon.landing, true, BigWigsColors.db.profile.start)
        self:Message(L["msg_landing"], "Important", nil, "Beware")
		
		-- landing in 12s
		if self.db.profile.classcall then
			self:DelayedBar(timer.landing, L["bar_classCall"], timer.firstClasscall - timer.landing, icon.classcall)
		end
        if self.db.profile.fear then
            self:DelayedBar(timer.landing, L["bar_fear"], timer.firstFear - timer.landing, icon.fear)
        end
        if self.db.profile.shadowflame then
            self:DelayedBar(timer.landing, L["bar_shadowFlame"], timer.firstShadowflame - timer.landing, icon.shadowflame)
        end
        
        -- set ktm
        --[[
        Nefarian usually targets healers after trigger_landingNow on V+, so we already start gaining threat,
        does not reset threat after timer.landing on vmangos (probably the same on V+),
        and can be targeted himself.
        So there seems to be no need to delay self:KTM_SetTarget and self:KTM_Reset.
        ]]
        --[[ local function setKTM()
            self:KTM_SetTarget(self:ToString())
            self:KTM_Reset()
        end
        self:ScheduleEvent("bwnefarianktm", setKTM, timer.landing + 1, self) ]]
        self:KTM_SetTarget(self:ToString())
        self:KTM_Reset()
	end
end

function module:NefCounter(n)
	n = tonumber(n)
	if not self.phase2 and n == (module.nefCounter + 1) and module.nefCounter <= module.nefCounterMax then
		module.nefCounter = module.nefCounter + 1
		--[[if self.db.profile.adds then
			self:Message(string.format(L["add_message"], nefCounter), "Positive")
		end]]
		self:TriggerEvent("BigWigs_SetCounterBar", self, L["misc_drakonidsDead"], (module.nefCounterMax - module.nefCounter))
	end
end

function module:MindControl(name)
	if name and self.db.profile.mc then 
		self:Message(string.format(L["msg_mindControlPlayer"], name), "Important")
		local barText = string.format(L["bar_mindControl"], name)
		self:Bar(barText, timer.mc, icon.mc, true, BigWigsColors.db.profile.mindControl)
		self:SetCandyBarOnClick(self:BarId(barText), function() TargetByName(name, true) end)
	end
end

function module:Zerg()
	if self.db.profile.otherwarn then
		self:Message(L["msg_zerg"])
	end
end

----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Zerg()
	module:MindControl("TestPlayer")
	module:NefCounter(1)
	module:Landing()
	module:Fear()
	module:Shadowflame()
	module:PreEngage()

	module:BigWigs_RecvSync(syncName.preEngage)
	module:BigWigs_RecvSync(syncName.shadowflame)
	module:BigWigs_RecvSync(syncName.fear)
	module:BigWigs_RecvSync(syncName.landing)
	module:BigWigs_RecvSync(syncName.addDead, 1)
	module:BigWigs_RecvSync(syncName.mindControl, "TestPlayer")
	module:BigWigs_RecvSync(syncName.zerg)
	
	module.phase2 = nil
	module.nefCounter = 0
end
