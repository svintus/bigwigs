------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.batRider
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "BatRider",
	
	-- commands
	exploding_cmd = "exploding",
	exploding_name = "Exploding alert",
	exploding_desc = "Warn when Bat Rider is exploding.",
	
	-- triggers
	trigger_exploding = "Gurubashi Bat Rider begins to cast Unstable Concoction.",
	
	-- messages
	msg_exploding = "Exploding!",
	
	-- bars
	bar_exploding = "Exploding",
	
} end )
