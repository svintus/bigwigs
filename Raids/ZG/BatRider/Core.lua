------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.batRider
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 1 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"exploding", --[[, "bosskill"]]}
module.trashMod = true


-- locals
module.timer = {
	exploding = 3,
}
local timer = module.timer

module.icon = {
	exploding = "Spell_Fire_Incinerate",
	explodingIcon = "Ability_Rogue_Sprint",
}
local icon = module.icon

module.syncName = {
	exploding = "BatRiderExploding",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.exploding then
		self:Exploding()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Exploding()
	if self.db.profile.exploding then
		self:Message(L["msg_exploding"], "Urgent", false, "Alarm")
		self:Bar(L["bar_exploding"], timer.exploding, icon.exploding, true, BigWigsColors.db.profile.urgent)
		self:WarningSign(icon.explodingIcon, timer.exploding)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions	
	module:Exploding()

	module:BigWigs_RecvSync(syncName.exploding)
end
