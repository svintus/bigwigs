------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.adder
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Adder",
	
	-- commands
	venomSpit_cmd = "venomSpit",
	venomSpit_name = "Venom Spit alert",
	venomSpit_desc = "Warn when Razzashi Adder begins to cast Venom Spit.",
	
	-- triggers
	trigger_venomSpit = "Razzashi Adder begins to perform Venom Spit.",
	
	-- messages
	msg_venomSpit = "Casting Venom Spit!",
	
	-- bars
	bar_venomSpit = "Venom Spit",
	
} end )
