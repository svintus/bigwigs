------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.adder
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 1 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"venomSpit", --[[, "bosskill"]]}
module.trashMod = true


-- locals
module.timer = {
	venomSpit = 2,
}
local timer = module.timer

module.icon = {
	venomSpit = "Spell_Nature_CorrosiveBreath",
}
local icon = module.icon

module.syncName = {
	venomSpit = "AdderVenomSpit",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.venomSpit then
		self:VenomSpit()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:VenomSpit()
	if self.db.profile.venomSpit then
		self:Message(L["msg_venomSpit"], "Important", false, "Alarm")
		self:Bar(L["bar_venomSpit"], timer.venomSpit, icon.venomSpit, true, BigWigsColors.db.profile.interrupt)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions	
	module:VenomSpit()

	module:BigWigs_RecvSync(syncName.venomSpit)
end
