------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.madServant
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "MadServant",
	
	-- commands
	voidwalkers_cmd = "voidwalkers",
	voidwalkers_name = "Voidwalkers spawn",
	voidwalkers_desc = "Show bar for Mad Voidwalkers spawn.",
	
	-- bars
	bar_voidwalkers = "Voidwalkers",

} end )
