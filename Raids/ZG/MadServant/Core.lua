------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.madServant
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]


-- module variables
module.revision = 5 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"voidwalkers", --[[, "bosskill"]]}
module.trashMod = true


-- locals
module.timer = {
	voidwalkers = 11,
}
local timer = module.timer

module.icon = {
	voidwalkers = "Spell_Shadow_SummonVoidWalker",
}
local icon = module.icon

module.syncName = {
}


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == self:GetBossDeathSync() and rest == self.bossSync then
		self:Voidwalkers()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Voidwalkers()
	if self.db.profile.voidwalkers then
		BigWigsBars:Bar(L["bar_voidwalkers"], timer.voidwalkers, icon.voidwalkers)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions	
	module:Voidwalkers()

	module:BigWigs_RecvSync(self:GetBossDeathSync(), self.bossSync)
end
