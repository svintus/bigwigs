------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.hermit
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local CandyBar = AceLibrary("CandyBar-2.1")


-- module variables
module.revision = 16 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"phase", "phaseBWCB", "ktmReset", "whirlwind", "devour", "echoes", "bosskill"}

module.defaultDB = {
	phaseBWCB = false,
}


-- locals
module.timer = {
	phase = 60,
	whirlwind = 10,
	devour = 10,
}
local timer = module.timer

module.icon = {
	phaseTroll = "INV_Misc_Head_Troll_01",
	phaseCrocolisk = "Ability_Hunter_Pet_Crocolisk",
	whirlwind = "Ability_Whirlwind",
	devour = "Spell_Shadow_SummonFelHunter",
	echoes = "Interface\\Icons\\Ability_Warrior_BattleShout",
}
local icon = module.icon

module.syncName = {
	phaseCrocolisk = "HermitPhaseCrocolisk",
	whirlwind = "HermitWhirlwind",
	devour = "HermitDevour",
	devourStop = "HermitDevourStop",
}
local syncName = module.syncName

module.whirlwindCount = 0
module.whirlwindCountMax = 6
module.echoesBar1Name = "HermitEchoes1"
module.echoesBar2Name = "HermitEchoes2"


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.phaseCrocolisk then
		self:PhaseCrocolisk()
	elseif sync == syncName.whirlwind then
		self:Whirlwind()
	elseif sync == syncName.devour then
		self:Devour()
	elseif sync == syncName.devourStop then
		self:DevourStop()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:PhaseTroll()
	self:Message(L["msg_phaseTroll"], "Attention")
	self:Bar(L["bar_phaseCrocolisk"], timer.phase, icon.phaseCrocolisk)
	if self.db.profile.phaseBWCB then
		BWCB(timer.phase, L["bar_phaseCrocolisk"])
	end
end

function module:PhaseCrocolisk()
	if self.db.profile.ktmReset then
		self:KTM_Reset()
	end
	self.whirlwindCount = 0
	if self.db.profile.phase then
		self:Message(L["msg_phaseCrocolisk"], "Attention")
		self:Bar(L["bar_phaseTroll"], timer.phase, icon.phaseTroll)
	end
	if self.db.profile.phaseBWCB then
		BWCB(timer.phase, L["bar_phaseTroll"])
	end
	self:ScheduleEvent(self.PhaseTroll, timer.phase, self)
	self:ScheduleEvent(self.Whirlwind, timer.phase - timer.whirlwind, self)
end

function module:Whirlwind()
	self.whirlwindCount = self.whirlwindCount + 1
	if self.db.profile.whirlwind and self.whirlwindCount <= module.whirlwindCountMax then
		self:Bar(L["bar_whirlwind"], timer.whirlwind, icon.whirlwind)
	end
end

function module:Devour()
	if self.db.profile.devour then
		self:Message(L["msg_devour"], "Attention", false)
		self:Bar(L["bar_devour"], timer.devour, icon.devour)
	end
end

function module:DevourStop()
	self:RemoveBar(L["bar_devour"])
end


------------------------------
--      Utility	Functions   --
------------------------------
function module:EchoesInit()
	if self.db.profile.echoes then
		self:TriggerEvent("BigWigs_StartCounterBar", self, self.echoesBar1Name, 250, icon.echoes, true, "white")
		self.echoesBar1Id = BigWigsBars:GetBarId(self, self.echoesBar1Name)
		self:SetCandyBarText(self.echoesBar1Id, "")
		self:TriggerEvent("BigWigs_SetCounterBar", self, self.echoesBar1Name, 250 - 0.1)
		
		self:TriggerEvent("BigWigs_StartCounterBar", self, self.echoesBar2Name, 250, icon.echoes, true, "white")
		self.echoesBar2Id = BigWigsBars:GetBarId(self, self.echoesBar2Name)
		self:SetCandyBarText(self.echoesBar2Id, "")
		self:TriggerEvent("BigWigs_SetCounterBar", self, self.echoesBar2Name, 250 - 0.1)
		
		self:ScheduleRepeatingEvent(self.UpdateEchoes, 0.1, self)
	end
end

function module:UpdateEchoes()
	local top1stacks = 0.1
	local top2stacks = 0.1
	local top1name = ""
	local top2name = ""
	for i = 1, GetNumRaidMembers() do
		local d = 1
		local unit = "raid" .. i
		local debuffIcon, stacks = UnitDebuff(unit, d)
		while debuffIcon do
			if debuffIcon == icon.echoes then
				if stacks > top1stacks then
					top2name   = top1name
					top2stacks = top1stacks
					top1name   = UnitName(unit)
					top1stacks = stacks
				elseif stacks > top2stacks then
					top2name   = UnitName(unit)
					top2stacks = stacks
				end
			end
			d = d + 1
			debuffIcon, stacks = UnitDebuff("raid"..i, d)
		end
	end
	self:TriggerEvent("BigWigs_SetCounterBar", self, self.echoesBar1Name, (250 - top1stacks))
	self:SetCandyBarText(self.echoesBar1Id, top1name)
	self:TriggerEvent("BigWigs_SetCounterBar", self, self.echoesBar2Name, (250 - top2stacks))
	self:SetCandyBarText(self.echoesBar2Id, top2name)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:PhaseTroll()
	module:PhaseCrocolisk()
	module:Whirlwind()
	module:Devour()
	module:DevourStop()
	module:EchoesInit()
	module:UpdateEchoes()
	
	module:BigWigs_RecvSync(syncName.phaseCrocolisk)
	module:BigWigs_RecvSync(syncName.whirlwind)
	module:BigWigs_RecvSync(syncName.devour)
	module:BigWigs_RecvSync(syncName.devourStop)
end
