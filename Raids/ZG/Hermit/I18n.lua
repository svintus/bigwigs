------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.hermit
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "hermit",
	
	-- commands
	phase_cmd = "phase",
	phase_name = "Phase notification",
	phase_desc = "Announce the boss' phase transitions.",
	
	phaseBWCB_cmd = "phaseBWCB",
	phaseBWCB_name = "Phase Custom Bar",
	phaseBWCB_desc = "Show a Custom Bar for the boss' phase transitions.\n\n(Requires assistant or higher)",
	
	ktmReset_cmd = "ktmReset",
	ktmReset_name = "KTM reset on Crocolisk phase",
	ktmReset_desc = "Reset KTM when Crocolisk phase starts.\n\n(Requires assistant or higher)",
	
	whirlwind_cmd = "whirlwind",
	whirlwind_name = "Whirlwind bar",
	whirlwind_desc = "Show Whirlwind bar.",
	
	devour_cmd = "devour",
	devour_name = "Devour alert",
	devour_desc = "Warn when the boss uses Devour.",
	
	echoes_cmd = "echoes",
	echoes_name = "Echoes tracking",
	echoes_desc = "Show 2 players with highest Echoes stacks.",
	
	-- triggers
	trigger_engage1 = "Intruders! DIE!",
	trigger_engage2 = "Silence!",
	trigger_phaseCrocolisk = "My fangs are swords!",
	trigger_whirlwind = "Nameless Hermit ?'s Whirlwind",
	trigger_devour = "The Nameless Hermit gains Devour.",
	trigger_devourStop = "Devour fades from The Nameless Hermit.",
	
	-- messages
	msg_phaseTroll = "Troll phase",
	msg_phaseCrocolisk = "Crocolisk phase",
	msg_devour = "Devouring!",
	
	-- bars
	bar_whirlwind = "Whirlwind",
	bar_phaseTroll = "Troll phase",
	bar_phaseCrocolisk = "Crocolisk phase",
	bar_devour = "Devour",
	
	-- misc	
	
} end )
