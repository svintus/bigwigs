------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.jindo
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]


-- module variables
module.revision = 20024 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"curse", "hex", "brainwash", "healingward", "puticon", "call", "callicon", "callwhisper", "beam", "beambwcb", "bosskill"}

module.defaultDB = {
    beambwcb = false,
}


-- locals
module.timer = {
	firstHex = 8,
	firstHealing = 12,
	firstBrainwash = 21,
    healing = 18, -- varies from 16.9 to 18.6
    brainwash = 25, -- varies from 22.9 to 26.8
	healingUptime = 240,
	brainwashUptime = 240,
	hex = 5,
	call = 21,
	beam = 3,
	beambwcb = 3,
}
local timer = module.timer

module.icon = {
	hex = "Spell_Nature_Polymorph",
	healing = "Spell_Holy_LayOnHands",
	brainwash = "Spell_Totem_WardOfDraining",
	call = "Spell_Shadow_ShadowWordDominate",
	beam = "Spell_Shadow_SiphonMana",
}
local icon = module.icon

module.syncName = {
	curse = "JindoCurse",
	hex = "JindoHexStart",
	hexOver = "JindoHexStop",
	call = "JindoCall",
	callOver = "JindoCallStop",
	beam = "JindoBeam",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.curse and rest then
		if self.db.profile.curse then
			if rest == UnitName("player") then
				self:Message(L["msg_curseWhisper"], "Attention")
			else
				self:Message(string.format(L["msg_curseOther"], rest), "Urgent")
				self:TriggerEvent("BigWigs_SendTell", rest, L["msg_curseWhisper"])
			end
		end
		if self.db.profile.puticon then 
			self:Icon(rest)
		end
	elseif sync == syncName.hex and rest and self.db.profile.hex then
        self:RemoveBar("Next Hex")
		self:Message(string.format(L["msg_hex"], rest), "Important")
		self:Bar(string.format(L["bar_hex"], rest), timer.hex, icon.hex)
	elseif sync == syncName.hexOver and rest then
		self:HexGone(rest)
	elseif sync == syncName.call and rest then
		self:Call(rest)
	elseif sync == syncName.callOver and rest then
		self:CallGone(rest)
	elseif sync == syncName.beam then
		self:Beam()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:HexGone(name)
	self:RemoveBar(string.format(L["bar_hex"], name))
end

function module:Call(name)
	if self.db.profile.call then
		self:Bar(string.format(L["bar_call"], name), timer.call, icon.call, true, BigWigsColors.db.profile.significant)
		if name == UnitName("player") then
			self:Message(L["msg_callYou"], "Personal", true, "Alert")
			self:Message(string.format(L["msg_callOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.call, timer.call, true)
		else
			self:Message(string.format(L["msg_callOther"], name), "Attention")
		end
	end
	if self.db.profile.callicon then
		self:Icon(name, 7, timer.call)
	end
	if self.db.profile.callwhisper then
		self:Whisper(L["msg_callYou"], name)
	end
end

function module:CallGone(name)
	self:RemoveBar(string.format(L["bar_call"], name))
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.call)
	end
end

function module:Beam()
	if self.db.profile.beam then
		self:Message(L["msg_beam"], "Attention", false, "Alarm")
		self:Bar(L["bar_beam"], timer.beam, icon.beam, true, BigWigsColors.db.profile.interrupt)
	end
	if self.db.profile.beambwcb then
		BWCB(timer.beambwcb, L["bar_beam"])
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions	
	module:BigWigs_RecvSync(syncName.curse, UnitName("player"))
	module:BigWigs_RecvSync(syncName.hex, UnitName("player"))
	module:BigWigs_RecvSync(syncName.hexOver, UnitName("player"))
	module:BigWigs_RecvSync(syncName.call, UnitName("player"))
	module:BigWigs_RecvSync(syncName.callOver, UnitName("player"))
	module:BigWigs_RecvSync(syncName.beam)
end
