local bossName = BigWigs.bossmods.zg.jindo
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 20024 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

module:RegisterYellEngage(L["trigger_engage"])

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF", "FadeFromYou")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY", "FadeFrom")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "FadeFrom")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Event")
	--self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_HOSTILEPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE")
	self:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH")
	self:RegisterEvent("PLAYER_DEAD")
	
	self:ThrottleSync(5, syncName.curse)
	self:ThrottleSync(4, syncName.hex)
	self:ThrottleSync(4, syncName.hexOver)
	self:ThrottleSync(3, syncName.call)
	self:ThrottleSync(0, syncName.callOver)  -- if 2nd target gets call while 1st still has and removes it, 1st fades naturally at the ~same time
end

-- called after module is enabled and after each wipe
function module:OnSetup()	
end

-- called after boss is engaged
function module:OnEngage()
	self:Bar("Next Hex", timer.firstHex, icon.hex)
    --self:Bar("Next Healing Ward", timer.firstHealing, icon.healing)
    --self:Bar("Next Brain Wash", timer.firstBrainwash, icon.brainwash)
	self:RegisterBarForInterrupts(L["bar_beam"])
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:CHAT_MSG_COMBAT_HOSTILE_DEATH(msg)
	if msg == L["trigger_brainWashDeath"] then
		self:RemoveBar(L["bar_brainWash"])
	elseif msg == L["trigger_healingDeath"] then
		self:RemoveBar(L["bar_healing"])
	--[[elseif msg == L["trigger_bossDeath"] then
		if self.db.profile.bosskill then self:Message(string.format(AceLibrary("AceLocale-2.2"):new("BigWigs")["%s has been defeated"], self:ToString()), "Bosskill", nil, "Victory") end
		self:TriggerEvent("BigWigs_RemoveRaidIcon")
		self.core:ToggleModuleActive(self, false)]]
	end
end
 
function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(msg)
    if self.db.profile.brainwash and string.find(msg, L["trigger_brainWash"]) then
		--self:Message(L["msg_brainWash"], "Attention", true, "Alarm")
		--self:Bar(L["bar_brainWash"], timer.brainwashUptime, icon.brainwash, true, "Black")
        --self:Bar(L["bar_brainWashNext"], timer.brainwash, icon.brainwash)
	elseif self.db.profile.healingward and msg == L["trigger_healing"] then
		--self:Message(L["msg_healing"], "Attention", true, "Alarm")
		--self:Bar(L["bar_healing"], timer.healingUptime, icon.healing, true, "Yellow")
        --self:Bar(L["bar_healingNext"], timer.healing, icon.healing)
	end
end

function module:Event(msg)
	local _, _, curseother_name = string.find(msg, L["trigger_curseOther"])
	local _, _, hexother_name= string.find(msg, L["trigger_hexOther"])
	local _, _, callother_name = string.find(msg, L["trigger_callOther"])
	if curseother_name then
		self:Sync(syncName.curse .. " "..curseother_name)
	elseif hexother_name then
		self:Sync(syncName.hex .. " "..hexother_name)
	elseif msg == L["trigger_curseYou"] then
		self:Sync(syncName.curse .. " "..UnitName("player"))
	elseif msg == L["trigger_hexYou"] then
		self:Sync(syncName.hex .. " "..UnitName("player"))
	--[[elseif self.db.profile.brainwash and string.find(msg, L["trigger_brainWash"]) then
		self:Message(L["msg_brainWash"], "Attention", true, "Alarm")
		self:Bar(L["bar_brainWash"], timer.brainwashUptime, icon.brainwash, true, "Black")]]
	elseif callother_name then
		self:Sync(syncName.call .. " " .. callother_name)
	elseif msg == L["trigger_callYou"] then
		self:Sync(syncName.call .. " " .. UnitName("player"))
	end
end

function module:FadeFromYou(msg)
	if msg == L["trigger_hexYouGone"] then
		self:Sync(syncName.hexOver .. " "..UnitName("player"))
	elseif msg == L["trigger_callYouGone"] then
		self:Sync(syncName.callOver .. " " .. UnitName("player"))
	end
end

function module:FadeFrom(msg)
	local _, _,  hexotherend_name = string.find(msg, L["trigger_hexOtherGone"])
	local _, _, callotherend_name = string.find(msg, L["trigger_callOtherGone"])
	if hexotherend_name then
		self:Sync(syncName.hexOver .. " "..hexotherend_name)
	elseif callotherend_name then
		self:Sync(syncName.callOver .. " " .. callotherend_name)
	end
end

function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(msg)
	if msg == L["trigger_beam"] then
		self:Sync(syncName.beam)
	end
end

function module:PLAYER_DEAD()
	self:RemoveWarningSign(icon.call)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:FadeFrom(L["trigger_hexOtherGone"])
	module:FadeFrom(L["trigger_callOtherGone"])
	module:FadeFromYou(L["trigger_hexYouGone"])
	module:FadeFromYou(L["trigger_callYouGone"])
	module:Event(L["trigger_curseOther"])
	module:Event(L["trigger_hexOther"])
	module:Event(L["trigger_callOther"])
	module:Event(L["trigger_curseYou"])
	module:Event(L["trigger_hexYou"])
	module:Event(L["trigger_callYou"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(L["trigger_brainWash"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(L["trigger_healing"])
	module:CHAT_MSG_COMBAT_HOSTILE_DEATH(L["trigger_brainWashDeath"])
	module:CHAT_MSG_COMBAT_HOSTILE_DEATH(L["trigger_healingDeath"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_beam"])
	module:PLAYER_DEAD()
	
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
