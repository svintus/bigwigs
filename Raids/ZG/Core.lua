
--------------------------------------
-- Create all modules for this raid	--
--------------------------------------

BigWigs.bossmods.zg = {}

BigWigs.bossmods.zg.arlokk = "High Priestess Arlokk"
BigWigs.bossmods.zg.gahzranka = "Gahz'ranka"
BigWigs.bossmods.zg.grilek = "Gri'lek"
BigWigs.bossmods.zg.hakkar = "Hakkar"
BigWigs.bossmods.zg.hazzarah = "Hazza'rah"
BigWigs.bossmods.zg.jeklik = "High Priestess Jeklik"
BigWigs.bossmods.zg.jindo = "Jin'do the Hexxer"
BigWigs.bossmods.zg.mandokir = "Bloodlord Mandokir"
BigWigs.bossmods.zg.marli = "High Priestess Mar'li"
BigWigs.bossmods.zg.renataki = "Renataki"
BigWigs.bossmods.zg.thekal = "High Priest Thekal"
BigWigs.bossmods.zg.venoxis = "High Priest Venoxis"
BigWigs.bossmods.zg.wushoolay = "Wushoolay"
BigWigs.bossmods.zg.hermit = "The Nameless Hermit"
BigWigs.bossmods.zg.azus = "Azus the Bloodseeker"
BigWigs.bossmods.zg.gurubashiBerserker = "Gurubashi Berserker"
BigWigs.bossmods.zg.madServant = "Mad Servant"
BigWigs.bossmods.zg.batRider = "Gurubashi Bat Rider"
BigWigs.bossmods.zg.broodwidow = "Razzashi Broodwidow"
BigWigs.bossmods.zg.adder = "Razzashi Adder"

BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.arlokk, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.gahzranka, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.grilek, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.hakkar, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.hazzarah, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.jeklik, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.jindo, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.mandokir, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.marli, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.renataki, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.thekal, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.venoxis, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.wushoolay, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.hermit, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.azus, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.gurubashiBerserker, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.madServant, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.batRider, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.broodwidow, "Zul'Gurub")
BigWigs:ModuleDeclaration(BigWigs.bossmods.zg.adder, "Zul'Gurub")
