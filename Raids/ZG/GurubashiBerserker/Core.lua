------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.gurubashiBerserker
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]


-- module variables
module.revision = 20017 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"fear", "knockback" --[[, "bosskill"]]}


-- locals
module.timer = {
	firstFear = 15,
	fear = 30,
	knockback = 10,
}
local timer = module.timer

module.icon = {
	fear = "Ability_GolemThunderClap",
	knockback = "INV_Gauntlets_05"
}
local icon = module.icon

module.syncName = {
	fear = "BerserkerFear",
	knockback = "BerserkerKnockback"
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick )
	if sync == syncName.fear then
		self:Fear()
	elseif sync == syncName.knockback then
		self:Knockback()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Fear()
	if self.db.profile.fear then
		self:Bar(L["bar_fear"], timer.fear, icon.fear)
	end
end

function module:Knockback()
	if self.db.profile.knockback then
		self:Bar(L["bar_knockback"], timer.knockback, icon.knockback)
	end
end


------------------------------
--      Utility	Functions   --
------------------------------

----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions	
	module:BigWigs_RecvSync(syncName.fear)
	module:BigWigs_RecvSync(syncName.knockback)
end
