local bossName = BigWigs.bossmods.zg.broodwidow
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 4 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "Event")
	self:RegisterEvent("PLAYER_DEAD")
end

-- called after module is enabled and after each wipe
function module:OnSetup()
end

-- called after boss is engaged
function module:OnEngage()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE(msg)
	if msg == L["trigger_intoxicationYou"] then
		self:Sync(syncName.intoxication .. " " .. UnitName("player"))
	end
end

function module:CHAT_MSG_SPELL_AURA_GONE_SELF(msg)
	local playerName = UnitName("player")
	if msg == L["trigger_intoxicationYouStop"] then
		self:Sync(syncName.intoxicationStop .. " " .. UnitName("player"))
	end
end

function module:Event(msg)
	local _,_, intoxicationOther = strfind(msg, L["trigger_intoxicationOther"])
	local _,_, intoxicationOtherStop = strfind(msg, L["trigger_intoxicationOtherStop"])
	
	if intoxicationOther then
		self:Sync(syncName.intoxication .. " " .. intoxicationOther)
	elseif intoxicationOtherStop then
		self:Sync(syncName.intoxicationStop .. " " .. intoxicationOtherStop)
    end
end

function module:PLAYER_DEAD()
	self:RemoveWarningSign(icon.intoxication)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE(L["trigger_intoxicationYou"])
	module:CHAT_MSG_SPELL_AURA_GONE_SELF(L["trigger_intoxicationYouStop"])
	module:Event(L["trigger_intoxicationOther"])
	module:Event(L["trigger_intoxicationOtherStop"])
	module:PLAYER_DEAD()
	
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
