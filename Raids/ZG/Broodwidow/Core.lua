------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.broodwidow
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 4 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"intoxication", "icon", --[[, "bosskill"]]}
module.trashMod = true


-- locals
module.timer = {
	intoxication = 5,
}
local timer = module.timer

module.icon = {
	intoxication = "Ability_Poisons",
}
local icon = module.icon

module.syncName = {
	intoxication = "BroodwidowIntoxication",
	intoxicationStop = "BroodwidowIntoxicationStop",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.intoxication then
		self:Intoxication(rest)
	elseif sync == syncName.intoxicationStop then
		self:IntoxicationStop(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Intoxication(name)
	if self.db.profile.intoxication then
		if name == UnitName("player") then
			self:Message(L["msg_intoxicationYou"], "Personal", true, "Alert")
			self:Message(format(L["msg_intoxicationOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.intoxication, timer.intoxication)
		else
			self:Message(format(L["msg_intoxicationOther"], name), "Attention")
		end
		self:Bar(format(L["bar_intoxication"], name), timer.intoxication, icon.intoxication, true, BigWigsColors.db.profile.significant)
	end
	if self.db.profile.icon then
		self:Icon(name, -1, timer.intoxication)
	end
end

function module:IntoxicationStop(name)
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.intoxication)
	end
	self:RemoveBar(format(L["bar_intoxication"], name))
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	local playerName = UnitName("player")
	
	-- check core functions	
	module:Intoxication(playerName)
	module:Intoxication(playerName.."1")
	module:IntoxicationStop(playerName)
	module:IntoxicationStop(playerName.."1")
	
	module:BigWigs_RecvSync(syncName.intoxication, playerName)
	module:BigWigs_RecvSync(syncName.intoxication, playerName.."1")
	module:BigWigs_RecvSync(syncName.intoxicationStop, playerName)
	module:BigWigs_RecvSync(syncName.intoxicationStop, playerName.."1")
end
