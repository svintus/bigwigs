------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.broodwidow
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Broodwidow",
	
	-- commands
	intoxication_cmd = "intoxication",
	intoxication_name = "Intoxication announcement",
	intoxication_desc = "Warn when players have Intoxication.",
	
	icon_cmd = "icon",
	icon_name = "Put Raid Icon",
	icon_desc = "Put a Raid Icon on the player with Intoxication.\n\n(Requires assistant or higher)",
	
	-- triggers
	trigger_intoxicationYou = "You are afflicted by Intoxication.",
	trigger_intoxicationYouStop = "Intoxication fades from you.",
	trigger_intoxicationOther = "(.+) is afflicted by Intoxication",
	trigger_intoxicationOtherStop = "Intoxication fades from (.*).",
	
	-- messages
	msg_intoxicationYou = "You have Intoxication!",
	msg_intoxicationOther = "%s has Intoxication!",
	
	-- bars
	bar_intoxication = "Intoxication: %s",
	
} end )
