local bossName = BigWigs.bossmods.zg.azus
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 17 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

module:RegisterYellEngage(L["trigger_engage1"])
module:RegisterYellEngage(L["trigger_engage2"])
module:RegisterYellEngage(L["trigger_engage3"])

-- called after module is enabled
function module:OnEnable()
    self:RegisterEvent("CHAT_MSG_MONSTER_EMOTE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "Event")
	self:RegisterEvent("PLAYER_DEAD")
	
	self:ThrottleSync(40, syncName.phaseBerserk)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
end

-- called after boss is engaged
function module:OnEngage()
	self:PhaseTroll()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:CHAT_MSG_MONSTER_EMOTE(msg)
	if msg == L["trigger_phaseBerserk"] then
		self:Sync(syncName.phaseBerserk)
	elseif msg == L["trigger_bloodTide"] then
		self:Sync(syncName.bloodTide)
    end
end

function module:CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE(msg)
	if msg == L["trigger_lacerateYou"] then
		self:Sync(syncName.lacerate .. " " .. UnitName("player"))
	elseif msg == L["trigger_bloodCloud"] then
		self:BloodCloud()
	end
end

function module:CHAT_MSG_SPELL_AURA_GONE_SELF(msg)
	local playerName = UnitName("player")
	if msg == L["trigger_lacerateYouStop"] then
		self:Sync(format("%s_%s %s", syncName.lacerateStop, playerName, playerName))
	elseif msg == L["trigger_bloodCloudStop"] then
		self:BloodCloudStop()
	end
end

function module:Event(msg)
	local _,_, lacerateOther = strfind(msg, L["trigger_lacerateOther"])
	local _,_, lacerateOtherStop = strfind(msg, L["trigger_lacerateOtherStop"])
	
	if lacerateOther then
		self:Sync(syncName.lacerate .. " " .. lacerateOther)
	elseif lacerateOtherStop then
		self:Sync(format("%s_%s %s", syncName.lacerateStop, lacerateOtherStop, lacerateOtherStop))
    end
end

function module:PLAYER_DEAD()
	self:BloodCloudStop()
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_MONSTER_EMOTE(L["trigger_phaseBerserk"])
	module:CHAT_MSG_MONSTER_EMOTE(L["trigger_bloodTide"])
	module:CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE(L["trigger_lacerateYou"])
	module:CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE(L["trigger_bloodCloud"])
	module:CHAT_MSG_SPELL_AURA_GONE_SELF(L["trigger_lacerateYouStop"])
	module:CHAT_MSG_SPELL_AURA_GONE_SELF(L["trigger_bloodCloudStop"])
	module:Event(L["trigger_lacerateOther"])
	module:Event(L["trigger_lacerateOtherStop"])
	module:PLAYER_DEAD()
	
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
