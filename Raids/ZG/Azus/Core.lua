------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.zg.azus
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 17 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["add_name"] } -- adds which will be considered in CheckForEngage
module.toggleoptions = {"phase", "phaseBWCB", "bloodTide", "bloodTideBWCB", "lacerateYou", "lacerateOthers", "lacerateBars", "bloodCloud", "bosskill"}

module.defaultDB = {
	phaseBWCB = false,
	bloodTideBWCB = false,
}


-- locals
module.timer = {
	phase = 45,
	bloodTide = 5,
	lacerate = 25,
	bloodCloud = 120,
}
local timer = module.timer

module.icon = {
	phaseTroll = "INV_Misc_Head_Troll_01",
	phaseBerserk = "Spell_Shadow_UnholyFrenzy",
	bloodTide = "Spell_Shadow_BloodBoil",
	bloodTideWarn = "Ability_Rogue_Sprint",
	lacerate = "Spell_Shadow_LifeDrain",
	bloodCloud = "INV_Misc_Gem_Pearl_04",
}
local icon = module.icon

module.syncName = {
	phaseBerserk = "AzusPhaseBerserk",
	bloodTide = "AzusBloodTide",
	lacerate = "AzusLacerate",
	lacerateStop = "AzusLacerateStop",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.phaseBerserk then
		self:PhaseBerserk()
	elseif sync == syncName.bloodTide then
		self:BloodTide()
	elseif sync == syncName.lacerate then
		self:Lacerate(rest)
	elseif strfind(sync, syncName.lacerateStop) then
		self:LacerateStop(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:PhaseBerserk()
	if self.db.profile.phase then
		self:RemoveBar(L["bar_phaseBerserk"])
		self:Message(L["msg_phaseBerserk"], "Attention")
		self:Bar(L["bar_phaseTroll"], timer.phase, icon.phaseTroll)
		self:ScheduleEvent(self.PhaseTroll, timer.phase, self)
	end
	if self.db.profile.phaseBWCB then
		BWCB(timer.phase, L["bar_phaseTroll"])
	end
end

function module:PhaseTroll()
	if self.db.profile.phase then
		self:Message(L["msg_phaseTroll"], "Attention")
		self:Bar(L["bar_phaseBerserk"], timer.phase, icon.phaseBerserk)
	end
	if self.db.profile.phaseBWCB then
		BWCB(timer.phase, L["bar_phaseBerserk"])
	end
end

function module:BloodTide()
	if self.db.profile.bloodTide then
		self:Message(L["msg_bloodTide"], "Important", false, "Alarm")
		self:Bar(L["bar_bloodTide"], timer.bloodTide, icon.bloodTide)
		self:WarningSign(icon.bloodTideWarn, 1)
	end
	if self.db.profile.bloodTideBWCB then
		BWCB(timer.bloodTide, L["bar_bloodTide"])
	end
end

function module:Lacerate(name)
	if self.db.profile.lacerateYou and name == UnitName("player") then
		self:Message(L["msg_lacerateYou"], "Personal", true, "Alarm")
		self:Message(format(L["msg_lacerateOther"], name), "Attention", false, false, true)
		self:WarningSign(icon.lacerate, timer.lacerate)
	elseif self.db.profile.lacerateOthers then
		self:Message(format(L["msg_lacerateOther"], name), "Attention")
	end
	if self.db.profile.lacerateBars then
		self:Bar(format(L["bar_lacerate"], name), timer.lacerate, icon.lacerate, true, "magenta")
	end
end

function module:LacerateStop(name)
	if name == UnitName("player") then
		self:RemoveWarningSign(icon.lacerate)
	end
	self:RemoveBar(format(L["bar_lacerate"], name))
end

function module:BloodCloud()
	if self.db.profile.bloodCloud then
		self:WarningSign(icon.bloodCloud, timer.bloodCloud)
		self:Message(L["msg_bloodCloud"], "Attention", true)
	end
end

function module:BloodCloudStop()
	self:RemoveWarningSign(icon.bloodCloud)
	if self.db.profile.bloodCloud and not UnitIsDead("player") then
		self:Message(L["msg_bloodCloudStop"], "Positive", true)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	local playerName = UnitName("player")
	
	-- check core functions
	module:PhaseBerserk()
	module:PhaseTroll()
	module:BloodTide()
	module:Lacerate(playerName)
	module:Lacerate(playerName.."1")
	module:LacerateStop(playerName)
	module:LacerateStop(playerName.."1")
	module:BloodCloud()
	module:BloodCloudStop()
	
	module:BigWigs_RecvSync(syncName.phaseBerserk)
	module:BigWigs_RecvSync(syncName.bloodTide)
	module:BigWigs_RecvSync(syncName.lacerate, playerName)
	module:BigWigs_RecvSync(syncName.lacerate, playerName.."1")
	module:BigWigs_RecvSync(syncName.lacerateStop, playerName)
	module:BigWigs_RecvSync(syncName.lacerateStop, playerName.."1")
end
