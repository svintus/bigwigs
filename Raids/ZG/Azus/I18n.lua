------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.zg.azus
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "azus",
	
	-- commands
	phase_cmd = "phase",
	phase_name = "Phase Notification",
	phase_desc = "Announce the boss' phase transitions.",
	
	phaseBWCB_cmd = "phaseBWCB",
	phaseBWCB_name = "Phase Custom Bar",
	phaseBWCB_desc = "Show a Custom Bar for the boss' phase transitions.\n\n(Requires assistant or higher)",
	
	bloodTide_cmd = "bloodTide",
	bloodTide_name = "Blood Tide alert",
	bloodTide_desc = "Announce when the boss begins to cast Blood Tide.",
	
	bloodTideBWCB_cmd = "bloodTideBWCB",
	bloodTideBWCB_name = "Blood Tide Custom Bar",
	bloodTideBWCB_desc = "Show a Custom Bar when the boss begins to cast Blood Tide.\n\n(Requires assistant or higher)",
	
	lacerateYou_cmd = "lacerateYou",
	lacerateYou_name = "Lacerate on you alert",
	lacerateYou_desc = "Warn when you have Lacerate.",
	
	lacerateOthers_cmd = "lacerateOthers",
	lacerateOthers_name = "Lacerate on others announcement",
	lacerateOthers_desc = "Announce when others have Lacerate.",
	
	lacerateBars_cmd = "lacerateBars",
	lacerateBars_name = "Lacerate bars",
	lacerateBars_desc = "Show bars for Lacerate on players.",
	
	bloodCloud_cmd = "bloodCloud",
	bloodCloud_name = "Blood Cloud alert",
	bloodCloud_desc = "Warn when you are standing in a Blood Cloud.",
	
	-- triggers
	trigger_engage1 = "Voluntary sacrifice%?!",
	trigger_engage2 = "The Blood is Life!",
	trigger_engage3 = "Life is in the Blood!",
	
	trigger_phaseBerserk = "Azus the Bloodseeker goes into a Berserking rage",
	trigger_bloodTide = "Azus the Bloodseeker starts a bloody ritual.",
	
	trigger_lacerateYou = "You are afflicted by Lacerate.",
	trigger_lacerateYouStop = "Lacerate fades from you.",
	trigger_lacerateOther = "(.+) is afflicted by Lacerate",
	trigger_lacerateOtherStop = "Lacerate fades from (.+).",
	
	trigger_bloodCloud = "You are afflicted by Blood Cloud.",
	trigger_bloodCloudStop = "Blood Cloud fades from you.",
	
	-- messages
	msg_phaseTroll = "Troll phase",
	msg_phaseBerserk = "Berserk phase",
	msg_bloodTide = "Blood Tide in 5 sec!",
	msg_lacerateYou = "You have Lacerate!",
	msg_lacerateOther = "%s has Lacerate",
	msg_bloodCloud = "You are in a Blood Cloud",
	msg_bloodCloudStop = "Blood Cloud gone",
	
	-- bars
	bar_phaseTroll = "Troll phase",
	bar_phaseBerserk = "Berserk phase",
	bar_bloodTide = "Blood Tide",
	bar_lacerate = "Lacerate: %s",
	
	-- misc	
	
} end )
