<b><a href="../..#raid-adjustments"> Return to the Overview </a></b>

# Outdoor Raid Bosses

## Lady Hederine
- <b>(QA)</b> Bloating Toxins alert
- <b>(QA)</b> Addcounter

<br>

##### Prefix legend
- <b>(100%)</b>  = it's working flawless
- <b>(99%)</b>   = it's working as good as it can be (from my research)
- <b>(QA)</b>    = <b>Q</b>uality <b>A</b>ssurance (need to test its modified state)
- <b>(DG)</b>    = <b>D</b>ata <b>G</b>athering (need to gather more data regarding this matter)
