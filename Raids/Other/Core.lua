
--------------------------------------
-- Create all modules for this raid	--
--------------------------------------

BigWigs.bossmods.other = {}


BigWigs.bossmods.other.azuregos = "Azuregos"
BigWigs.bossmods.other.kazzak = "Lord Kazzak"
BigWigs.bossmods.other.emeriss = "Emeriss"
BigWigs.bossmods.other.lethon = "Lethon"
BigWigs.bossmods.other.taerar = "Taerar"
BigWigs.bossmods.other.ysondre = "Ysondre"
BigWigs.bossmods.other.hederine = "Lady Hederine"
BigWigs.bossmods.other.kurinnaxx = "Kurinnaxx (Outdoor)"
BigWigs.bossmods.other.boar = "Elder Mottled Boar"

BigWigs:ModuleDeclaration(BigWigs.bossmods.other.azuregos, "Azshara")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.kazzak, "Blasted Lands")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.emeriss, "Ashenvale")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.lethon, "Duskwood")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.taerar, "The Hinterlands")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.ysondre, "Feralas")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.hederine, "Winterspring")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.kurinnaxx, "Silithus")
BigWigs:ModuleDeclaration(BigWigs.bossmods.other.boar, "Durotar")