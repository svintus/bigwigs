local bossName = BigWigs.bossmods.other.hederine
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(bossName)
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 26 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

module:RegisterYellEngage(L["trigger_engage1"])
module:RegisterYellEngage(L["trigger_engage2"])

-- called after module is enabled
function module:OnEnable()    
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE",          "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE",           "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF",                 "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY",                "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER",                "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_HOSTILEPLAYER_DAMAGE",  "Event")

	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE")
	
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS")
	
	self:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH")
	self:RegisterEvent("PLAYER_DEAD")

	self:ThrottleSync(3, syncName.knockback)
	self:ThrottleSync(2, syncName.mc)
	self:ThrottleSync(1, syncName.mcStop)
	self:ThrottleSync(4, syncName.debuffs)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.deadBodyguards = 0
	self.firstDebuffs = true
	self.skullName = nil
end

-- called after boss is engaged
function module:OnEngage()
	self:Debuffs()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:TearsGone()
	self:RemoveBar(string.format(L["bar_tears"], UnitName("player")))
	self:RemoveWarningSign(icon.tears)
end

function module:Event(msg)
	local _, _, bombOther     = string.find(msg, L["trigger_bombOther"])
	local _, _, bombOtherGone = string.find(msg, L["trigger_bombOtherGone"])
	local _, _, mcOther       = string.find(msg, L["trigger_mcOther"])
	local _, _, mcOtherGone   = string.find(msg, L["trigger_mcOtherGone"])

	-- Bomb
	-- can be 2 bombs at once, using different sync for each player to prevent throttle
	-- otherwise would have to remove ThrottleSync (set to 0) for bomb event
	if msg == L["trigger_bombYou"] then
		self:Sync(syncName.bomb     .. UnitName("player") .. " " .. UnitName("player"))
	elseif msg == L["trigger_bombYouGone"] then
		self:Sync(syncName.bombStop .. UnitName("player") .. " " .. UnitName("player"))
	elseif bombOther then
		self:Sync(syncName.bomb     .. bombOther     .. " " .. bombOther)
	elseif bombOtherGone then
		self:Sync(syncName.bombStop .. bombOtherGone .. " " .. bombOtherGone)

	-- Tears
	elseif msg == L["trigger_tears"] and self.db.profile.tears then
		self:Bar(string.format(L["bar_tears"], UnitName("player")), timer.tears, icon.tears, true, BigWigsColors.db.profile.urgent)
		self:Message(L["msg_tears"], "Personal", true, "Alarm")
		self:WarningSign(icon.tears, 4)
	elseif msg == L["trigger_tearsGone"] and self.db.profile.tears then
		self:TearsGone()

	-- MC
	elseif msg == L["trigger_mcYou"] then
		self:Sync(syncName.mc     .. " " .. UnitName("player"))
	elseif msg == L["trigger_mcYouGone"] then
		self:Sync(syncName.mcStop .. " " .. UnitName("player"))
	elseif mcOther then
		self:Sync(syncName.mc     .. " " .. mcOther)
	elseif mcOtherGone then
		self:Sync(syncName.mcStop .. " " .. mcOtherGone)

	-- Magic and Curse debuffs
	elseif string.find(msg, L["trigger_debuff1Hit"]) or string.find(msg, L["trigger_debuff1Resist"]) or
           string.find(msg, L["trigger_debuff2Hit"]) or string.find(msg, L["trigger_debuff2Resist"]) then
		self:Sync(syncName.debuffs)
	end
end

function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(msg)
	if msg == L["trigger_knockback"] then
		self:Sync(syncName.knockback)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(msg)
	if msg == L["trigger_antiKite"] then
		self:Sync(syncName.antiKite)
	end
end

function module:CHAT_MSG_COMBAT_HOSTILE_DEATH(msg)
	if msg == L["trigger_deathAdd"] then
		self:Sync(syncName.add .. " " .. tostring(self.deadBodyguards + 1))
	end
end

function module:PLAYER_DEAD()
	self:RemoveWarningSign(icon.tears)
	self:RemoveWarningSign(icon.bombSign)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:Event(L["trigger_bombOther"])
	module:Event(L["trigger_bombOtherGone"])
	module:Event(L["trigger_bombYou"])
	module:Event(L["trigger_bombYouGone"])
	module:Event(L["trigger_tears"])
	module:Event(L["trigger_tearsGone"])
	module:Event(L["trigger_mcOther"])
	module:Event(L["trigger_mcOtherGone"])
	module:Event(L["trigger_mcYou"])
	module:Event(L["trigger_mcYouGone"])
	module:Event(L["trigger_debuff1Hit"])
	module:Event(L["trigger_debuff1Resist"])
	module:Event(L["trigger_debuff2Hit"])
	module:Event(L["trigger_debuff2Resist"])

	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_knockback"])
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_BUFFS(L["trigger_antiKite"])
	module:CHAT_MSG_COMBAT_HOSTILE_DEATH(L["trigger_deathAdd"])
	module:PLAYER_DEAD()

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
