------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.other.hederine
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Hederine",
	
	-- commands
	bomb_cmd = "bomb",
	bomb_name = "Bloating Toxins alert",
	bomb_desc = "Warn when players are the bomb.",

	icons_cmd = "icons",
	icons_name = "Raid Icons on bombs",
	icons_desc = "Put Skull and Cross on the persons who are the bombs. (Requires assistant or higher)",

	announce_cmd = "whispers",
	announce_name = "Whisper to bomb targets",
	announce_desc = "Send a whisper to players targetted by Bloating Toxins. (Requires assistant or higher)",
	
	poisonresist_cmd = "poisonresist",
	poisonresist_name = "|cffffffff|Hitem:3386:0:0:0|h[Elixir of Poison Resistance]|h|r announcement",
	poisonresist_desc = "Announce the raid to use |cffffffff|Hitem:3386:0:0:0|h[Elixir of Poison Resistance]|h|r when one of tanks is targetted by Bloating Toxins.\n\n(not used)",
	
	tears_cmd = "tears",
	tears_name = "Tears of the Hederine alert",
	tears_desc = "Warn if you are standing in Tears of the Hederine.",
	
	knockback_cmd = "knockback",
	knockback_name = "Knockback alert",
	knockback_desc = "Warn for Sonic Lash knockback.",

	mc_cmd = "mc",
	mc_name = "Mind control alert",
	mc_desc = "Alert when someone is afflicted by Subservience.",

	debuffs_cmd = "debuffs",
	debuffs_name = "Magic and Curse debuffs bar",
	debuffs_desc = "Show timers for Impotence and Curse of Weakness.",
	
	antiKite_cmd = "antiKite",
	antiKite_name = "Anti-kite mechanic announcement",
	antiKite_desc = "Announce when anti-kite mechanics is triggered.",

	adds_cmd = "adds",
	adds_name = "Dead adds counter",
	adds_desc = "Announce dead Hederine Bodyguards.",
	
	-- triggers
	trigger_engage1 = "Finally! New toys!",
	trigger_engage2 = "Submit or die!",
	
	trigger_bombYou = "You are afflicted by Bloating Toxins.",
	trigger_bombOther = "(.*) is afflicted by Bloating Toxins.",
	trigger_bombYouGone = "Bloating Toxins fades from you.",
	trigger_bombOtherGone = "Bloating Toxins fades from (.*).",
	
	trigger_deathAdd = "Hederine Bodyguard dies.",

	trigger_tears = "You are afflicted by Tears of the Hederine.",
	trigger_tearsGone = "Tears of the Hederine fades from you.",

	trigger_knockback = "Lady Hederine begins to cast Sonic Lash.",

	trigger_mcYou = "You are afflicted by Subservience.",
	trigger_mcOther = "(.*) is afflicted by Subservience.",
	trigger_mcYouGone = "Subservience fades from you.",
	trigger_mcOtherGone = "Subservience fades from (.*).",

	trigger_debuff1Hit    = "afflicted by Curse of Weakness",
	trigger_debuff1Resist = "Curse of Weakness was resisted",
	trigger_debuff2Hit    = "afflicted by Impotence",
	trigger_debuff2Resist = "Impotence was resisted",
	
	trigger_antiKite = "Hederine Bodyguard gains Speed.",

	-- messages
	msg_bombWhisper = "You are the bomb!",
	msg_bombYou = "You are the bomb!",
	msg_bombOther = "%s is the bomb!",
	msg_poisonResist = "Use |cffffffff|Hitem:3386:0:0:0|h[Elixir of Poison Resistance]|h|r!",
	
	msg_add = "%d/2 Hederine Bodyguards dead!",
	
	msg_knockback = "Knockback in 1 sec!",
	
	msg_tears = "You are in the Tears of the Hederine!",

	msg_mcYou = "You are mind controlled!",
	msg_mcOther = "%s is mind controlled!",
	
	msg_antiKite = "Anti-kite mechanics is triggered!",

	-- bars
	bar_bomb = "Bloating Toxins: %s",
	bar_knockback = "Knockback",
	bar_tears = "Tears of the Hederine",
	bar_mc = "MC: %s",
	bar_debuffs = "Impotence & Curse of Weakness",
	
	-- misc
	misc_addName = "Hederine Bodyguard",
	
} end)
