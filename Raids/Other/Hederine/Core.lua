------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.other.hederine
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 26 -- To be overridden by the module!
module.enabletrigger = { module.translatedName, L["misc_addName"] } -- string or table {boss, add1, add2}
module.wipemobs = { L["misc_addName"] }
module.toggleoptions = {"bomb", "icons", "announce", --[["poisonresist",]] "tears", "knockback", "mc", "debuffs", "adds", "antiKite", "bosskill"}

module.defaultDB = {
	poisonresist = false,
}


-- locals
module.timer = {
	bomb = 6,
	knockback = 1,
	tears = 4,
	firstDebuffs = 10,
	mc = 20,
	debuffs = 30,
	poisonResistDelay = 2.5,
}
local timer = module.timer

module.icon = {
	bomb = "Ability_Creature_Disease_02",
	bombSign = "Spell_Shadow_MindBomb",
	knockback = "Spell_Shadow_Curse",
	tears = "Spell_Nature_AbolishMagic",
	mc = "Spell_Shadow_SummonSuccubus",
	debuffs = "Spell_Shadow_CurseOfMannoroth",
}
local icon = module.icon

module.syncName = {
	bomb = "HederineBombStart",
	bombStop = "HederineBombStop",
	add = "HederineAddDead",
	knockback = "HededineKnockback",
	mc = "HederineMC",
	mcStop = "HederineMCStop",
	debuffs = "HederineDebuffs",
	antiKite = "HederineAntiKite",
}
local syncName = module.syncName


module.deadBodyguards = 0
module.firstDebuffs = true
module.tanks = {}


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if strfind(sync, syncName.bomb) and rest then
		self:ThrottleSync(3, sync)
		self:Bomb(rest)
	elseif strfind(sync, syncName.bombStop) and rest then
		self:ThrottleSync(2, sync)
		self:BombGone(rest)
	elseif sync == syncName.add and rest and rest ~= "" then
		rest = tonumber(rest)
		self:AddDeath(rest)
	elseif sync == syncName.knockback then
		self:Knockback()
	elseif sync == syncName.mc and rest then
		self:MindControl(rest)
	elseif sync == syncName.mcStop and rest then
		self:MindControlStop(rest)
	elseif sync == syncName.debuffs then
		self:Debuffs()
	elseif sync == syncName.antiKite then
		self:AntiKite()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:CheckBossTarget(playerName)
	for i = 1, GetNumRaidMembers() do
		local unitTarget = "raid"..i.."target"
		local targetName = UnitName(unitTarget)
		if UnitExists(unitTarget) and (targetName == bossName or targetName == L["misc_addName"]) and UnitName(unitTarget.."target") == playerName then
			return true
		end
	end
end

function module:Bomb(name)
	if self.db.profile.bomb then
		self:Bar(string.format(L["bar_bomb"], name), timer.bomb, icon.bomb, true, BigWigsColors.db.profile.significant)
		if name == UnitName("player") then
			self:Message(L["msg_bombYou"], "Personal", true, "Alert")
			self:Message(string.format(L["msg_bombOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.bombSign, timer.bomb, true)
		else
			self:Message(string.format(L["msg_bombOther"], name), "Attention")
		end
	end

	if self.db.profile.icons then
		if not self.skullName or self.skullName == name then
			self:Icon(name, -1, timer.bomb)
			self:ScheduleEvent("BWHederineSkullStop", function() self.skullName = nil end, timer.bomb)
			self.skullName = name
		elseif self.skullName ~= name then
			self:Icon(name, 7, timer.bomb)
		end
	end

	if self.db.profile.announce then
		self:TriggerEvent("BigWigs_SendTell", name, L["msg_bombWhisper"])
	end

	if self.db.profile.poisonresist and self.tanks[name] --[[and self:CheckBossTarget(name)]] then
		self:ScheduleEvent("bwhederinepoisonresist"..name, function() self:Message(L["msg_poisonResist"], "Important") end, timer.poisonResistDelay)
	end
end

function module:BombGone(name)
	if self.db.profile.bomb then
		self:RemoveBar(string.format(L["bar_bomb"], name))
		if name == UnitName("player") then
			self:RemoveWarningSign(icon.bombSign)
		end
		if self.skullName == name then
			self.skullName = nil
		end
	end
	if self.db.profile.poisonresist and self.tanks[name] then
		self:CancelScheduledEvent("bwhederinepoisonresist"..name)
	end
end

function module:AddDeath(number)
	if number and number <= 2 and self.deadBodyguards < number then
		self.deadBodyguards = number
		if self.db.profile.adds then
			self:Message(string.format(L["msg_add"], self.deadBodyguards), "Positive")
		end
	end
end

function module:Knockback()
	if self.db.profile.knockback then
		self:Message(L["msg_knockback"], "Attention")
		self:Bar(L["bar_knockback"], timer.knockback, icon.knockback)
	end
end

function module:MindControl(name)
	if self.db.profile.mc and name then
		if name == UnitName("player") then
			self:Message(L["msg_mcYou"], "Attention", true)
			self:Message(string.format(L["msg_mcOther"], name), "Attention", false, nil, true)
		else
			self:Message(string.format(L["msg_mcOther"], name), "Attention")
		end
		self:Bar(string.format(L["bar_mc"], name), timer.mc, icon.mc, true, BigWigsColors.db.profile.mindControl)
	end
end

function module:MindControlStop(name)
	if self.db.profile.mc and name then
		self:RemoveBar(string.format(L["bar_mc"], name))
	end
end

function module:Debuffs()
	if self.db.profile.debuffs then
		if self.firstDebuffs then
			self:Bar(L["bar_debuffs"], timer.firstDebuffs, icon.debuffs, true, "magenta")
		else
			self:Bar(L["bar_debuffs"], timer.debuffs,      icon.debuffs, true, "magenta")
		end
	end
	self.firstDebuffs = false
end

function module:AntiKite()
	if self.db.profile.antiKite then		
		self:Message(L["msg_antiKite"], "Attention")
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Bomb(UnitName("player"))
	module:BombGone(UnitName("player"))	
	module:AddDeath(1)
	module:Knockback()
	module:MindControl(UnitName("player"))
	module:MindControlStop(UnitName("player"))
	module:Debuffs()
	module:AntiKite()

	module:BigWigs_RecvSync(syncName.bomb, UnitName("player"))
	module:BigWigs_RecvSync(syncName.bombStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.add, 1)
	module:BigWigs_RecvSync(syncName.knockback)
	module:BigWigs_RecvSync(syncName.mc, UnitName("player"))
	module:BigWigs_RecvSync(syncName.mcStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.debuffs)
	module:BigWigs_RecvSync(syncName.antiKite)
end
