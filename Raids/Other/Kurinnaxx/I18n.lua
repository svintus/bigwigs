------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.other.kurinnaxx
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Kurinnaxx",
	
	-- commands
	threatDrop_cmd = "threatDrop",
	threatDrop_name = "Threat drop bar",
	threatDrop_desc = "Show bar for threat drop when the bosss uses Sand Reaver's Rush.",
	
	enrage_cmd = "enrage",
	enrage_name = "Enrage announcement",
	enrage_desc = "Announce when the boss is enraged.",
	
	-- triggers
	trigger_threatDrop = "Kurinnaxx ?'s Sand Reaver's Rush",
	trigger_enrage = "Kurinnaxx gains Enrage.",
	
	-- messages
	msg_enrage = "Enrage!",
	
	-- bars
	bar_threatDrop = "Threat drop",
	
	-- misc
	
} end )