------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.other.kurinnaxx
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 2 -- To be overridden by the module!
module.enabletrigger = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")["Kurinnaxx"] -- string or table {boss, add1, add2}
module.toggleoptions = {"threatDrop", "enrage", "bosskill"}


-- locals
module.timer = {
	threatDrop = 25,
}
local timer = module.timer

module.icon = {
	threatDrop = "Ability_Warrior_Charge",
}
local icon = module.icon

module.syncName = {
	threatDrop = "KurinnaxxOutdoorThreatDrop",
	enrage = "KurinnaxxOutdoorEnrage",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.threatDrop then
		self:ThreatDrop()
	elseif sync == syncName.enrage then
		self:Enrage()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------

function module:ThreatDrop()
	if self.db.profile.threatDrop then
		self:Bar(L["bar_threatDrop"], timer.threatDrop, icon.threatDrop)
	end
end

function module:Enrage()
	if self.db.profile.enrage then
		self:Message(L["msg_enrage"], "Attention", false, "Beware")
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:ThreatDrop()
	module:Enrage()
	
	module:BigWigs_RecvSync(syncName.threatDrop)
	module:BigWigs_RecvSync(syncName.enrage)
end
