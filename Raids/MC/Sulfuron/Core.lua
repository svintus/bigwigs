------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.sulfuron
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

module.revision = 20024 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.wipemobs = { L["misc_addName"] }
module.toggleoptions = {"heal", "adds", "spear", "warStomp", "inspire", "shieldWall", "retaliation", "retaliationSign", "recklessness", "bosskill"}


-- locals
module.timer = {
    heal = 2,
    flame_spear = 8,
    warStomp = 20,
    inspire = 10,
    shieldWall = 20,
    retaliation = 15,
    recklessness = 15,
}
local timer = module.timer

module.icon = {
    heal = "Spell_Shadow_ChillTouch",
    flame_spear = "Spell_Fire_FlameBlades",
    warStomp = "Ability_BullRush",
    inspire = "Ability_Warrior_BattleShout",
    shieldWall = "Ability_Warrior_ShieldWall",
    retaliation = "Ability_Warrior_Challange",
    recklessness = "Ability_CriticalStrike",
    tranquil = "Spell_Nature_Drowsy",
}
local icon = module.icon

module.syncName = {
    heal = "SulfuronAddHeal",
    flame_spear = "SulfuronSpear",
    add_dead = "SulfuronAddDead",
    warStomp = "SulfuronWarStomp",
    inspire = "SulfuronInspire",
    shieldWall = "SulfuronShieldWall",
    retaliation = "SulfuronRetaliation",
    recklessness = "SulfuronRecklessness",
    recklessnessStop = "SulfuronRecklessnessStop",
}
local syncName = module.syncName

local _, playerClass = UnitClass("player")

module.deadpriests = 0


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)    
    if sync == syncName.add_dead and rest and rest ~= "" then
        rest = tonumber(rest)
        self:AddDeath(rest)
	elseif sync == syncName.heal then
		self:Heal()
    elseif sync == syncName.flame_spear then
        self:FlameSpear()
    elseif sync == syncName.warStomp then
        self:WarStomp()
    elseif sync == syncName.inspire then
        self:Inspire()
    elseif sync == syncName.shieldWall then
        self:ShieldWall()
    elseif sync == syncName.retaliation then
        self:Retaliation()
    elseif sync == syncName.recklessness then
        self:Recklessness()
    elseif sync == syncName.recklessnessStop then
        self:RecklessnessStop()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:AddDeath(number)
	if number and number <= 4 and module.deadpriests < number then
		module.deadpriests = number
		if self.db.profile.adds then
			self:Message(string.format(L["msg_adds"], module.deadpriests), "Positive")
			--self:TriggerEvent("BigWigs_SetCounterBar", self, "Priests dead", (4 - deadpriests))
		end
	end
end

function module:Heal()
	if self.db.profile.heal then		
		self:Message(L["msg_heal"], "Attention", true, "Alarm")
		self:Bar(L["bar_heal"], timer.heal, icon.heal, true, BigWigsColors.db.profile.interrupt)
	end
end

function module:FlameSpear()
	if self.db.profile.spear then
		self:Bar(L["bar_flameSpear"], timer.flame_spear, icon.flame_spear)
	end
end

function module:WarStomp()
	if self.db.profile.warStomp then
		self:Message(L["msg_warStomp"], "Attention")
		self:Bar(L["bar_warStomp"], timer.warStomp, icon.warStomp)
	end
end

function module:Inspire()
	if self.db.profile.inspire then
		self:Message(L["msg_inspire"], "Attention")
		self:Bar(L["bar_inspire"], timer.inspire, icon.inspire)
	end
end

function module:ShieldWall()
	if self.db.profile.shieldWall then
		self:Message(L["msg_shieldWall"], "Attention")
		self:Bar(L["bar_shieldWall"], timer.shieldWall, icon.shieldWall)
	end
end

function module:Retaliation()
	if self.db.profile.retaliation then
		self:Message(L["msg_retaliation"], "Important", false, "Alarm")
		self:Bar(L["bar_retaliation"], timer.retaliation, icon.retaliation)
	end
	if self.db.profile.retaliationSign then
		self:WarningSign(icon.retaliation, timer.retaliation)
	end
end

function module:Recklessness()
	if self.db.profile.recklessness then
		self:Message(L["msg_recklessness"], "Important", true, "Alert")
		self:Bar(L["bar_recklessness"], timer.recklessness, icon.recklessness, true, BigWigsColors.db.profile.frenzy)
		if playerClass == "HUNTER" then
			self:WarningSign(icon.tranquil, timer.recklessness, true)
		end
	end
end

function module:RecklessnessStop()
	self:RemoveBar(L["bar_recklessness"])
    self:RemoveWarningSign(icon.tranquil)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:AddDeath(1)
	module:Heal()
	module:FlameSpear()
	module:WarStomp()
	module:Inspire()
	module:ShieldWall()
	module:Retaliation()
	module:Recklessness()
	module:RecklessnessStop()
	
	module:BigWigs_RecvSync(syncName.add_dead, 1)
	module:BigWigs_RecvSync(syncName.heal)
	module:BigWigs_RecvSync(syncName.flame_spear)
	module:BigWigs_RecvSync(syncName.warStomp)
	module:BigWigs_RecvSync(syncName.inspire)
	module:BigWigs_RecvSync(syncName.shieldWall)
	module:BigWigs_RecvSync(syncName.retaliation)
end
