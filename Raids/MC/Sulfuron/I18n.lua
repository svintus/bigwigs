------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.mc.sulfuron
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Sulfuron",
	
	-- commands
	heal_cmd = "heal",
	heal_name = "Adds' heals",
	heal_desc = "Announces Flamewaker Priests' heals",
	
	adds_cmd = "adds",
	adds_name = "Dead adds counter",
	adds_desc = "Announces dead Flamewaker Priests",
	
	spear_cmd = "spear",
	spear_name = "Flame Spear bar",
	spear_desc = "Show bar for Flame Spear.",
	
	warStomp_cmd = "warStomp",
	warStomp_name = "War Stomp bar",
	warStomp_desc = "Show bar for War Stomp.\n\nNotes:\n1. The bar starts only if at least 1 player gets hit by War Stomp.\n2. The bar does not appear or disappear instantly when the boss switches to Defensive or Battle/Berserker stance because combat messages \"Sulfuron Harbinger gains Battle/Defensive/Berserker Stance.\" do not happen.",
	
	inspire_cmd = "inspire",
	inspire_name = "Inspire alert",
	inspire_desc = "Announcement and bar for Inspire.",
	
	shieldWall_cmd = "shieldWall",
	shieldWall_name = "Shield Wall alert",
	shieldWall_desc = "Announcement and bar for Shield Wall.",
	
	retaliation_cmd = "retaliation",
	retaliation_name = "Retaliation alert",
	retaliation_desc = "Announcement and bar for Retaliation.",
	
	retaliationSign_cmd = "retaliationSign",
	retaliationSign_name = "Retaliation sign",
	retaliationSign_desc = "Show sign when the boss has Retaliation.",
	
	recklessness_cmd = "recklessness",
	recklessness_name = "Recklessness alert",
	recklessness_desc = "Announcement and bar for Recklessness.",
	
	-- triggers
	trigger_addDeath = "Flamewaker Priest dies",
	trigger_heal = "begins to cast Dark Mending",
    trigger_spear = "begins to perform Flame Spear",
	trigger_warStomp = "Sulfuron Harbinger ?'s War Stomp",
	trigger_inspire = "Sulfuron Harbinger gains Inspire.",
	trigger_shieldWall = "Sulfuron Harbinger gains Shield Wall.",
	trigger_retaliation = "Sulfuron Harbinger gains Retaliation.",
	trigger_recklessness = "Sulfuron Harbinger is afflicted by Recklessness.",
	trigger_recklessnessStop = "Recklessness fades from Sulfuron Harbinger.",
	
	-- messages
	msg_heal = "Healing!",
	msg_adds = "%d/4 Flamewaker Priests dead!",
	msg_warStomp = "War Stomp!",
	msg_inspire = "Inspire!",
	msg_shieldWall = "Shield Wall!",
	msg_retaliation = "Retaliation!",
	msg_recklessness = "Recklessness! Tranq now!",
	
	-- bars
	bar_heal = "Dark Mending",
	bar_flameSpear = "Flame Spear",
	bar_warStomp = "War Stomp",
	bar_inspire = "Inspire",
	bar_shieldWall = "Shield Wall",
	bar_retaliation = "Retaliation",
	bar_recklessness = "Recklessness",
	
	-- misc
	misc_addName = "Flamewaker Priest",

} end)

L:RegisterTranslations("deDE", function() return {
	-- commands
	heal_name = "Heilung der Adds",
	heal_desc = "Verkündet Heilung der Flamewaker Priest",
	
	adds_name = "Zähler für tote Adds",
	adds_desc = "Verkündet Flamewaker Priests Tod",
	
	-- triggers
	trigger_addDeath = "Flamewaker Priest stirbt",
	trigger_heal = "beginnt Dunkle Besserung",
    trigger_spear = "beginnt Flammenspeer zu wirken",
	
	-- messages
	msg_heal = "Heilung!",
	msg_adds = "%d/4 Flamewaker Priest tot!",
	
	-- bars
	bar_heal = "Dunkle Besserung",
	bar_flameSpear = "Flammenspeer",
	
	-- misc
	misc_addName = "Flamewaker Priest",

} end)
