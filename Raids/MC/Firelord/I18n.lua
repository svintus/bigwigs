------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.mc.firelord
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Firelord",
	
	-- commands	
	lavaSpawn_cmd = "lavaSpawn",
	lavaSpawn_name = "Lava Spawn announcement",
	lavaSpawn_desc = "Announce when Lava Spawn appears.",
	
	conflagrationAnnounce_cmd = "conflagrationAnnounce",
	conflagrationAnnounce_name = "Conflagration announcement",
	conflagrationAnnounce_desc = "Announce when someone is afflicted by Conflagration.",
	
	conflagrationIcon_cmd = "conflagrationIcon",
	conflagrationIcon_name = "Raid Icon on Conflagration",
	conflagrationIcon_desc = "Put a Raid Icon (Triangle) on the person who's afflicted by Conflagration.\n\n(Requires assistant or higher)",
	
	-- triggers
	trigger_lavaSpawn = "Firelord casts Split.",
	trigger_lavaSpawnSplit = "Lava Spawn casts Split.",
	trigger_conflagrationYou = "You are afflicted by Conflagration.",
	trigger_conflagrationOther = "(.+) is afflicted by Conflagration.",
	
	-- messages
	msg_lavaSpawn = "Lava Spawn!",
	msg_lavaSpawnSplit = "Lava Spawn split!",
	msg_conflagration = "Conflagration on %s!",
	
	-- bars
	
	-- misc
	
} end)