------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.firelord
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 3 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.toggleoptions = {"lavaSpawn", "conflagrationAnnounce", "conflagrationIcon" --[[, "bosskill"]]}
module.trashMod = true

-- locals
module.timer = {
	conflagration = 10,
}
local timer = module.timer

module.icon = {}
local icon = module.icon

module.syncName = {
	lavaSpawn = "FirelordLavaSpawn",
	lavaSpawnSplit = "FirelordLavaSpawnSplit",
	conflagration = "FirelordConflagration",
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------

function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.lavaSpawn then
		self:LavaSpawn()
    elseif sync == syncName.lavaSpawnSplit then
		self:LavaSpawnSplit()
    elseif strfind(sync, syncName.conflagration) then
		self:Conflagration(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------

function module:LavaSpawn()
	if self.db.profile.lavaSpawn then
		self:Message(L["msg_lavaSpawn"], "Attention")
	end
end

function module:LavaSpawnSplit()
	if self.db.profile.lavaSpawn then
		self:Message(L["msg_lavaSpawnSplit"], "Attention")
	end
end

function module:Conflagration(name)
	if self.db.profile.conflagrationAnnounce then
		self:Message(format(L["msg_conflagration"], name), "Attention")
	end
	if self.db.profile.conflagrationIcon then
		self:Icon(name, 4, timer.conflagration)
	end
end

----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	local name = UnitName("player")
	
	-- check core functions
	module:LavaSpawn()
	module:LavaSpawnSplit()
	module:Conflagration(name)
	
	module:BigWigs_RecvSync(syncName.lavaSpawn)
	module:BigWigs_RecvSync(syncName.lavaSpawnSplit)
	module:BigWigs_RecvSync(syncName.conflagration, name)
end
