------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.gehennas
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20022 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.wipemobs = { L["misc_flamewakerName"] }
module.toggleoptions = {"adds", "curse", "rain", "shadowbolt", "antiKite", "bosskill"}


-- locals
module.timer = {
	firstCurse = 8.6,  -- 8.6, 10.5
	firstRain = 4,
	rainTick = 2,
	rainDuration = 6,
	nextRain = 19, -- 12, 18
	curse = 21.5,  -- 21.5 - 31.8 or so
	shadowBolt = 1.5,
}
local timer = module.timer

module.icon = {
	curse = "Spell_Shadow_BlackPlague",
	rain = "Spell_Shadow_RainOfFire",
	shadowBolt = "Spell_Shadow_ShadowBolt",
}
local icon = module.icon

module.syncName = {
	curse = "GehennasCurse1",
	add = "GehennasAddDead",
	shadowBolt = "GehennasShadowBolt",
	antiKite = "GehennasAntiKite",
}
local syncName = module.syncName


module.flamewaker = 0

------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.curse then
		self:Curse()
	elseif sync == syncName.add and rest and rest ~= "" then
        rest = tonumber(rest)
        self:AddDeath(rest)
	elseif sync == syncName.shadowBolt then
		self:ShadowBolt()
	elseif sync == syncName.antiKite then
		self:AntiKite()
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Curse()
	if self.db.profile.curse then
		self:DelayedMessage(timer.curse - 5, L["msg_curseSoon"], "Urgent", nil, nil, true)
		self:Bar(L["bar_curse"], timer.curse, icon.curse)
	end
end

function module:AddDeath(number)
	if number and number <= 2 and module.flamewaker < number then
		module.flamewaker = number
		if self.db.profile.adds then
			self:Message(string.format(L["msg_add"], module.flamewaker), "Positive")
		end
	end
end

function module:ShadowBolt()
	if self.db.profile.shadowbolt then		
		self:Message(L["msg_shadowbolt"], "Attention", false, "Alarm")
		self:Bar(L["bar_shadowbolt"], timer.shadowBolt, icon.shadowBolt, true, BigWigsColors.db.profile.interrupt)
	end
end

function module:AntiKite()
	if self.db.profile.antiKite then		
		self:Message(L["msg_antiKite"], "Attention")
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Curse()
	module:AddDeath(1)
	module:ShadowBolt()
	module:AntiKite()
	
	module:BigWigs_RecvSync(syncName.curse)
	module:BigWigs_RecvSync(syncName.add, 1)
	module:BigWigs_RecvSync(syncName.shadowBolt)
	module:BigWigs_RecvSync(syncName.antiKite)
end
