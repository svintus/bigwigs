------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.garr
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 20022 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
--module.wipemobs = { L["misc_addName"] }
module.toggleoptions = {"adds", "addEnrage", "magnetizing", "announce", "icon", "bosskill"}


-- locals
module.timer = {
	magnetizing = 5,
	magnetize   = 25,
}
local timer = module.timer

module.icon = {
	magnetizing = "Spell_Nature_GroundingTotem",
	magnetize   = "Spell_Nature_GroundingTotem",
}
local icon = module.icon

module.syncName = {
	addDeath = "GarrAddDead",
	addEnrage = "GarrAddEnrage",
	magnetizing     = "GarrMagnetizing",
	magnetizingStop = "GarrMagnetizingStop",
	magnetize       = "GarrMagnetize",
	magnetizeStop   = "GarrMagnetizingStop",
}
local syncName = module.syncName


module.adds = 0

------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.addDeath and rest then
        local newCount = tonumber(rest)
		self:AddDeath(newCount)
	elseif sync == syncName.addEnrage then
		self:AddEnrage()
	elseif sync == syncName.magnetizing and rest then
		self:Magnetizing(rest)
	elseif sync == syncName.magnetizingStop and rest then
		self:MagnetizingGone(rest)
	elseif sync == syncName.magnetize and rest then
		self:Magnetize(rest)
	elseif sync == syncName.magnetizeStop and rest then
		self:MagnetizeGone(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:AddDeath(number)
	if number and self.adds < number then
		self.adds = number
		if self.db.profile.adds then
			self:Message(L["msg_add" .. number], "Positive")
			--self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_adds"], (8 - number))
		end
	end
end

function module:AddEnrage()
	if self.db.profile.addEnrage then
		self:Message(L["msg_addEnrage"], "Attention")
	end
end

function module:Magnetizing(name)
	if self.db.profile.magnetizing then
		self:Bar(string.format(L["bar_magnetizing"], name), timer.magnetizing, icon.magnetizing, true, BigWigsColors.db.profile.significant)
		if name == UnitName("player") then
			self:Message(L["msg_magnetizingYou"], "Personal", true, "Alert")
			self:Message(format(L["msg_magnetizingOther"], name), "Attention", false, false, true)
			self:WarningSign(icon.magnetizing, timer.magnetizing)
		else
			self:Message(string.format(L["msg_magnetizingOther"], name), "Attention")
		end
	end
	
	if self.db.profile.icon then
		-- self:Icon(name, -1, timer.magnetizing)
		self:Icon(name, -1, timer.magnetizing + timer.magnetize)
	end
	
	if self.db.profile.announce then
		self:TriggerEvent("BigWigs_SendTell", name, L["msg_magnetizingWhisper"])
	end
end

function module:MagnetizingGone(name)
	if self.db.profile.magnetizing then
		self:RemoveBar(string.format(L["bar_magnetizing"], name))
		if name == UnitName("player") then
			self:RemoveWarningSign(icon.magnetizing)
		end
	end
end

function module:Magnetize(name)
	if self.db.profile.magnetizing then
		self:Bar(string.format(L["bar_magnetize"], name), timer.magnetize, icon.magnetize, true, BigWigsColors.db.profile.significant)
	end
	
	if self.db.profile.icon then
		--self:Icon(name, -1, timer.magnetize)
	end
end

function module:MagnetizeGone(name)
	if self.db.profile.magnetizing then
		self:RemoveBar(string.format(L["bar_magnetize"], name))
		if name == UnitName("player") then
			self:RemoveWarningSign(icon.magnetizing)
		end
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:AddDeath(1)
	module:AddEnrage()
	module:Magnetizing(UnitName("player"))
	module:MagnetizingGone(UnitName("player"))
	module:Magnetize(UnitName("player"))
	module:MagnetizeGone(UnitName("player"))
	
	module:BigWigs_RecvSync(syncName.addDeath, 1)
	module:BigWigs_RecvSync(syncName.addEnrage)
	module:BigWigs_RecvSync(syncName.magnetizing, UnitName("player"))
	module:BigWigs_RecvSync(syncName.magnetizingStop, UnitName("player"))
	module:BigWigs_RecvSync(syncName.magnetize, UnitName("player"))
	module:BigWigs_RecvSync(syncName.magnetizeStop, UnitName("player"))
end
