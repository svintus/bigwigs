------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.mc.garr
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Garr",
	
	-- commands	
	adds_cmd = "adds",
	adds_name = "Dead adds counter",
	adds_desc = "Announces dead Firesworns",
	
	addEnrage_cmd = "addEnrage",
	addEnrage_name = "Add enrage announcement",
	addEnrage_desc = "Announce when Firesworn gains Separation Anxiety.",
	
	magnetizing_cmd = "magnetizing",
	magnetizing_name = "Magnetizing alert",
	magnetizing_desc = "Warn when players have Magnetizing.",
	
	icon_cmd = "icon",
	icon_name = "Raid Icon on Magnetizing players",
	icon_desc = "Put a Raid Icon on the Magnetizing person. (Requires assistant or higher)",
	
	announce_cmd = "whispers",
	announce_name = "Whisper Magnetizing players",
	announce_desc = "Sends a whisper to players targetted by Magnetizing. (Requires assistant or higher)",
	
	-- triggers
	trigger_addDead8 = "Garr is afflicted by Enrage(.+)8",
	trigger_addDead7 = "Garr is afflicted by Enrage(.+)7",
	trigger_addDead6 = "Garr is afflicted by Enrage(.+)6",
	trigger_addDead5 = "Garr is afflicted by Enrage(.+)5",
	trigger_addDead4 = "Garr is afflicted by Enrage(.+)4",
	trigger_addDead3 = "Garr is afflicted by Enrage(.+)3",
	trigger_addDead2 = "Garr is afflicted by Enrage(.+)2",
	trigger_addDead1 = "Garr is afflicted by Enrage.",
	
	trigger_addEnrage = "Firesworn gains Separation Anxiety.",
	
	trigger_magnetizingYou = "You are afflicted by Magnetizing.",
	trigger_magnetizingOther = "(.*) is afflicted by Magnetizing.",
	trigger_magnetizingYouGone = "Magnetizing fades from you.",
	trigger_magnetizingOtherGone = "Magnetizing fades from (.*).",
	
	trigger_magnetizeYou = "You are afflicted by Magnetize.",
	trigger_magnetizeOther = "(.*) is afflicted by Magnetize.",
	trigger_magnetizeYouGone = "Magnetize fades from you.",
	trigger_magnetizeOtherGone = "Magnetize fades from (.*).",
	
	-- messages
	msg_add1 = "1/8 Firesworns dead!",
	msg_add2 = "2/8 Firesworns dead!",
	msg_add3 = "3/8 Firesworns dead!",
	msg_add4 = "4/8 Firesworns dead!",
	msg_add5 = "5/8 Firesworns dead!",
	msg_add6 = "6/8 Firesworns dead!",
	msg_add7 = "7/8 Firesworns dead!",
	msg_add8 = "8/8 Firesworns dead!",
	
	msg_addEnrage = "Add enraged!",
	
	msg_magnetizingWhisper = "You are magnetizing!",
	msg_magnetizingYou = "You are magnetizing!",
	msg_magnetizingOther = "%s is magnetizing!",
	
	-- bars
    bar_adds = "Firesworns dead",
	bar_magnetizing = "Magnetizing: %s",
	bar_magnetize   = "Magnetize: %s",
	
	-- misc
	misc_fireswornName = "Firesworn",

} end)

L:RegisterTranslations("deDE", function() return {
	-- commands	
	adds_name = "Zähler für tote Adds",
	adds_desc = "Verkündet Feueranbeter Tod",
	
	-- triggers
	trigger_addDead1 = "Garr ist von \'Wutanfall\'.",
	trigger_addDead2 = "Garr ist von \'Wutanfall(.+)2",
	trigger_addDead3 = "Garr ist von \'Wutanfall(.+)3",
	trigger_addDead4 = "Garr ist von \'Wutanfall(.+)4",
	trigger_addDead5 = "Garr ist von \'Wutanfall(.+)5",
	trigger_addDead6 = "Garr ist von \'Wutanfall(.+)6",
	trigger_addDead7 = "Garr ist von \'Wutanfall(.+)7",
	trigger_addDead8 = "Garr ist von \'Wutanfall(.+)8",
	
	-- messages
	msg_add1 = "1/8 Feueranbeter tot!",
	msg_add2 = "2/8 Feueranbeter tot!",
	msg_add3 = "3/8 Feueranbeter tot!",
	msg_add4 = "4/8 Feueranbeter tot!",
	msg_add5 = "5/8 Feueranbeter tot!",
	msg_add6 = "6/8 Feueranbeter tot!",
	msg_add7 = "7/8 Feueranbeter tot!",
	msg_add8 = "8/8 Feueranbeter tot!",
	
	-- bars
    bar_adds = "Feueranbeter tot",
	
	-- misc
	misc_fireswornName = "Feueranbeter",

} end)
