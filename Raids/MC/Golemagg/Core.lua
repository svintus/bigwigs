------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.golemagg
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

module.revision = 20017 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.wipemobs = { L["misc_addName"] }
module.toggleoptions = {"earthquake", "enraged", "implosion", "icon", "bosskill"}


-- locals
module.timer = {
	implosion = 8,
}
local timer = module.timer

module.icon = {
	implosion = "INV_Elemental_Primal_Fire",
}
local icon = module.icon

module.syncName = {
	earthquake = "GolemaggEarthquake",
	enrage = "GolemaggEnrage",
	implosion     = "GolemaggImplosion",
	implosionStop = "GolemaggImplosionStop",
}
local syncName = module.syncName

module.earthquakeon = nil


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
    if sync == syncName.earthquake then
		self:Earthquake()
	elseif sync == syncName.enrage then
		self:Enrage()
	elseif sync == syncName.implosion and rest then
		self:Implosion(rest)
	elseif sync == syncName.implosionStop and rest then
		self:ImplosionGone(rest)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:Earthquake()
	if self.db.profile.earthquake then
		self:Message(L["msg_earthquakeSoon"], "Attention", "Alarm")
	end
end

function module:Enrage()
	if self.db.profile.enraged then
		self:Message(L["msg_enrage"], "Attention", true, "Beware")
	end
end

function module:Implosion(name)
	if self.db.profile.implosion then
		self:Bar(string.format(L["bar_implosion"], name), timer.implosion, icon.implosion)
		if name == UnitName("player") then
			self:Message(L["msg_implosionYou"], "Attention", true)
			self:Message(string.format(L["msg_implosionOther"], name), "Attention", false, false, true)
		else
			self:Message(string.format(L["msg_implosionOther"], name), "Attention")
		end
	end
	
	if self.db.profile.icon then
		self:Icon(name, -1, timer.implosion)
	end
end

function module:ImplosionGone(name)
	self:RemoveBar(string.format(L["bar_implosion"], name))
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:Earthquake()
	module:Enrage()
	module:Implosion    (UnitName("player"))
	module:ImplosionGone(UnitName("player"))
	
	module:BigWigs_RecvSync(syncName.earthquake)
	module:BigWigs_RecvSync(syncName.enrage)
	module:BigWigs_RecvSync(syncName.implosion,     UnitName("player"))
	module:BigWigs_RecvSync(syncName.implosionStop, UnitName("player"))
end
