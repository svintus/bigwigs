------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.lavaAnnihilator
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

-- module variables
module.revision = 3
module.enabletrigger = module.translatedName
module.toggleoptions = {"threatreset" --[[, "bosskill"]]}
module.trashMod = true

module.defaultDB = {
    threatreset = false,
}


-- locals
module.timer = {
	threatReset = 10,
}
local timer = module.timer

module.icon = {
	threatReset = "Spell_Shadow_UnholyFrenzy"
}
local icon = module.icon

module.syncName = {
}
local syncName = module.syncName


------------------------------
--      Synchronization	    --
------------------------------


------------------------------
-- Sync Handlers	    	--
------------------------------


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
end
