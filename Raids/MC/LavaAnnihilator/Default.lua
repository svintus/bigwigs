local bossName = BigWigs.bossmods.mc.lavaAnnihilator
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)

------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 3 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
-- Initialization      		--
------------------------------

-- called after module is enabled
function module:OnEnable()
    -- if we exit the instance
    self:RegisterEvent("PLAYER_LEAVING_WORLD", "Disengage")
end

-- called after module is enabled and after each wipe
function module:OnSetup()
end

-- called after boss is engaged
function module:OnEngage()
	if self.db.profile.threatreset then
		self:ThreatReset()
	end
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
	self:RemoveBar(L["bar_threatreset"])
	self:CancelScheduledEvent("BWLavaAnnihilatorThreatReset")
end


------------------------------
-- Event Handlers      		--
------------------------------
function module:ThreatReset()
	self:Bar(L["bar_threatreset"], timer.threatReset, icon.threatReset)
	self:ScheduleRepeatingEvent("BWLavaAnnihilatorThreatReset", self.ThreatReset, timer.threatReset, self)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:ThreatReset()

	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
