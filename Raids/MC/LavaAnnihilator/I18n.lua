------------------------------
-- Localization      		--
------------------------------

local bossName = BigWigs.bossmods.mc.lavaAnnihilator
local L = BigWigs.i18n[bossName]

L:RegisterTranslations("enUS", function() return {
	cmd = "Lavannnihilator",

	-- commands
	threatreset_cmd = "threatreset",
	threatreset_name = "Threat reset",
	threatreset_desc = "Toggles showing bars for threat reset.\n\nNote: the timer is probably not exactly 10 sec (maybe random even) and the bar restarts just every 10 sec rather than when threat reset actually happens. So this bar can be misleading.",
	
	-- triggers
	
	-- bars
	bar_threatreset = "Threat reset",
	
	-- messages
	
	-- misc

} end )
