local bossName = BigWigs.bossmods.mc.ragnaros
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 20026 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()
	self:RegisterEvent("CHAT_MSG_MONSTER_YELL")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF")
	self:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH")
	
	self:ThrottleSync(5, syncName.knockback)
	self:ThrottleSync(timer.magmaBlast, syncName.magmaBlast)
	--self:ThrottleSync(180, syncName.emergeSoon)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	module.firstKnockback = true
	module.sonsdead = 0
	module.phase = nil
	self.doCheckForWipe = false
end

-- called after boss is engaged
function module:OnEngage()
	-- Engage can be triggered earlier during the event with Majordomo, when we are not in combat yet.
	-- So delay CheckForWipe to prevent wipe detection and resetting the module.
	self:ScheduleEvent("BigWigs_RagnarosDelayedCheckForWipe", function() self.doCheckForWipe = true end, timer.combat3)
	self:ScheduleRepeatingEvent("bwragnarosemergecheck", self.EmergeCheck, 0.1, self)
	self:EmergeCheck()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:CHAT_MSG_COMBAT_HOSTILE_DEATH(msg)
	if msg == L["trigger_sonDead"] then
		local numDead = module.sonsdead + 1
		self:Sync(syncName.sons .. "_" .. numDead .. " " .. numDead)
	end
end

function module:CHAT_MSG_MONSTER_YELL(msg)
	if string.find(msg, L["trigger_knockback"]) then
		self:Sync(syncName.knockback)
	elseif string.find(msg, L["trigger_submerge1"]) or msg == L["trigger_submerge2"] then
		self:Sync(syncName.submerge)
	elseif string.find(msg, L["trigger_engage"]) then
		self:SendEngageSync()
    elseif string.find(msg, L["trigger_engageSoon"]) then
        self:Sync(syncName.engageSoon .. " " .. 3)
	-- more annoying than useful
    --elseif string.find(msg, L["trigger_hammer"]) then
    --    self:Sync(syncName.hammer)
	end
end

function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(msg)
	if msg == L["trigger_magmaBlast"] then
		self:Sync(syncName.magmaBlast)
	end
end

function module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(msg)
	if msg == L["trigger_emergeSoon"] and module.sonsdead ~= 0 then  -- sonsdead == 0 means Ragnaros emerges when Majordomo summons him
		self:Sync(syncName.emergeSoon)
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:CHAT_MSG_COMBAT_HOSTILE_DEATH(L["misc_addName"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_knockback"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_submerge1"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_submerge2"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_engage"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_engageSoon"])
	module:CHAT_MSG_MONSTER_YELL(L["trigger_hammer"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_DAMAGE(L["trigger_magmaBlast"])
	module:CHAT_MSG_SPELL_CREATURE_VS_CREATURE_BUFF(L["trigger_emergeSoon"])
	
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
