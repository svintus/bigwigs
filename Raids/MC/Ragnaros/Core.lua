------------------------------
-- Variables     			--
------------------------------

local bossName = BigWigs.bossmods.mc.ragnaros
local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]

module.revision = 20026 -- To be overridden by the module!
module.enabletrigger = module.translatedName -- string or table {boss, add1, add2}
module.wipemobs = { L["misc_addName"] }
module.toggleoptions = {"aoeknock", "ktmReset", "submerge", "emerge", "adds", "magmablast", "bosskill"}


-- locals
module.timer = {
	combat1 = 83.3,
	combat2 = 77,
	combat3 = 47,
	hammer_of_ragnaros = 11,
	emerge = 90,
	emergeSoon = 2.9,
	submerge = 180,
	knockback = 30,       -- 30-35 or so
	knockbackFirst = 25,  -- 25-30 or so
	knockbackAfterEmerge = 4,
	magmaBlast = 0.7,
}
local timer = module.timer

module.icon = {
	combat = "Inv_Hammer_Unique_Sulfuras",
	hammer_of_ragnaros = "Spell_Fire_Incinerate",
	emerge = "Spell_Fire_Volcano",
	submerge = "Spell_Fire_SelfDestruct",
	knockback = "Spell_Fire_SoulBurn",
	knockbackWarn = "Ability_Rogue_Sprint",
	sons = "Interface\\Icons\\spell_fire_fire",
	magmaBlast = "Spell_Fire_FlameShock",
}
local icon = module.icon

module.syncName = {
	knockback = "RagnarosKnockback",
	sons = "RagnarosSonDeadX",
	submerge = "RagnarosSubmerge",
	emerge = "RagnarosEmerge",
	emergeSoon = "RagnarosEmergeSoon",
	engageSoon = "RagnarosEngageSoon",
	hammer = "RagnarosHammer",
	magmaBlast = "RagnarosMagmaBlast",
}
local syncName = module.syncName

module.firstKnockback = nil
module.sonsdead = nil
module.phase = nil
module.doCheckForWipe = nil


------------------------------
-- Override CheckForWipe  	--
------------------------------
function module:CheckForWipe(event)
	if module.doCheckForWipe then
		BigWigs:CheckForWipe(self)
	end
end


------------------------------
--      Synchronization	    --
------------------------------
function module:BigWigs_RecvSync(sync, rest, nick)
	if sync == syncName.engageSoon then
		self:EngageSoon(rest)
	elseif strfind(sync, syncName.sons) and rest and rest ~= "" then
        rest = tonumber(rest)
        self:SonDeath(rest)
	elseif sync == syncName.knockback then
		self:Knockback()
	elseif sync == syncName.submerge then
		self:Submerge()
	elseif sync == syncName.emerge then
		self:Emerge()
	elseif sync == syncName.emergeSoon then
		self:EmergeSoon()
	elseif sync == syncName.hammer then
		self:Hammer()
	elseif sync == syncName.magmaBlast then
		self:ScheduleEvent("bwragnarosmagmablast", self.MagmaBlast, 1 - timer.magmaBlast, self)
	end
end


------------------------------
-- Sync Handlers	    	--
------------------------------
function module:EngageSoon(triggerNumber)
	self:AdjustBar(L["msg_combat"], timer["combat"..triggerNumber], icon.combat, true, BigWigsColors.db.profile.start)
end

function module:SonDeath(number)
	if number and number <= 8 and module.sonsdead < number then
		module.sonsdead = number
		if self.db.profile.adds then
			self:Message(string.format(L["msg_sonDeath"], module.sonsdead), "Positive")
		end
		
		if module.sonsdead == 8 and module.phase == "emerged" then  -- we finished adds only after emerge
			module.sonsdead = 0 -- reset counter
			self:TriggerEvent("BigWigs_StopCounterBar", self, L["bar_sons"])
		end
		self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_sons"], (8 - module.sonsdead))
	end
end

function module:Submerge()
    module.phase = "submerged"
	self:RemoveBar(L["bar_knockback"])
	self:CancelDelayedMessage(L["msg_knockbackSoon"])
	self:CancelDelayedWarningSign(icon.knockbackWarn)
	
	if self.db.profile.submerge then
		self:Message(L["msg_submerge"], "Important")
	end
	if self.db.profile.emerge then
		self:Bar(L["bar_emerge"], timer.emerge, icon.emerge)
		self:DelayedMessage(timer.emerge - 15, L["msg_emergeSoon"], "Urgent", nil, nil, true)
	end
    self:TriggerEvent("BigWigs_StartCounterBar", self, L["bar_sons"], 8, icon.sons)
    self:TriggerEvent("BigWigs_SetCounterBar", self, L["bar_sons"], 8 - 0.1)
end

function module:Emerge()
	-- If someone came online or reloaded mid fight they will send emerge sync.
	-- It is wrong if Ragnaros is already emerged, so ignore it, otherwise it will reset the submerge timer.
	if module.phase == "emerged" then
		return
	end
	
    module.phase = "emerged"
	self.doCheckForWipe = true
	if module.sonsdead == 8 then  -- check in case we didn't kill all the 8 sons before emerge
		module.sonsdead = 0 -- reset counter
		self:TriggerEvent("BigWigs_StopCounterBar", self, L["bar_sons"])
	end

	self:CancelDelayedSync(syncName.emerge)
	self:CancelScheduledEvent("bwragnarosemergecheck")
	self:CancelDelayedMessage(L["msg_emergeSoon"])
	self:RemoveBar(L["bar_emerge"])
	
	if self.db.profile.emerge then
		self:Message(L["msg_emergeNow"], "Attention")
	end
	
	if module.firstKnockback then
		self:Knockback()
	end
	
	if self.db.profile.submerge then
		self:Bar(L["bar_submerge"], timer.submerge, icon.submerge)
		
		self:DelayedMessage(timer.submerge - 60, L["msg_submerge60"], "Attention", nil, nil, true)
		self:DelayedMessage(timer.submerge - 30, L["msg_submerge30"], "Attention", nil, nil, true)
		self:DelayedMessage(timer.submerge - 10, L["msg_submerge10"], "Attention", nil, nil, true)
		self:DelayedMessage(timer.submerge - 5, L["msg_submerge5"], "Attention", nil, nil, true)
	end
end

function module:EmergeSoon()
	if self.db.profile.emerge then
		self:Bar(L["bar_emerge"], timer.emergeSoon, icon.emerge)
	end
	if self.db.profile.aoeknock then
		self:Bar(L["bar_knockback"], timer.knockbackAfterEmerge, icon.knockback)
		self:Message(L["msg_knockbackAfterEmerge"], "Urgent")
		self:WarningSign(icon.knockbackWarn, timer.knockbackAfterEmerge)
	end
	self:DelayedSync(timer.emergeSoon, syncName.emerge)
end

function module:Knockback()
	if self.db.profile.aoeknock then
		local barTimer
		if module.firstKnockback then
			barTimer = timer.knockbackFirst
		else
			barTimer = timer.knockback
			self:Message(L["msg_knockbackNow"], "Important")
		end
		self:Bar(L["bar_knockback"], barTimer, icon.knockback)
		self:DelayedMessage(barTimer - 5, L["msg_knockbackSoon"], "Urgent", true, "Alarm", nil, nil, true)
		self:DelayedWarningSign(barTimer - 5, icon.knockbackWarn, 5)
	end
	if self.db.profile.ktmReset and not module.firstKnockback then
		self:KTM_Reset()
	end
	module.firstKnockback = false
end

function module:Hammer()
	self:Bar(L["bar_hammer"], timer.hammer_of_ragnaros, icon.hammer_of_ragnaros)
end

function module:MagmaBlast()
	if self.db.profile.magmablast then
		local name = ""
		for i = 1, GetNumRaidMembers() do
			if UnitName("raid"..i.."target") == "Ragnaros" then
				name = UnitName("raid"..i.."targettarget")
				break
			end
			if UnitName("raidpet"..i.."target") == "Ragnaros" then
				name = UnitName("raid"..i.."targettarget")
				break
			end
		end
		self:Message(format(L["msg_magmaBlast"], name), "Attention")
		self:Bar(format(L["bar_magmaBlast"], name), timer.magmaBlast, icon.magmaBlast, true, "orange")
	end
end


------------------------------
-- Utility Functions   		--
------------------------------

function module:EmergeCheck()
	local ragnaros = module.translatedName
	local majordomo = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[BigWigs.bossmods.mc.majordomo]).translatedName
	if UnitName("target") == ragnaros and UnitExists("targettarget") and UnitName("targettarget") ~= majordomo then
		self:Sync(syncName.emerge)
		return
	end
	
	local num = GetNumRaidMembers()
	for i = 1, num do
		local raidUnit = string.format("raid%starget", i)
		local raidUnitTarget = raidUnit .. "target"
		if UnitName(raidUnit) == ragnaros and UnitExists(raidUnitTarget) and UnitName(raidUnitTarget) ~= majordomo then
			self:Sync(syncName.emerge)
			return
		end
	end
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModuleCore()
	-- check core functions
	module:EmergeCheck()
	module:Hammer()
	module:Knockback()
	module:Emerge()
	module:EmergeSoon()
	module:Submerge()
	module:SonDeath(1)
	module:EngageSoon(1)
	module:MagmaBlast()
	
	module:BigWigs_RecvSync(syncName.engageSoon, 2)
	module:BigWigs_RecvSync(syncName.sons, 1)
	module:BigWigs_RecvSync(syncName.knockback)
	module:BigWigs_RecvSync(syncName.submerge)
	module:BigWigs_RecvSync(syncName.emerge)
	module:BigWigs_RecvSync(syncName.emergeSoon)
	module:BigWigs_RecvSync(syncName.hammer)
	module:BigWigs_RecvSync(syncName.magmaBlast)
end
