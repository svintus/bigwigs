local bossName = BigWigs.bossmods.mc.geddon
if BigWigs:IsBossSupportedByAnyServerProject(bossName) then
	return
end
-- no implementation found => use default implementation
--BigWigs:Print("default " .. bossName)


------------------------------
-- Variables     			--
------------------------------

local module = BigWigs:GetModule(AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName])
local L = BigWigs.i18n[bossName]
local timer = module.timer
local icon = module.icon
local syncName = module.syncName

-- module variables
module.revision = 20019 -- To be overridden by the module!

-- override timers if necessary
--timer.berserk = 300


------------------------------
--      Initialization      --
------------------------------

-- called after module is enabled
function module:OnEnable()    
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_FRIENDLYPLAYER_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_PARTY_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_SELF_DAMAGE", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_SELF", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_PARTY", "Event")
	self:RegisterEvent("CHAT_MSG_SPELL_AURA_GONE_OTHER", "Event")
	self:RegisterEvent("PLAYER_DEAD")
	self:RegisterEvent("CHAT_MSG_MONSTER_EMOTE")
	
	self:ThrottleSync(5, syncName.bomb)
	self:ThrottleSync(3, syncName.bombStop)
	self:ThrottleSync(4, syncName.service)
	self:ThrottleSync(4, syncName.ignite)
	self:ThrottleSync(29, syncName.inferno)
	self:ThrottleSync(5, syncName.spreadingFlames)
	self:ThrottleSync(2, syncName.spreadingFlamesStop)
end

-- called after module is enabled and after each wipe
function module:OnSetup()
	self.firstinferno = true
	self.firstignite = true
end

-- called after boss is engaged
function module:OnEngage()
	self:Inferno()
	self:ManaIgnite()
end

-- called after boss is disengaged (wipe(retreat) or victory)
function module:OnDisengage()
end


------------------------------
--      Event Handlers      --
------------------------------
function module:Event(msg)
	local _,_, bombother, mcverb = string.find(msg, L["trigger_bombOther"])
	local _,_, bombotherend, mcverb = string.find(msg, L["trigger_bombOtherGone"])
	
	local _,_, spreadingflamesother = string.find(msg, L["trigger_spreadingFlamesOther"])
	local _,_, spreadingflamesotherend = string.find(msg, L["trigger_spreadingFlamesOtherGone"])
	
	if string.find(msg, L["trigger_bombYou"]) then
		self:Sync(syncName.bomb .. " " .. UnitName("player"))
	elseif string.find(msg, L["trigger_bombYouGone"]) then
		self:Sync(syncName.bombStop .. " " .. UnitName("player"))

	elseif bombother then
		self:Sync(syncName.bomb .. " " .. bombother)
	elseif bombotherend then
		self:Sync(syncName.bombStop .. " " .. bombotherend)

	elseif string.find(msg, L["trigger_spreadingFlamesYou"]) then
		self:Sync(syncName.spreadingFlames .. " " .. UnitName("player"))
	elseif string.find(msg, L["trigger_spreadingFlamesYouGone"]) then
		self:Sync(syncName.spreadingFlamesStop .. " " .. UnitName("player"))

    elseif spreadingflamesother then
		self:Sync(syncName.spreadingFlames .. " " .. spreadingflamesother)
	elseif spreadingflamesotherend then
		self:Sync(syncName.spreadingFlamesStop .. " " .. spreadingflamesotherend)

	elseif (string.find(msg, L["trigger_igniteManaHit"]) or string.find(msg, L["trigger_igniteManaResist"])) then
		self:Sync(syncName.ignite)
	end
end

function module:CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE(msg)
	if string.find(msg, L["trigger_inferno"]) then
		self:Sync(syncName.inferno)
	end
end

function module:CHAT_MSG_MONSTER_EMOTE(msg)
	if string.find(msg, L["trigger_service"]) then
		self:Sync(syncName.service)
	end
end

function module:PLAYER_DEAD()
	self:RemoveWarningSign(icon.bombSign)
end


----------------------------------
-- Module Test Function    		--
----------------------------------

-- automated test
function module:TestModule()
	module:OnEnable()
	module:OnSetup()
	module:OnEngage()

	module:TestModuleCore()

	-- check event handlers
	module:Event(L["trigger_bombOther"])
	module:Event(L["trigger_bombOtherGone"])
	module:Event(L["trigger_bombYou"])
	module:Event(L["trigger_bombYouGone"])
	module:Event(L["trigger_igniteManaHit"])
	module:Event(L["trigger_igniteManaResist"])
	module:Event(L["trigger_spreadingFlamesOther"])
	module:Event(L["trigger_spreadingFlamesOtherGone"])
	module:Event(L["trigger_spreadingFlamesYou"])
	module:Event(L["trigger_spreadingFlamesYouGone"])
	module:CHAT_MSG_SPELL_PERIODIC_CREATURE_DAMAGE(L["trigger_inferno"])
	module:CHAT_MSG_MONSTER_EMOTE(L["trigger_service"])
	module:PLAYER_DEAD()
		
	module:OnDisengage()
	module:TestDisable()
end

-- visual test
function module:TestVisual()
	BigWigs:Print(self:ToString() .. " TestVisual not yet implemented")
end
