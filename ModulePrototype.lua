assert(BigWigs, "BigWigs not found!")

local BB = BigWigs.BabbleBoss
local BZ = BigWigs.BabbleZone
local L = BigWigs.i18n["Core"]
local RL = AceLibrary("RosterLib-2.0")

--------------------------------
-- Module Prototype      --
--------------------------------

-- do not override
BigWigs.modulePrototype.core = BigWigs
BigWigs.modulePrototype.debugFrame = ChatFrame1
BigWigs.modulePrototype.engaged = false
BigWigs.modulePrototype.bossSync = nil -- "Ouro"

-- override
BigWigs.modulePrototype.revision = 11 -- To be overridden by the module!
BigWigs.modulePrototype.started = false
BigWigs.modulePrototype.zonename = nil -- AceLibrary("Babble-Zone-2.2")["Ahn'Qiraj"]
BigWigs.modulePrototype.enabletrigger = nil -- boss
BigWigs.modulePrototype.wipemobs = nil -- adds that will be considered in CheckForEngage
BigWigs.modulePrototype.toggleoptions = nil -- {"sweep", "sandblast", "scarab", -1, "emerge", "submerge", -1, "berserk", "bosskill"}
BigWigs.modulePrototype.proximityCheck = nil -- function(unit) return CheckInteractDistance(unit, 2) end
BigWigs.modulePrototype.proximitySilent = nil -- false
BigWigs.modulePrototype.trashMod = nil -- change to true for trash/special boss mods

-- do not override
function BigWigs.modulePrototype:IsBossModule()
	return self.zonename and self.enabletrigger and true
end

-- do not override
function BigWigs.modulePrototype:DebugMessage(msg)
	self.core:DebugMessage(msg, self)
end

-- do not override
function BigWigs.modulePrototype:OnInitialize()
	-- Unconditionally register, this shouldn't happen from any other place
	-- anyway.
	self.core:RegisterModule(self.name, self)

	-- Notify observers that we have loaded.
	self:TriggerEvent("BigWigs_ModuleLoaded", self.name, self)
	
	--[[
	Some events like CHAT_MSG_COMBAT_HOSTILE_DEATH that are used in module prototype for boss modules
	can be later needed by the boss modules themselves.
	But it is not possible to add an additional handler for the same event.
	So register such events to an internal object of ModuleSharedEvents type.
	Then boss modules can register these events without overriding original handlers
	and won't have to manually call the original handlers they overrided.
	]]
	if self:IsBossModule() then
		self.sharedEvents = BigWigs.ModuleSharedEvents:new(self)
	end
end

-- override
function BigWigs.modulePrototype:OnSetup()
end

function BigWigs.modulePrototype:OnEngage()
end

function BigWigs.modulePrototype:OnDisengage()
end

-- do not override
function BigWigs.modulePrototype:Engage(noInstantCheckForWipe)
	self:DebugMessage("Engage() " .. self:ToString())

	if not BigWigs:IsModuleActive(self) then
		BigWigs:EnableModule(self:ToString())
	end

	if self.bossSync and not self.engaged then
		self.engaged = true
		
		if not self.trashMod then
			self:Message(string.format(L["%s engaged!"], self.translatedName), "Positive")
			BigWigsAutoReply:StartBossfight(self)
			BigWigsBossRecords:StartBossfight(self)
			self:KTM_SetTarget(self:ToString())
		end
		
		BigWigs:GetModule("CombatlogFilter"):OnEngage(self:ToString())
		
		self.sharedEvents:RegisterEvent("PLAYER_REGEN_ENABLED")            -- used for CheckForWipe
		self.sharedEvents:RegisterEvent("CHAT_MSG_COMBAT_FRIENDLY_DEATH")  -- used for CheckForWipe, SendUnitDeathSync
		self.sharedEvents:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH")   -- used for CheckForBossDeath, SendMobInterruptSync
		self.sharedEvents:RegisterEvent("BigWigs_RecvSync")                -- used for UnitDeath, Interrupt
		
		self:OnEngage()
		
		if Stopwatch_Command then
			Stopwatch_Command("reset")
			Stopwatch_Command("play")
		end
		
		--[[
		If you are not in the zone/combat you will get Engage sync,
		but CheckForWipe won't start because there might be no triggers for it (you leaving combat or someone dying).
		So we start periodic CheckForWipe checks right after Engage.
		
		TODO: this should not be required if SendWipeSync is implemented. 
		
		Setting noInstantCheckForWipe can be useful for debugging to prevent Wipe in 9.5 sec when manually calling Engage.
		
		Note: a module can override self:CheckForWipe and disable it from the very start, at least for some time.
		That means that the corresponding repeating event won't start even after self:CheckForWipe is enabled back
		(because the original PLAYER_REGEN_ENABLED and CHAT_MSG_COMBAT_FRIENDLY_DEATH triggers are currently disabled).
		This happens on Razorgore module.
		So instead of calling self:CheckForWipe call its body instead to start the repeating event.
		The event won't do anything when self:CheckForWipe is disabled,
		but it will be running and do its work when self:CheckForWipe is enabled.
		]]
		if not noInstantCheckForWipe then
			BigWigs:CheckForWipe(self)
		end
	end
	
end

function BigWigs.modulePrototype:Disengage()
	-- you already released but someone else was still sending syncs
	self:CancelAllScheduledEvents()

	if BigWigs:IsModuleActive(self) then		
		self.engaged = false
		self.started = false

		self:KTM_ClearTarget()

		self:RemoveIcon()
		self:RemoveWarningSign("", true)
		BigWigsBars:Disable(self)
		BigWigsBars:BigWigs_HideCounterBars()

		self:RemoveProximity()
		BigWigsEnrage:Stop()
		
		self:OnDisengage()
		
		if Stopwatch_Command then
			Stopwatch_Command("pause")
		end
	end
end

function BigWigs.modulePrototype:IsEngaged()
	if self.engaged then
		return true
	end
	
	return false
end

function BigWigs.modulePrototype:GetBossPercentage()
	local percentage = nil
	local unitId = nil
	if self:IsEngaged() then
		unitId = "playertarget"
		if UnitName(unitId) == self:ToString() then
			percentage = UnitHealth(unitId) / UnitHealthMax(unitId) * 100
		else
			for i = 1, GetNumRaidMembers(), 1 do
				unitId = "Raid" .. i .. "target"
				if UnitName(unitId) == self:ToString() then
					percentage = UnitHealth(unitId) / UnitHealthMax(unitId) * 100
					break
				end
			end
		end
	end
	
	-- round
	--percentage = percentage + 0.5 - (percentage + 0.5) % 1
	percentage = floor(percentage + 0.5)
	
	return percentage
end

function BigWigs.modulePrototype:GetStatus()
	local percentage = self:GetBossPercentage()
	local alive = 0
	local members = GetNumRaidMembers()
	
	for i = 1, members, 1 do
		if not UnitIsDead("Raid" .. i) then
			alive = alive + 1
		end
	end
	
	return percentage, alive, members
end

function BigWigs.modulePrototype:Victory()
	if self:IsEngaged() then
		if self.db.profile.bosskill then
			self:Message(string.format(L["%s has been defeated"], self.translatedName), "Bosskill", nil, "Victory")
		end

		if not self.trashMod then
			BigWigsAutoReply:Victory(self)
			BigWigsBossRecords:EndBossfight(self)
		end
		
		self:DebugMessage("Boss dead, disabling module [" .. self:ToString() .. "].")
		self.core:DisableModule(self:ToString())
	end
end

function BigWigs.modulePrototype:Wipe()
	if self:IsEngaged() then
		if not self.trashMod then
			BigWigsAutoReply:Wipe(self)
		end
		
		BigWigs:BigWigs_RebootModule(self:ToString())
	end
end

function BigWigs.modulePrototype:Interrupt(mob)
	local mobL = BB:HasTranslation(mob) and BB[mob]
	if not mobL then
		local moduleL = BigWigs.i18n[self.bossSync]
		mobL = moduleL:HasTranslation(mob) and moduleL[mob]
	end
	if mobL then
		for bar in self.sharedEvents.interruptibleBars[mobL].bars do
			self:RemoveBar(bar)
		end
	end
end

function BigWigs.modulePrototype:UnitDeath(name)
	self:RemoveBars("^.+: " .. name .. "$")
end

function BigWigs.modulePrototype:Disable()
	self:Disengage()
	self.core:ToggleModuleActive(self, false)
end

-- synchronize functions
function BigWigs.modulePrototype:GetEngageSync()
	return "BossEngaged"
end

function BigWigs.modulePrototype:SendEngageSync()
	if self.bossSync then
		--self:TriggerEvent("BigWigs_SendSync", "BossEngaged "..self:ToString())
		self:Sync(self:GetEngageSync() .. " " .. self.bossSync)
	end
end

function BigWigs.modulePrototype:GetWipeSync()
	return "BossWipe"
end

--[[function BigWigs.modulePrototype:SendWipeSync()
    if self.bossSync then
        --self:TriggerEvent("BigWigs_SendSync", "BossEngaged "..self:ToString())
		self:Sync(self:GetWipeSync() .. " " .. self.bossSync)
    end
end]]

function BigWigs.modulePrototype:GetBossDeathSync()
	return "BossDeath"
end

function BigWigs.modulePrototype:SendBossDeathSync()
	if self.bossSync then
		self:Sync(self:GetBossDeathSync() .. " " .. self.bossSync)
	end
end

function BigWigs.modulePrototype:GetCombatQuestionSync()
	return "BossCombatQuestion"
end

function BigWigs.modulePrototype:SendCombatQuestionSync()
	if self.bossSync then
		self:Sync(self:GetCombatQuestionSync() .. " " .. self.bossSync)
	end
end

function BigWigs.modulePrototype:GetCombatAnswerSync()
	return "BossCombatAnswer"
end

function BigWigs.modulePrototype:SendCombatAnswerSync()
	if self.bossSync then
		self:Sync(self:GetCombatAnswerSync() .. " " .. self.bossSync)
	end
end

function BigWigs.modulePrototype:GetUnitDeathSync()
	return "UnitDeath"
end

function BigWigs.modulePrototype:SendUnitDeathSync(name)
	-- Add _name (avoid spaces) to sync for cases when several units die almost at once
	-- to prevent losing messages due to throttle and so we don't have to set ThrottleSync to 0.
	self:Sync(self:GetUnitDeathSync() .. "_" .. gsub(name, "%s", "") .. " " .. self.bossSyncOneWord .. " " .. name)
end

function BigWigs.modulePrototype:GetMobInterruptSync()
	return "MobInterrupt"
end

function BigWigs.modulePrototype:SendMobInterruptSync(name)
	--[[ Add _name (avoid spaces) to sync for cases when several units are interrupted almost at once
	to prevent losing messages due to throttle and so we don't have to set ThrottleSync to 0.
	     Adding core revision number to sync might be needed in future
	if changes are made to the interrupt system and people with old versions keep sending wrong syncs. ]]
	self:Sync(self:GetMobInterruptSync() .. "_" .. gsub(name, "%s", "") .. " " .. self.bossSyncOneWord .. " " .. name)
end

-- event handler
local yellTriggers = {} -- [i] = {yell, bossmod}
function BigWigs.modulePrototype:RegisterYellEngage(yell)
	-- Bosses with Yells as Engagetrigger should go through even when the bossmod isn't active yet.
	tinsert(yellTriggers, { yell, self })
end

function BigWigs:CHAT_MSG_MONSTER_YELL(msg)
	for i = 1, table.getn(yellTriggers) do
		local yell = yellTriggers[i][1]
		local mod = yellTriggers[i][2]
		if string.find(msg, yell) then
			-- enable and engage
			self:EnableModule(mod:ToString())
			--self:TriggerEvent("BigWigs_SendSync", "BossEngaged "..self:ToString())
			mod:SendEngageSync()
		end
	end
end

function BigWigs.modulePrototype:CheckForEngage()
	BigWigs:CheckForEngage(self)
end

--[[
The body of this function is called in modules overriding it and BigWigs.modulePrototype:Engage.
So if making changes to it then update those as well.
]]
function BigWigs.modulePrototype:CheckForWipe()
	BigWigs:CheckForWipe(self)
end

function BigWigs.modulePrototype:CheckForBossDeath(msg)
	BigWigs:CheckForBossDeath(msg, self)
end

-- override
function BigWigs.modulePrototype:BigWigs_RecvSync(sync, rest, nick)
end

-- test functions
function BigWigs.modulePrototype:TestDisable()
	self:CancelAllScheduledEvents()
	self:RemoveIcon()
	self:RemoveWarningSign("", true)
	BigWigsBars:Disable(self)
	BigWigsBars:BigWigs_HideCounterBars()
	self:RemoveProximity()
	BigWigsEnrage:Stop()
	
	BigWigs:TriggerEvent("BigWigs_RebootModule", self:ToString())
	BigWigs:DisableModule(self:ToString())
end

function BigWigs.modulePrototype:TestModule()
end

function BigWigs.modulePrototype:TestModuleCore()
end

function BigWigs.modulePrototype:TestVisual()
end

------------------------------
-- Provided API      --
------------------------------

local delayPrefix = "ScheduledEventPrefix"

function BigWigs.modulePrototype:Sync(sync)
	self:TriggerEvent("BigWigs_SendSync", sync)
end

function BigWigs.modulePrototype:DelayedSync(delay, sync)
	self:ScheduleEvent(delayPrefix .. "Sync" .. self:ToString() .. sync, "BigWigs_SendSync", delay, sync)
end

function BigWigs.modulePrototype:CancelDelayedSync(sync)
	self:CancelScheduledEvent(delayPrefix .. "Sync" .. self:ToString() .. sync)
end

function BigWigs.modulePrototype:ThrottleSync(throttle, sync)
	self:TriggerEvent("BigWigs_ThrottleSync", sync, throttle)
end

function BigWigs.modulePrototype:GetVersionIdForSync()
	return "_" .. BigWigs.forkName .. "." .. self.revision
end

function BigWigs.modulePrototype:Message(text, priority, noRaidSay, sound, broadcastOnly)
	self:TriggerEvent("BigWigs_Message", text, priority, noRaidSay, sound, broadcastOnly)
end

function BigWigs.modulePrototype:DelayedMessage(delay, text, priority, noRaidSay, sound, broadcastOnly)
	return self:ScheduleEvent(delayPrefix .. "Message" .. self:ToString() .. text, "BigWigs_Message", delay, text, priority, noRaidSay, sound, broadcastOnly)
end

function BigWigs.modulePrototype:CancelDelayedMessage(text)
	self:CancelScheduledEvent(delayPrefix .. "Message" .. self:ToString() .. text)
end

function BigWigs.modulePrototype:Whisper(text, name)
	self:TriggerEvent("BigWigs_SendTell", name, text)
end

--[[
	starts a regular bar if time is a number
	starts a irregular bar if time is a table with the keys min and max
 ]]
function BigWigs.modulePrototype:Bar(text, time, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	BigWigsBars:StartBar(self, text, time, "Interface\\Icons\\" .. icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	local _, _, name = strfind(text, "^.+: (.+)$")
	if name and RL:GetUnitObjectFromName(name) then
		self:SetCandyBarOnClick(self:BarId(text), function() TargetByName(name, true) end)
	end
end

function BigWigs.modulePrototype:AdjustBar(text, time, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	if self:BarStatus(text) then    -- the bar is already running
		-- adjust the bar's remaining time without changing max time
		-- (calling self:Bar for the existing bar would set max time to the new timer)
		self:SetCandyBarTimeLeft(self:BarId(text), time)
	else    -- there is no bar with such text
		-- start a new bar then
		self:Bar(text, time, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	end
end

function BigWigs.modulePrototype:RemoveBar(text)
	self:TriggerEvent("BigWigs_StopBar", self, text)
end

function BigWigs.modulePrototype:RemoveBars(pattern)
	self:TriggerEvent("BigWigs_StopBars", self, pattern)
end

function BigWigs.modulePrototype:IrregularBar(text, minTime, maxTime, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	BigWigsBars:StartIrregularBar(self, text, minTime, maxTime, "Interface\\Icons\\" .. icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
end

function BigWigs.modulePrototype:DelayedBar(delay, text, time, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	return self:ScheduleEvent(delayPrefix .. "Bar" .. self:ToString() .. text, "BigWigs_StartBar", delay, self, text, time, "Interface\\Icons\\" .. icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
end

function BigWigs.modulePrototype:CancelDelayedBar(text)
	self:CancelScheduledEvent(delayPrefix .. "Bar" .. self:ToString() .. text)
end

function BigWigs.modulePrototype:HPBar(text, max, icon, otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
	self:TriggerEvent("BigWigs_StartHPBar", self, text, max, "Interface\\Icons\\" .. icon,
		otherColor, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10)
end

function BigWigs.modulePrototype:SetHPBar(text, value)
	self:TriggerEvent("BigWigs_SetHPBar", self, text, value)
end

function BigWigs.modulePrototype:BarStatus(text)
	local registered, time, elapsed, running = BigWigsBars:GetBarStatus(self, text)
	return registered, time, elapsed, running
end

function BigWigs.modulePrototype:BarValue(text)
	local _, time, elapsed = BigWigsBars:GetBarStatus(self, text)
	if time and elapsed then
		return time - elapsed
	end
end

function BigWigs.modulePrototype:BarId(text)
	return BigWigsBars:GetBarId(self, text)
end

function BigWigs.modulePrototype:RegisterBarForInterrupts(bar, mob)

	mob = mob or self.translatedName
	
	--[[ interruptibleBars =
	{
		"translatedMobName1" = {
			mobReverseTranslation = "reverseTranslatedMobName1",
			bars = {
				"barName1" = true,
				"barName2" = true,
			},
		},
		"translatedMobName2" = {
			mobReverseTranslation = "reverseTranslatedMobName2",
			bars = {
				"barName3" = true,
				"barName4" = true,
			},
		},
	}
	mobReverseTranslation is used for Sync since it should be the same on all localizations.
	mobReverseTranslation search locations:
	1. Babble-Boss library. Usually for mob == self.translatedName or for mob from self.enabletrigger.
	2. Module's localization. Usually for mob from self.wipemobs.
	]]
	
	if not self.sharedEvents.interruptibleBars then
		self.sharedEvents.interruptibleBars = {}
	end
	if not self.sharedEvents.interruptibleBars[mob] then
		local mobUnlocalized = BB:HasReverseTranslation(mob) and BB:GetReverseTranslation(mob)
		if not mobUnlocalized then
			local moduleL = BigWigs.i18n[self.bossSync]
			mobUnlocalized = moduleL:HasReverseTranslation(mob) and moduleL:GetReverseTranslation(mob)
		end
		if not mobUnlocalized then
			BigWigs:Print(
				"(RegisterBarForInterrupts) Cannot register bar " .. bar .. " for mob " .. mob .. ". " ..
				"Reverse translation for the mob is not found neither in Babble-Boss library nor in module's localization."
			)
			return
		end
		self.sharedEvents.interruptibleBars[mob] = {
			mobReverseTranslation = mobUnlocalized,
			bars = {},
		}
	end
	self.sharedEvents.interruptibleBars[mob].bars[bar] = true
	
	self.sharedEvents:RegisterEvent("CHAT_MSG_SPELL_SELF_DAMAGE",             "CheckForInterrupt_CommonYou")
	self.sharedEvents:RegisterEvent("CHAT_MSG_SPELL_PARTY_DAMAGE",            "CheckForInterrupt_CommonOther")
	self.sharedEvents:RegisterEvent("CHAT_MSG_SPELL_FRIENDLYPLAYER_DAMAGE",   "CheckForInterrupt_CommonOther")
	self.sharedEvents:RegisterEvent("CHAT_MSG_SPELL_DAMAGESHIELDS_ON_SELF",   "CheckForInterrupt_JudgementYou")
	self.sharedEvents:RegisterEvent("CHAT_MSG_SPELL_DAMAGESHIELDS_ON_OTHERS", "CheckForInterrupt_JudgementOther")
	-- "CHAT_MSG_COMBAT_HOSTILE_DEATH" was already registered in Engage
	
	--[[
	TODO: maybe add mob's melee attacks as interrupt triggers since they indicate that the mob is not casting.
	In case the previous methods did not work.
	
	But then would need to put ThrottleSync to 0 for GetMobInterruptSync for cases with fast attack speed,
	like attack -> cast -> quick interrupt -> attack, otherwise the attack after interrupt might get throttled.
	That is not good for performance.
	
	Another option would be to send such syncs only when a bar registered for interrupts is running.
	But if a player has disabled the bar in options then he won't send the sync.
	]]
end

function BigWigs.modulePrototype:Sound(sound)
	self:TriggerEvent("BigWigs_Sound", sound)
end

function BigWigs.modulePrototype:DelayedSound(delay, sound, id)
	if not id then id = "_" end
	return self:ScheduleEvent(delayPrefix .. "Sound" .. self:ToString() .. sound .. id, "BigWigs_Sound", delay, sound)
end

function BigWigs.modulePrototype:CancelDelayedSound(sound, id)
	if not id then id = "_" end
	self:CancelScheduledEvent(delayPrefix .. "Sound" .. self:ToString() .. sound .. id)
end

function BigWigs.modulePrototype:Icon(name, iconnumber, restoreTime)
	self:TriggerEvent("BigWigs_SetRaidIcon", name, iconnumber, restoreTime)
end

function BigWigs.modulePrototype:RemoveIcon()
	self:TriggerEvent("BigWigs_RemoveRaidIcon")
end

function BigWigs.modulePrototype:DelayedRemoveIcon(delay)
	self:ScheduleEvent(delayPrefix .. "Icon" .. self:ToString(), "BigWigs_RemoveRaidIcon", delay)
end


function BigWigs.modulePrototype:WarningSign(icon, duration, force)
	self:TriggerEvent("BigWigs_ShowWarningSign", "Interface\\Icons\\" .. icon, duration, force)
end

function BigWigs.modulePrototype:RemoveWarningSign(icon, forceHide)
	self:TriggerEvent("BigWigs_HideWarningSign", "Interface\\Icons\\" .. icon, forceHide)
end

function BigWigs.modulePrototype:DelayedWarningSign(delay, icon, duration, id)
	if not id then id = "_" end
	self:ScheduleEvent(delayPrefix .. "WarningSign" .. self:ToString() .. icon .. id, "BigWigs_ShowWarningSign", delay, "Interface\\Icons\\" .. icon, duration)
end

function BigWigs.modulePrototype:CancelDelayedWarningSign(icon, id)
	if not id then id = "_" end
	self:CancelScheduledEvent(delayPrefix .. "WarningSign" .. self:ToString() .. icon .. id)
end

function BigWigs.modulePrototype:WarningSignOnClick(func)
	BigWigsWarningSign:OnClick(func)
end


function BigWigs.modulePrototype:Say(msg)
	SendChatMessage(msg, "SAY")
end

function BigWigs.modulePrototype:CombatlogFilter(filter, callback, addStopwatch)
	local CombatlogFilter = BigWigs:GetModule("CombatlogFilter")
	CombatlogFilter:AddFilter(self:ToString(), filter, callback, addStopwatch)
end

-- CombatlogFilter
function BigWigs.modulePrototype:CombatlogFilter(filter, callback, addStopwatch)
	local CombatlogFilter = BigWigs:GetModule("CombatlogFilter")
	CombatlogFilter:AddFilter(self:ToString(), filter, callback, addStopwatch)
end

function BigWigs.modulePrototype:RemoveCombatlogFilter(filter)
	local CombatlogFilter = BigWigs:GetModule("CombatlogFilter")
	CombatlogFilter:AddFilter(self:ToString(), filter)
end

function BigWigs.modulePrototype:RemoveAllCombatlogFilters()
	local CombatlogFilter = BigWigs:GetModule("CombatlogFilter")
	CombatlogFilter:AddFilter(self:ToString())
end

-- proximity
function BigWigs.modulePrototype:Proximity()
	BigWigs:Proximity(self:ToString())
end

function BigWigs.modulePrototype:RemoveProximity()
	BigWigs:RemoveProximity()
end

-- KLHThreatMeter
function BigWigs.modulePrototype:KTM_Reset()
	BigWigs:KTM_Reset()
end

function BigWigs.modulePrototype:KTM_ClearTarget(forceReset)
	BigWigs:KTM_ClearTarget(forceReset)
end

function BigWigs.modulePrototype:KTM_SetTarget(targetName, forceReset)
	if IsAddOnLoaded("KLHThreatMeter") and klhtm and klhtm.net and klhtm.target then
		if targetName and type(targetName) == "string" and (IsRaidLeader() or IsRaidOfficer()) then
			if UnitName("target") == targetName then
				klhtm.net.sendmessage("target " .. targetName)
				if forceReset then
					self:KTM_Reset()
				end
			else
				-- we need to delay the setting mastertarget, as KTM only allows it to work if the person
				-- calling the mastertarget sync has the unit as target
				BigWigs:RegisterEvent("PLAYER_TARGET_CHANGED")
				BigWigs.masterTarget = targetName
				BigWigs.forceReset = forceReset
			end
		end
	end
end


-------------------------------
-- Module Handling      --
-------------------------------

--BigWigs.i18n = {}  -- This would erase BigWigs.i18n["Core"], but why? Some modules might need it.
function BigWigs:ModuleDeclaration(bossName, zoneName)
	translatedName = AceLibrary("Babble-Boss-2.2_BigWigs_VanillaPlus")[bossName]
	local module = BigWigs:NewModule(translatedName)
	--local L = AceLibrary("AceLocale-2.2"):new("BigWigs" .. translatedName)
	BigWigs.i18n[bossName] = AceLibrary("AceLocale-2.2"):new("BigWigs" .. module.name)
	module.translatedName = translatedName

	--local name = string.gsub(bossName, "%s", "") -- untranslated, unique string
	module.bossSync = bossName
	module.bossSyncOneWord = gsub(BB:GetReverseTranslation(bossName), "%s", "") -- untranslated, unique (hopefully) string without spaces

	-- zone
	local raidZones = { "Blackwing Lair", "Ruins of Ahn'Qiraj", "Ahn'Qiraj", "Molten Core", "Naxxramas", "Zul'Gurub", "Onyxia's Lair" }
	local isOutdoorraid = true
	for i, value in ipairs(raidZones) do
		if value == zoneName then
			module.zonename = AceLibrary("Babble-Zone-2.2")[zoneName]
			isOutdoorraid = false
			break
		end
	end
	if isOutdoorraid then
		module.zonename = {
			AceLibrary("AceLocale-2.2"):new("BigWigs")["Outdoor Raid Bosses Zone"],
			AceLibrary("Babble-Zone-2.2")[zoneName]
		}
	end

	--return module, L
	return module, BigWigs.i18n[bossName]
end

function BigWigs:RegisterModule(name, module)
	--[[if module:IsRegistered() then
		error(string.format("%q is already registered.", name))
		return
	end]]

	if module:IsBossModule() then
		self:ToggleModuleActive(module, false)
	end

	-- Set up DB
	local opts
	if module:IsBossModule() and module.toggleoptions then
		opts = module.defaultDB or {}
		for _, v in pairs(module.toggleoptions) do
			if v ~= -1 and opts[v] == nil then  -- do not replace "opts[v] == nil" with "not opts[v]": we need nil only, not "nil or false"
				opts[v] = true
			end
		end
	end

	if module.db and module.RegisterDefaults and type(module.RegisterDefaults) == "function" then
		module:RegisterDefaults("profile", opts or module.defaultDB or {})
	else
		self:RegisterDefaults(name, "profile", opts or module.defaultDB or {})
	end

	if not module.db then
		module.db = self:AcquireDBNamespace(name)
	end

	-- Set up AceConsole
	if module:IsBossModule() then
		local cons
		local revision = type(module.revision) == "number" and module.revision or -1
		local L2 = AceLibrary("AceLocale-2.2"):new("BigWigs" .. name)
		if module.toggleoptions then
			local m = module
			cons = {
				type = "group",
				name = name,
				desc = string.format(L["Options for %s (r%s)."], name, revision),
				args = {
					[L["toggle"]] = {
						type = "toggle",
						name = L["Active"],
						order = 1,
						desc = L["Activate or deactivate this module."],
						get = function() return m.core:IsModuleActive(m) end,
						set = function() m.core:ToggleModuleActive(m) end,
					},
					[L["reboot"]] = {
						type = "execute",
						name = L["Reboot"],
						order = 2,
						desc = L["Reboot this module."],
						func = function() m.core:TriggerEvent("BigWigs_RebootModule", m:ToString()) end,
						hidden = function() return not m.core:IsModuleActive(m) end,
					},
					[L["rebootall"]] = {
						type = "execute",
						name = L["Reboot All"],
						desc = L["Forces the module to reset for everyone in the raid.\n\n(Requires assistant or higher)"],
						order = 3,
						func = function() 
							if (IsRaidLeader() or IsRaidOfficer()) then 
								local name = tostring(module)
								if BigWigs.BabbleBoss:HasReverseTranslation(name) then
									name = BigWigs.BabbleBoss:GetReverseTranslation(name)
								end
								BigWigs:DebugMessage("module sync ".. name)
								m.core:TriggerEvent("BigWigs_SendSync", "RebootModule " .. name) 
							end 
						end,
						hidden = function() return not m.core:IsModuleActive(m) end,
					},
					[L["debug"]] = {
						type = "toggle",
						name = L["Debugging"],
						desc = L["Show debug messages."],
						order = 4,
						get = function() return m:IsDebugging() end,
						set = function(v) m:SetDebugging(v) end,
						hidden = function() return not m:IsDebugging() and not BigWigs:IsDebugging() end,
					},
				},
			}
			local x = 10
			for _, v in pairs(module.toggleoptions) do
				local val = v
				x = x + 1
				if x == 11 and v ~= "bosskill" then
					cons.args["headerblankspotthingy"] = {
						type = "header",
						order = 4,
					}
				end
				if v == -1 then
					cons.args["blankspacer" .. x] = {
						type = "header",
						order = x,
					}
				else
					local l = v == "bosskill" and L or L2
					if l:HasTranslation(v .. "_validate") then
						cons.args[l[v .. "_cmd"]] = {
							type = "text",
							order = v == "bosskill" and -1 or x,
							name = l[v .. "_name"],
							desc = l[v .. "_desc"],
							get = function() return m.db.profile[val] end,
							set = function(v) m.db.profile[val] = v end,
							validate = l[v .. "_validate"],
						}
					else
						cons.args[l[v .. "_cmd"]] = {
							type = "toggle",
							order = v == "bosskill" and -1 or x,
							name = l[v .. "_name"],
							desc = l[v .. "_desc"],
							get = function() return m.db.profile[val] end,
							set = function(v) m.db.profile[val] = v end,
						}
					end
				end
			end
		end

		if cons or module.consoleOptions then
			local zonename = type(module.zonename) == "table" and module.zonename[1] or module.zonename
			local zone = zonename
			if BZ:HasReverseTranslation(zonename) and L:HasTranslation(BZ:GetReverseTranslation(zonename)) then
				zone = L[BZ:GetReverseTranslation(zonename)]
			elseif L:HasTranslation(zonename) then
				zone = L[zonename]
			end
			if not self.cmdtable.args[L["boss"]].args[zone] then
				self.cmdtable.args[L["boss"]].args[zone] = {
					type = "group",
					name = zonename,
					desc = string.format(L["Options for bosses in %s."], zonename),
					args = {},
				}
			end
			if module.external then
				self.cmdtable.args[L["extra"]].args[L2["cmd"]] = cons or module.consoleOptions
			else
				self.cmdtable.args[L["boss"]].args[zone].args[L2["cmd"]] = cons or module.consoleOptions
			end
		end
	elseif module.consoleOptions then
		if module.external then
			self.cmdtable.args[L["extra"]].args[module.consoleCmd or name] = cons or module.consoleOptions
		else
			self.cmdtable.args[L["plugin"]].args[module.consoleCmd or name] = cons or module.consoleOptions
		end
	end

	module.registered = true
	if module.OnRegister and type(module.OnRegister) == "function" then
		module:OnRegister()
	end

	-- Set up target monitoring, in case the monitor module has already initialized
	if module.zonename and module.enabletrigger then
		self:TriggerEvent("BigWigs_RegisterForTargetting", module.zonename, module.enabletrigger)
	end
	
	-- Add version Id for boss syncs to ignore syncs from different forks and module revisions
	if module:IsBossModule() then
		for sync in module.syncName do
			module.syncName[sync] = module.syncName[sync] .. module:GetVersionIdForSync()
		end
	end
end

function BigWigs:EnableModule(moduleName, nosync)
	--local name = BB:HasTranslation(moduleName) and BB[moduleName] or moduleName
	local m = self:GetModule(moduleName)
	if m and not self:IsModuleActive(moduleName) then
		self:ToggleModuleActive(moduleName, true)
		if m:IsBossModule() then
			--m.bossSync = m:ToString()
			if not m.translatedName then
				m.translatedName = m:ToString()
				self:DebugMessage("translatedName for module " .. m:ToString() .. " missing")
			end
			
			if not m.trashMod then
				self:TriggerEvent("BigWigs_Message", string.format(L["%s mod enabled"], m.translatedName or "??"), "Core", true)
			end
		end

		--if not nosync then self:TriggerEvent("BigWigs_SendSync", (m.external and "EnableExternal " or "EnableModule ") .. m.bossSync or (BB:GetReverseTranslation(moduleName))) end
		if not nosync then self:TriggerEvent("BigWigs_SendSync", (m.external and "EnableExternal " or "EnableModule ") .. (m.synctoken or BB:GetReverseTranslation(moduleName))) end

		self:SetupModule(moduleName)
	end
end

-- registers generic events
function BigWigs:SetupModule(moduleName)
	--local name = BB:HasTranslation(moduleName) and BB[moduleName] or moduleName
	local m = self:GetModule(moduleName)
	if m and m:IsBossModule() then
		--m.bossSync = m:ToString()
		--m.bossSync = BB:GetReverseTranslation(moduleName) -- untranslated string
		--self:Print("bossSync: " .. string.gsub(BB:GetReverseTranslation(moduleName), "%s", ""))
		--m.bossSync = string.gsub(BB:GetReverseTranslation(moduleName), "%s", "") -- untranslated, unique string without spaces
		
		m.sharedEvents:RegisterEvent("PLAYER_REGEN_DISABLED") -- used for CheckForEngage
		
		m:RegisterEvent("BigWigs_RecvSync")
		
		m.engaged = false
		
		m:OnSetup()
		
		--[[
		This check is needed in case we are already in combat with a boss after logging in or reloading UI.
		In that case PLAYER_REGEN_DISABLED won't trigger and therefore CheckForEngage won't either.
		Call CheckForEngage manually then.
		
		This can potentially cause issues with sending wrong syncs
		because the module thinks the battle has just started
		while it has already been going for some time instead.
		
		If this happens with certain modules I guess a possible solution would be
		to use a flag in the module file like module.someFlag and check it here to avoid calling CheckForEngage.
		Will have to partially rely on syncs from others then.
		And will be needed to manually register events from Engage and
		call CheckForWipe (to start periodic checks) here in that case.
		]]
		if UnitAffectingCombat("player") then
			m:CheckForEngage()
		end
		
	end
end

function BigWigs:DisableModule(moduleName)
	--local name = BB:HasTranslation(moduleName) and BB[moduleName] or moduleName
	local m = self:GetModule(moduleName)
	if m then
		if m:IsBossModule() then
			m:Disengage()
		end
		self:ToggleModuleActive(m, false)
	end
end

-- event handler
function BigWigs:BigWigs_RebootModule(moduleName)
	if not BigWigs:HasModule(moduleName) then
		-- requester has en client
		if BB:HasTranslation(moduleName) then
			moduleName = BB[moduleName]
		-- requester has not en client
		elseif BB:HasReverseTranslation(moduleName)  then
			moduleName = BB:GetReverseTranslation(moduleName)
		end
	end
	
	local m = self:GetModule(moduleName)
	if m and m:IsBossModule() then
		self:DebugMessage("BigWigs:BigWigs_RebootModule(): " .. m:ToString())
		m:Disengage()
		self:SetupModule(moduleName)
	end
end

function BigWigs:Test()
	local count = 0
	for name, module in self:IterateModules() do
		local status, retval = pcall(module.TestModule)
		if not status then
			count = count + 1
			BigWigs:Print("|cffff0000" .. name .. "|r TestModule |cffff0000failed:|r " .. retval)
		end
	end

	local msg = ""
	if count == 0 then
		msg = "No errors found."
	else
		msg = tostring(count) .. " error(s) found"
	end
	BigWigs:Print(msg)
end